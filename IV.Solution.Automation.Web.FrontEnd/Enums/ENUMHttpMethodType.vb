﻿Public Enum ENUMHttpMethodType
    POST_METHOD = 0
    GET_METHOD = 1
    JSON_POST_METHOD = 2
    PUT_METHOD = 3
    JSON_PUT_METHOD = 4
End Enum
