﻿angular.module('automationApp').service("UtilService", function ($rootScope, $window) {

    //DESENVOLVIMENTO - MAQUINA LOCAL
    var _AutomationServiceURI = 'http://L1P1N0010:7001';
    //var _AutomationServiceURI = 'http://rpahomolog.novigo.com.br/api/7001';
    //var _AutomationServiceURI = 'http://rpa.novigo.com.br/api/7001';
    //var _AutomationServiceURI = 'http://note-brnalves:7001';
    //var _AutomationServiceURI = 'http://l4P2D0020:7001';

    this.AutomationServiceURI = function () {
        return _AutomationServiceURI;
    }

    //DESENVOLVIMENTO - MAQUINA LOCAL
    var _DashboardURLBase = 'http://localhost:50554/';
    //var _DashboardURLBase = 'http://rpahomolog.novigo.com.br/Dash/';
    //var _DashboardURLBase = 'http://rpa.novigo.com.br/api/Dash/';
    this.DashboardURLBase = function () {
        return _DashboardURLBase;
    }

    //DOMINIO HTTPS
    var _arDominioSSL = ['rpahomolog.novigo.com.br'];
    this.GetDominiosSSL = function () {
        return _arDominioSSL;
    }

    //DOMINIO HTTPS
    var _arSimNao = [{ value: undefined, name: '---Selecione---' }, { value: true, name: 'Sim' }, { value: false, name: 'Não' }];
    this.GetListaSimNao = function () {
        return _arSimNao;
    }

    //FORMATAÇÔES
    this.Clear = {
        SpecialCharacters: function (str) {
            var _str = str.toString();
            return _str.replace(/[^a-zA-Z0-9]/g, '');
        }
    };

    //VALIDAÇÔES
    this.CPF = {
        isValid: function (txtPF) {
            if (txtPF != undefined && txtPF != '') {
                var _strCPF = txtPF.toString();
                _strCPF = this.removeFormat(_strCPF);
                var rt = this.ValidateCPF(_strCPF)
                return rt;
            } else {
                return $rootScope.Culture.SystemMessage.Validation.CPF_Undefined;
            }
        },
        ValidateCPF: function (strCPF) {
            var Soma;
            var Resto;
            Soma = 0;
            if (strCPF == "00000000000") return $rootScope.Culture.SystemMessage.Validation.CPF_False;

            for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
            Resto = (Soma * 10) % 11;

            if ((Resto == 10) || (Resto == 11)) Resto = 0;
            if (Resto != parseInt(strCPF.substring(9, 10))) return $rootScope.Culture.SystemMessage.Validation.CPF_False;

            Soma = 0;
            for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
            Resto = (Soma * 10) % 11;

            if ((Resto == 10) || (Resto == 11)) Resto = 0;
            if (Resto != parseInt(strCPF.substring(10, 11))) return $rootScope.Culture.SystemMessage.Validation.CPF_False;
            return true;
        },
        RemoveFormat: function (str) {
            return str.replace(/(\.|\/|\-)/g, "");
        },
        AddMask: function (str) {
            return str.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");
        }
    };

    this.Email = {
        isValid: function (txtEmail) {
            if (txtEmail != undefined && txtEmail != '') {
                var _strEmail = txtEmail.toString();
                var rt = this.ValidateEmail(_strEmail)
                return rt;
            } else {
                return $rootScope.Culture.SystemMessage.Validation.Email_Undefined;
            }
        },
        ValidateEmail: function (strEmail) {
            var reg = new RegExp(/^$|[A-Za-z0-9\\._-]+@[A-Za-z0-9]+([.]{1}[.A-Za-z]{2,})/);
            var result = reg.test(strEmail);
            if (result) { return true }
            else { return $rootScope.Culture.SystemMessage.Validation.Email_Invalid; }
        }
    };

    //DOM
    this.DOM = {
        PageRedirect: function (url) {
            if (url != undefined && url != '') {
                $window.location.href = url;
            } else {
                return 'URL Inválida';
            }
        },
        PageOpenBlank: function (url) {
            if (url != undefined && url != '') {
                $window.open(url, '_blank')
            } else {
                return 'URL Inválida';
            }
        },
        PageLogout: function (url) {
            if (url != undefined && url != '') {
                $window.location.href = url;
            } else {
                return 'URL Inválida';
            }
        },
        GetCheckedValue: function (name) {
            return $('input[name=' + name + ']:checked').val();
        }
    };

    this.CalculaTempo = {
        $values: {
            oDataInicio: null,
            oDataTermino: null
        },
        SetValues: function myfunction(DataInicio, DataTermino) {
            this.$values.oDataInicio = new Date(DataInicio);
            this.$values.oDataTermino = new Date(DataTermino);
        },
        Diferenca: function (DataInicio, DataTermino) {
            this.SetValues(DataInicio, DataTermino);

            var milisecondsDiff = this.$values.oDataTermino.getTime() - this.$values.oDataInicio.getTime();
            return this.ValorRetorno(milisecondsDiff);
        },
        DiferencaMilesegundos: function (DataInicio, DataTermino) {
            this.SetValues(DataInicio, DataTermino);
            return this.$values.oDataTermino.getTime() - this.$values.oDataInicio.getTime();
        },
        ValorRetorno: function (milisecondsDiff) {
            if (milisecondsDiff == 0) {
                return '0s';
            }

            if (milisecondsDiff < 1000) {
                return milisecondsDiff + 'ms';
            }

            var secondsDiff = milisecondsDiff / 1000;
            if (secondsDiff < 60) {
                return secondsDiff.toString().substring(0, 2) + 's';
            }

            var minutesDiff = secondsDiff / 60;
            if (minutesDiff < 60) {
                return this.MilisegundosEmMinutos(milisecondsDiff);
            }

            var hoursDiff = minutesDiff / 60;
            if (hoursDiff.split('.')[0] < 24) {
                return this.MilisegundosEmHoras(milisecondsDiff);
            }

            var daysDiff = hoursDiff / 24;
            return daysDiff.toFixed() + 'd';
        },
        MilisegundosEmMinutos: function (value) {
            var minutes = Math.floor(value / 60000);
            var seconds = ((value % 60000) / 1000).toFixed(0);
            return minutes + "m:" + (seconds < 10 ? '0' : '') + seconds + 's';
        },
        MilisegundosEmHoras: function (value) {
            var hours = Math.floor(value / (1000 * 60 * 60));
            var minutes = Math.floor(value / 60000);
            return hours + "h:" + (minutes < 10 ? '0' : '') + minutes + 'm';
        }
    };

});


