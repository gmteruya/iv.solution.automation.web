﻿angular.module('automationApp').service("ImportacaoLoteService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaImportacaoLote = function (nIdImportacaoDetalhe, nIdImportacaoStatus, bCarregaImportacaoLoteTipo, bCarregaImportacaoLoteDemanda) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaImportacaoLote/' + nIdImportacaoDetalhe + '/' + nIdImportacaoStatus + '/' + bCarregaImportacaoLoteTipo + '/' + bCarregaImportacaoLoteDemanda,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaImportacaoLote = function (nIdImportacaoLote, bCarregaImportacaoLoteTipo, bCarregaImportacaoLoteDemanda) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaImportacaoLote/' + nIdImportacaoLote + '/' + bCarregaImportacaoLoteTipo + '/' + bCarregaImportacaoLoteDemanda,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereImportacaoLote = function (oImportacaoLote) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereImportacaoLote',
            data: oImportacaoLote,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaImportacaoLote = function (oImportacaoLote) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaImportacaoLote',
            data: oImportacaoLote,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    
});