﻿angular.module('automationApp').service("ExpurgoDemandaService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaExpurgoDemanda = function (nIdExpurgoDetalhe, nIdExpurgoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaExpurgoDemanda/' + nIdExpurgoDetalhe + '/' + nIdExpurgoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaExpurgoDemanda = function (nIdExpurgoDemanda) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaExpurgoDemanda/' + nIdExpurgoDemanda,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereExpurgoDemanda = function (oExpurgoDemanda) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereExpurgoDemanda',
            data: oExpurgoDemanda,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaExpurgoDemanda = function (nIdExpurgoDemanda, oExpurgoDemanda) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaExpurgoDemanda/' + nIdExpurgoDemanda,
            data: oExpurgoDemanda,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaExpurgoStatus = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaExpurgoStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };

});