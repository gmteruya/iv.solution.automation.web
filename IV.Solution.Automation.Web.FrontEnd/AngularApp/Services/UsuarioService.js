﻿angular.module('automationApp').service("UsuarioService", function ($http, UtilService) {
    
    // SERVICOS BASICOS - CRUD
    this.ListaUsuario = function (nIdCliente, nIdPerfilAcesso) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaUsuario/' + nIdCliente + '/' + nIdPerfilAcesso,
            headers: { 'Content-Type': 'application/json'}
        });
    };
    this.RetornaUsuario = function (IdUsuario, bCarregaCliente, bCarregaPerfilAcesso, bCarregaUsuarioSessao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaUsuario/' + IdUsuario + '/' + bCarregaCliente + '/' + bCarregaPerfilAcesso + '/' + bCarregaUsuarioSessao,
            headers: { 'Content-Type': 'application/json'}
        });
    };
    this.InsereUsuario = function (oUsuario) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereUsuario',
            data: oUsuario,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaUsuario = function (IdUsuario, oUsuario) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaUsuario/' + IdUsuario,
            data: oUsuario,
            headers: { 'Content-Type': 'application/json' }
        });
    };
     
    // SERVICOS ESPECIFICOS
    this.Login = function (sLogin, sSenha) {
        $http.defaults.headers.common['Authorization'] = undefined;
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/token',
            data: 'username=' + sLogin + '&password=' + sSenha + '&grant_type=password',
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded' }
        });
    };
    this.RetornaUsuarioPorLogin = function (sLogin, bCarregaCliente, bCarregaPerfilAcesso, bCarregaUsuarioSessao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaUsuarioPorLogin/' + sLogin + '/' + bCarregaCliente + '/' + bCarregaPerfilAcesso + '/' + bCarregaUsuarioSessao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaUsuarioPorPerfilAcesso = function (nIdCliente, nIdPerfilAcesso) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaUsuarioPorPerfilAcesso/' + nIdCliente + '/' + nIdPerfilAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.TrocaSenhaUsuario = function (sLogin, sSenha, sNovaSenha, sConfirmacaoNovaSenha, bEsqueceuSenha, sChaveMudancaSenha, sDataSolicitacao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/TrocaSenhaUsuario/' + sLogin + '/' + sSenha + '/' + sNovaSenha + '/' + sConfirmacaoNovaSenha + '/' + bEsqueceuSenha + '/' + sChaveMudancaSenha + '/' + sDataSolicitacao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.EsqueciMinhaSenha = function (sLogin) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/EsqueciMinhaSenha/' + sLogin + '/',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaToken = function (refresh_token, client_id) {
        $http.defaults.headers.common['Authorization'] = undefined;
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/token',
            data: 'grant_type=refresh_token&refresh_token=' + refresh_token + '&client_id=' + client_id,
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded' }
        });
    };

});