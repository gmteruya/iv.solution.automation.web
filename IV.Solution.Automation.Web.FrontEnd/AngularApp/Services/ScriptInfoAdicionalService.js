﻿angular.module('automationApp').service("ScriptInfoAdicionalService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaScriptInfoAdicional = function (nIdScript, sTabelaRelacionada) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptInfoAdicional/' + nIdScript + '/' + sTabelaRelacionada,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaScriptInfoAdicional = function (nIdScriptInfoAdicional, bCarregaScript, bCarregaDominio) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaScriptInfoAdicional/' + nIdScriptInfoAdicional + '/' + bCarregaScript + '/' + bCarregaDominio,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereScriptInfoAdicional = function (oScriptInfoAdicional) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereScriptInfoAdicional',
            data: oScriptInfoAdicional,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaScriptInfoAdicional = function (nIdScriptInfoAdicional, oScriptInfoAdicional) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaScriptInfoAdicional/' + nIdScriptInfoAdicional,
            data: oScriptInfoAdicional,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaScriptInfoAdicionalPorClienteRelacionado = function (nIdCliente, nIdScript, sTabelaRelacionada) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptInfoAdicionalPorClienteRelacionado/' + nIdCliente + '/' + nIdScript + '/' + sTabelaRelacionada,
            headers: { 'Content-Type': 'application/json' }
        });
    };
});