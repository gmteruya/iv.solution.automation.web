﻿angular.module('automationApp').service("ClienteService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaCliente = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaCliente',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaCliente = function (nIdCliente, bCarregaMenu, bCarregaGrupo, bCarregaScript, bCarregaUsuario) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaCliente/' + nIdCliente + '/' + bCarregaMenu + '/' + bCarregaGrupo + '/' + bCarregaScript + '/' + bCarregaUsuario,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereCliente = function (oCliente) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereCliente',
            data: oCliente,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaCliente = function (nIdCliente, oCliente) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaCliente/' + nIdCliente,
            data: oCliente,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.RetornaClientePorCpfCnpj = function (sCpfCnpj) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaClientePorCpfCnpj/' + sCpfCnpj,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaCientePorScriptGrupoRelacionado = function (nIdScript, nIdGrupo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaClientePorScriptGrupoRelacionado/' + nIdScript + '/' + nIdGrupo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});