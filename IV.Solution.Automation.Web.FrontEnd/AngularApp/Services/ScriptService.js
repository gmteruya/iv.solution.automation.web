﻿angular.module('automationApp').service("ScriptService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaScript = function (nIdScriptStatus, nIdScriptPrioridade) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScript/' + nIdScriptStatus + '/' + nIdScriptPrioridade,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaScript = function (IdScript, bCarregaScriptStatus, bCarregaScriptPrioridade, bCarregaCliente, bCarregaGrupo, bCarregaScriptStep, bCarregaScriptVBExterno, bCarregaScriptInfoAdicional) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaScript/' + IdScript + '/' + bCarregaScriptStatus + '/' + bCarregaScriptPrioridade + '/' + bCarregaCliente + '/' + bCarregaGrupo + '/' + bCarregaScriptStep + '/' + bCarregaScriptVBExterno + '/' + bCarregaScriptInfoAdicional,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereScript = function (oScript) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereScript',
            data: oScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaScript = function (IdScript, oScript) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaScript/' + IdScript,
            data: oScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaScriptPorGrupoRelacionado = function (nIdGrupo, nIdScriptStatus, nIdScriptPrioridade) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptPorGrupoRelacionado/' + nIdGrupo + '/' + nIdScriptStatus + '/' + nIdScriptPrioridade,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaScriptPorClienteRelacionado = function (nIdCliente, nIdScriptStatus, nIdScriptPrioridade) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptPorClienteRelacionado/' + nIdCliente + '/' + nIdScriptStatus + '/' + nIdScriptPrioridade,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaScriptPorRoboGrupoRelacionado = function (nIdRobo, nIdGrupo, nIdScriptStatus, nIdScriptPrioridade) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptPorRoboGrupoRelacionado/' + nIdRobo + '/' + nIdGrupo + '/' + nIdScriptStatus + '/' + nIdScriptPrioridade,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS BASICOS - TABELAS AUXILIARES    
    this.ListaScriptStatus = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaScriptPrioridade = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptPrioridade',
            headers: { 'Content-Type': 'application/json' }
        });
    };    

});