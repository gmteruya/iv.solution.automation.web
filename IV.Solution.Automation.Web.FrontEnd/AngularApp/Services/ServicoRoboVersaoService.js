﻿angular.module('automationApp').service("ServicoRoboVersaoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaServicoRoboVersao = function (nIdServicoRoboVersaoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaServicoRoboVersao/' + nIdServicoRoboVersaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaServicoRoboVersao = function (nIdServicoRoboVersao, bCarregaServicoRobo, bCarregaArquivo, bCarregaArquivoTipo, bCarregaArquivoTipoGrupo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaServicoRoboVersao/' + nIdServicoRoboVersao + '/' + bCarregaServicoRobo + '/' + bCarregaArquivo + '/' + bCarregaArquivoTipo + '/' + bCarregaArquivoTipoGrupo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereServicoRoboVersao = function (oServicoRoboVersao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereServicoRoboVersao',
            data: oServicoRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaServicoRoboVersao = function (nIdServicoRoboVersao, oServicoRoboVersao) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaServicoRoboVersao/' + nIdServicoRoboVersao,
            data: oServicoRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.TrocaVersaoServicoRobo = function (oServicoRoboVersao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/TrocaVersaoServicoRobo',
            data: oServicoRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS BASICOS - TABELAS AUXILIARES
    this.ListaServicoRoboVersaoStatus = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaServicoRoboVersaoStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };

});