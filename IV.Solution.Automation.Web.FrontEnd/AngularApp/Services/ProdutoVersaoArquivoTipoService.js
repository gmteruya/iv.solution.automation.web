﻿angular.module('automationApp').service("ProdutoVersaoArquivoTipoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProdutoVersaoArquivoTipo = function () {
        var url = UtilService.AutomationServiceURI() + '/ListaProdutoVersaoArquivoTipo';
        return $http({
            method: 'GET', url: url,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProdutoVersaoArquivoTipo = function (nIdProdutoVersaoArquivoTipo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProdutoVersaoArquivoTipo/' + nIdProdutoVersaoArquivoTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProdutoVersaoArquivoTipo = function (oProdutoVersaoArquivoTipo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProdutoVersaoArquivoTipo',
            data: oProdutoVersaoArquivoTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProdutoVersaoArquivoTipo = function (oProdutoVersaoArquivoTipo) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProdutoVersaoArquivoTipo',
            data: oProdutoVersaoArquivoTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS

});