﻿angular.module('automationApp').service("ScriptCodigoRetornoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaScriptCodigoRetorno = function (nIdScript) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptCodigoRetorno/' + nIdScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaScriptCodigoRetorno = function (nIdScriptCodigoRetorno, bCarregaScript) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaScriptCodigoRetorno/' + nIdScriptCodigoRetorno + '/' + bCarregaScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereScriptCodigoRetorno = function (oScriptCodigoRetorno) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereScriptCodigoRetorno',
            data: oScriptCodigoRetorno,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaScriptCodigoRetorno = function (nIdScriptCodigoRetorno, oScriptCodigoRetorno) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaScriptCodigoRetorno/' + nIdScriptCodigoRetorno,
            data: oScriptCodigoRetorno,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS

});