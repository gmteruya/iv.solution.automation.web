﻿angular.module('automationApp').service("ClienteRegraAcessoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaClienteRegraAcesso = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaClienteRegraAcesso',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaClienteRegraAcesso = function (nIdClienteRegraAcesso) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaClienteRegraAcesso/' + nIdClienteRegraAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereClienteRegraAcesso = function (oClienteRegraAcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereClienteRegraAcesso',
            data: oClienteRegraAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaClienteRegraAcesso = function (nIdClienteRegraAcesso, oClienteRegraAcesso) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaClienteRegraAcesso/' + nIdClienteRegraAcesso,
            data: oClienteRegraAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
 
});