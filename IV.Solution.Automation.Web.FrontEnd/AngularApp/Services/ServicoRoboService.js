﻿angular.module('automationApp').service("ServicoRoboService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaServicoRobo = function (nIdServicoRoboEstado, nIdServicoRoboVersao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaServicoRobo/' + nIdServicoRoboEstado + '/' + nIdServicoRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaServicoRobo = function (nIdServicoRobo, bCarregaRobo, CarregaServicoRoboVersao, CarregaServicoRoboSessao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaServicoRobo/' + nIdServicoRobo + '/' + bCarregaRobo + '/' + CarregaServicoRoboVersao + '/' + +CarregaServicoRoboSessao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereServicoRobo = function (oServicoRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereServicoRobo',
            data: oServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaServicoRobo = function (nIdServicoRobo, oServicoRobo) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaServicoRobo/' + nIdServicoRobo,
            data: oServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ReiniciaServicoRobo = function (ListaServicoRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/ReiniciaServicoRobo',
            data: ListaServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.LigaServicoRobo = function (ListaServicoRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/LigaServicoRobo',
            data: ListaServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.DesligaServicoRobo = function (ListaServicoRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/DesligaServicoRobo',
            data: ListaServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.IniciarMonitoramentoServicoRobo = function (ListaServicoRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/IniciarMonitoramentoServicoRobo',
            data: ListaServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.PararMonitoramentoServicoRobo = function (ListaServicoRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/PararMonitoramentoServicoRobo',
            data: ListaServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS BASICOS - TABELAS AUXILIARES
    this.ListaServicoRoboEstado = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaServicoRoboEstado',
            headers: { 'Content-Type': 'application/json' }
        });
    };   
    this.ListaServicoRoboVersao = function (oServicoRobo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaServicoRoboVersao',
            headers: { 'Content-Type': 'application/json' }
        });
    };

});