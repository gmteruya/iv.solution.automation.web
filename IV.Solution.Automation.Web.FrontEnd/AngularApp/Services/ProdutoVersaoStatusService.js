﻿angular.module('automationApp').service("ProdutoVersaoStatusService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProdutoVersaoStatus = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProdutoVersaoStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProdutoVersaoStatus = function (nIdProdutoVersaoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProdutoVersaoStatus/' + nIdProdutoVersaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProdutoVersaoStatus = function (oProdutoVersaoStatus) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProdutoVersaoStatus',
            data: oProdutoVersaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProdutoVersaoStatus = function (oProdutoVersaoStatus) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProdutoVersaoStatus',
            data: oProdutoVersaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS

});