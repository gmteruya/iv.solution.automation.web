﻿angular.module('automationApp').service("ImportacaoDetalheService", function ($http, UtilService) {
    
    // SERVICOS BASICOS - CRUD
    this.ListaImportacaoDetalhe = function (nIdImportacaoDetalheTipo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaImportacaoDetalhe/' + nIdImportacaoDetalheTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaImportacaoDetalhe = function (nIdImportacaoDetalhe) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaImportacaoDetalhe/' + nIdImportacaoDetalhe,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereImportacaoDetalhe = function (oImportacaoDetalhe) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereImportacaoDetalhe',
            data: oImportacaoDetalhe,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaImportacaoDetalhe = function (oImportacaoDetalhe) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaImportacaoDetalhe',
            data: oImportacaoDetalhe,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    
    // SERVICOS ESPECIFICOS
    this.ListaImportacaoDetalhePorClienteRelacionado = function (nIdCliente) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaImportacaoDetalhePorClienteRelacionado/' + nIdCliente,
            headers: { 'Content-Type': 'application/json' }
        });
    };
});