﻿angular.module('automationApp').service("ProdutoVersaoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProdutoVersao = function (nIdProduto, nIdProdutoVersaoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProdutoVersao/' + nIdProduto + '/' + nIdProdutoVersaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProdutoVersao = function (nIdProdutoVersao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProdutoVersao/' + nIdProdutoVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProdutoVersao = function (oProdutoVersao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProdutoVersao',
            data: oProdutoVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProdutoVersao = function (oProdutoVersao) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProdutoVersao',
            data: oProdutoVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    
});