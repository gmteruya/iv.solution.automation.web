﻿angular.module('automationApp').service("ProcessoExternoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProcessoExterno = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProcessoExterno',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProcessoExterno = function (nIdProcessoExterno, sToken) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProcessoExterno/' + nIdProcessoExterno + '/' + sToken,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProcessoExterno = function (oProcessoExterno) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProcessoExterno',
            data: oProcessoExterno,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProcessoExterno = function (oProcessoExterno) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProcessoExterno/',
            data: oProcessoExterno,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ValidaTokenProcessoExterno = function (sToken) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/ValidaTokenProcessoExterno/' + sToken,
            headers: { 'Content-Type': 'application/json' }
        });
    };
});