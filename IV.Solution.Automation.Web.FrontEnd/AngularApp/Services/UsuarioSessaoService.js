﻿angular.module('automationApp').service("UsuarioSessaoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.RetornaUsuarioSessao = function (nIdUsuarioSessao, sToken) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaUsuarioSessao/' + nIdUsuarioSessao,
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + sToken }
        });
    };
    
    // SERVICOS ESPECIFICOS
    this.EncerraUsuarioSessao = function (oUsuarioSessao) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/EncerraUsuarioSessao/',
            data: oUsuarioSessao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});