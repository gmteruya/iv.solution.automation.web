﻿angular.module('automationApp').service("ScriptStepReprocessamentoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaScriptStepReprocessamento = function (nIdScriptStep, nIdScriptCodigoRetorno, nIdStepTentativaReprocessamento) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptStepReprocessamento/' + nIdScriptStep + '/' + nIdScriptCodigoRetorno + '/' + nIdStepTentativaReprocessamento,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaScriptStepReprocessamento = function (nIdScriptStepReprocessamento, bCarregaScriptStep, bCarregaScriptCodigoRetorno, bCarregaStepTentativaReprocessamento) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaScriptStepReprocessamento/' + nIdScriptStepReprocessamento + '/' + bCarregaScriptStep + '/' + bCarregaScriptCodigoRetorno + '/' + bCarregaStepTentativaReprocessamento,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereScriptStepReprocessamento = function (oScriptStepReprocessamento) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereScriptStepReprocessamento',
            data: oScriptStepReprocessamento,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaScriptStepReprocessamento = function (nIdScriptStepReprocessamento, oScriptStepReprocessamento) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaScriptStepReprocessamento/' + nIdScriptStepReprocessamento,
            data: oScriptStepReprocessamento,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaScriptStepReprocessamentoPorClienteRelacionado = function (nIdCliente, nIdScriptStep, nIdScriptCodigoRetorno, nIdStepTentativaReprocessamento) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptStepReprocessamentoPorClienteRelacionado/' + nIdCliente + '/' + nIdScriptStep + '/' + nIdScriptCodigoRetorno + '/' + nIdStepTentativaReprocessamento,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});