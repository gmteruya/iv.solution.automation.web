﻿angular.module('automationApp').service("PerfilAcessoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaPerfilAcesso = function (nIdPerfilAcessoPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaPerfilAcesso/' + nIdPerfilAcessoPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaPerfilAcesso = function (nIdPerfilAcesso, bCarregaPerfilAcessoMenu, bCarregaPerfilAcessoScriptGrupo, bCarregaPerfilAcessoScriptStep, bCarregaPerfilAcessoScriptSistema, bCarregaUsuario) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaPerfilAcesso/' + nIdPerfilAcesso + '/' + bCarregaPerfilAcessoMenu + '/' + bCarregaPerfilAcessoScriptGrupo + '/' + bCarregaPerfilAcessoScriptStep + '/' + bCarregaPerfilAcessoScriptSistema + '/' + bCarregaUsuario,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InserePerfilAcesso = function (oPerfilAcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InserePerfilAcesso',
            data: oPerfilAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaPerfilAcesso = function (nIdPerfilAcesso, oPerfilAcesso) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaPerfilAcesso/' + nIdPerfilAcesso,
            data: oPerfilAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaPerfilAcessoPorClienteRelacionado = function (nIdCliente, nIdPerfilAcessoPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaPerfilAcessoPorClienteRelacionado/' + nIdCliente + '/' + nIdPerfilAcessoPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaPerfilAcessoPorMenuRelacionado = function (nIdMenu, nIdPerfilAcessoPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaPerfilAcessoPorMenuRelacionado/' + nIdMenu + '/' + nIdPerfilAcessoPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaPerfilAcessoPorScriptGrupoRelacionado = function (nIdScriptGrupo, nIdPerfilAcessoPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaPerfilAcessoPorScriptGrupoRelacionado/' + nIdScriptGrupo + '/' + nIdPerfilAcessoPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaPerfilAcessoPorScriptSistemaRelacionado = function (nIdScriptSistema, nIdPerfilAcessoPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaPerfilAcessoPorScriptSistemaRelacionado/' + nIdScriptSistema + '/' + nIdPerfilAcessoPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaPerfilAcessoPorPerfilAcesso = function (nIdPerfilAcesso, nIdPerfilAcessoPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaPerfilAcessoPorPerfilAcesso/' + nIdPerfilAcesso + '/' + nIdPerfilAcessoPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});