﻿angular.module('automationApp').service("ProdutoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProduto = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProduto',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProduto = function (nIdProduto) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProduto/' + nIdProduto,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProduto = function (oProduto) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProduto',
            data: oProduto,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProduto = function (oProduto) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProduto',
            data: oProduto,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS

});