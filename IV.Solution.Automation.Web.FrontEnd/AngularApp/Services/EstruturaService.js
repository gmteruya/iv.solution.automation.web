﻿angular.module('automationApp').service("EstruturaService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaEstrutura = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaEstrutura',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaEstrutura = function (nIdEstruturaMarco) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaEstrutura/' + nIdEstruturaMarco,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaDados = function (nIdEstruturaMarco) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaDados/' + nIdEstruturaMarco,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});