﻿angular.module('automationApp').service("ProdutoVersaoArquivoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProdutoVersaoArquivo = function (nIdProdutoVersao, nIdProdutoVersaoArquivoTipo, nIdArquivo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProdutoVersaoArquivo/' + nIdProdutoVersao + '/' + nIdProdutoVersaoArquivoTipo + '/' + nIdArquivo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProdutoVersaoArquivo = function (nIdProdutoVersaoArquivo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProdutoVersaoArquivo/' + nIdProdutoVersaoArquivo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProdutoVersaoArquivo = function (oProdutoVersaoArquivo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProdutoVersaoArquivo',
            data: oProdutoVersaoArquivo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProdutoVersaoArquivo = function (oProdutoVersaoArquivo) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProdutoVersaoArquivo',
            data: oProdutoVersaoArquivo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS

});