﻿angular.module('automationApp').service("AutomationConfiguracaoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaAutomationConfiguracao = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaAutomationConfiguracao',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaAutomationConfiguracao = function (nIdAutomationConfiguracao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaAutomationConfiguracao/' + nIdAutomationConfiguracao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereAutomationConfiguracao = function (oAutomationConfiguracao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereAutomationConfiguracao',
            data: oAutomationConfiguracao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaAutomationConfiguracao = function (nIdAutomationConfiguracao, oAutomationConfiguracao) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaAutomationConfiguracao/' + nIdAutomationConfiguracao,
            data: oAutomationConfiguracao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.RetornaAutomationConfiguracaoPorChave = function (sChave) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaAutomationConfiguracaoPorChave/' + sChave,
            headers: { 'Content-Type': 'application/json' }
        });
    };
});