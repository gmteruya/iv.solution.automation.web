﻿angular.module('automationApp').service("RoboService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaRobo = function (nIdRoboEstado, nIdRoboStatus, nIdRoboVersao, sDominioRede) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaRobo/' + nIdRoboEstado + '/' + nIdRoboStatus + '/' + nIdRoboVersao + '/' + sDominioRede,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaRobo = function (nIdRobo, CarregaRoboVersao, CarregaRoboSessao, CarregaProcessoExecucao, CarregaServicoRobo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaRobo/' + nIdRobo + '/' + CarregaRoboVersao + '/' + CarregaRoboSessao + '/' + CarregaProcessoExecucao + '/' + CarregaServicoRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereRobo = function (oRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereRobo',
            data: oRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaRobo = function (nIdRobo, oRobo) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaRobo/' + nIdRobo,
            data: oRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ReiniciaRobo = function (ListaRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/ReiniciaRobo',
            data: ListaRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.LigaRobo = function (ListaRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/LigaRobo',
            data: ListaRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.DesligaRobo = function (ListaRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/DesligaRobo',
            data: ListaRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.IniciarMonitoramentoRobo = function (ListaRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/IniciarMonitoramentoRobo',
            data: ListaRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.PararMonitoramentoRobo = function (ListaRobo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/PararMonitoramentoRobo',
            data: ListaRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaVersaoBot = function (ListaRobo) {

        var url = UtilService.AutomationServiceURI() + '/AtualizaVersaoBot';

        return $http({
            method: 'POST', url: url,
            data: ListaRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    }

    // SERVICOS BASICOS - TABELAS AUXILIARES
    this.ListaRoboEstado = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaRoboEstado',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaRoboStatus = function (oRobo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaRoboStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaRoboVersao = function (oRobo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaRoboVersao',
            headers: { 'Content-Type': 'application/json' }
        });
    };

});