﻿angular.module('automationApp').service("DominioService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaDominio = function (nIdDominioPai, nIdScript) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaDominio/' + nIdDominioPai + '/' + nIdScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaDominio = function (nIdDominio, bCarregaDominioPai, bCarregaScript, bCarregaDominioValor) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaDominio/' + nIdDominio + '/' + bCarregaDominioPai + '/' + bCarregaScript + '/' + bCarregaDominioValor,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereDominio = function (oDominio) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereDominio',
            data: oDominio,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaDominio = function (nIdDominio, oDominio) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaDominio/' + nIdDominio,
            data: oDominio,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaDominioPorClienteRelacionado = function (nIdCliente, nIdDominioPai, nIdScript) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaDominioPorClienteRelacionado/' + nIdCliente + '/' + nIdDominioPai + '/' + nIdScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});