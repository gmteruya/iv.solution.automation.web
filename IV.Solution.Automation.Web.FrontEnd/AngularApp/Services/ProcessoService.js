﻿angular.module('automationApp').service("ProcessoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProcesso = function (nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica, nIdImportacaoLote, sChaveProcesso, sServidorOrigem) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProcesso/' + nIdProcessoStatus + '/' + nIdProcessoClassificacaoPrioridade + '/' + nIdProcessoPai + '/' + nIdScript + '/' + nIdGrupo + '/' + nIdCliente + '/' + nIdProcessoChaveUnica + '/' + nIdImportacaoLote + '/' + sChaveProcesso + '/' + sServidorOrigem,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProcesso = function (nIdProcesso, sAutor, bCarregaProcessoStatus, bCarregaProcessoClassificacaoPrioridade, bCarregaScript, bCarregaGrupo, bCarregaCliente, bCarregaArquivo, bCarregaHistoricoOcorrencia, bCarregaProcessoExecucao, bCarregaInteracao, bCarregaProcessoRelacionado) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProcesso/' + nIdProcesso + '/' + sAutor + '/' + bCarregaProcessoStatus + '/' + bCarregaProcessoClassificacaoPrioridade + '/' + bCarregaScript + '/' + bCarregaGrupo + '/' + bCarregaCliente + '/' + bCarregaArquivo + '/' + bCarregaHistoricoOcorrencia + '/' + bCarregaProcessoExecucao + '/' + bCarregaInteracao + '/' + bCarregaProcessoRelacionado,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProcesso = function (oProcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProcesso',
            data: oProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProcesso = function (nIdProcesso, oProcesso) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProcesso/' + nIdProcesso,
            data: oProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ContinuaExecucaoProcesso = function (oProcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/ContinuaExecucaoProcesso',
            data: oProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.CancelaExecucaoProcesso = function (oProcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/CancelaExecucaoProcesso',
            data: oProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaProcessoPorPerfilAcesso = function (nIdPerfilAcesso, nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica, nIdImportacaoLote, sChaveProcesso, sServidorOrigem) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProcessoPorPerfilAcesso/' + nIdPerfilAcesso + '/' + nIdProcessoStatus + '/' + nIdProcessoClassificacaoPrioridade + '/' + nIdProcessoPai + '/' + nIdScript + '/' + nIdGrupo + '/' + nIdCliente + '/' + nIdProcessoChaveUnica + '/' + nIdImportacaoLote + '/' + sChaveProcesso + '/' + sServidorOrigem,
            headers: { 'Content-Type': 'application/json' }
        });
    };
        
    // SERVICOS BASICOS - TABELAS AUXILIARES
    this.ListaHistoricoClassificacao = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaHistoricoClassificacao',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaInteracaoTipo = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaInteracaoTipo',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaProcessoStatus = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProcessoStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaProcessoClassificacaoPrioridade = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProcessoClassificacaoPrioridade',
            headers: { 'Content-Type': 'application/json' }
        });
    };

});