﻿angular.module('automationApp').service("ProcessoExecucaoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProcessoExecucao = function (nIdProcesso, nIdRobo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProcessoExecucao/' + nIdProcesso + '/' + nIdRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProcessoExecucao = function (nIdProcessoExecucao, bCarregaProcesso, bCarregaRobo, bCarregaArquivo, bCarregaArquivoTipo, bCarregaArquivoTipoGrupo, bCarregaProcessoExecucaoScriptStep, bCarregaScriptStep, bCarregaHistoricoOcorrencia, bCarregaInteracao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProcessoExecucao/' + nIdProcessoExecucao + '/' + bCarregaProcesso + '/' + bCarregaRobo + '/' + bCarregaArquivo + '/' + bCarregaArquivoTipo + '/' + bCarregaArquivoTipoGrupo + '/' + bCarregaProcessoExecucaoScriptStep + '/' + bCarregaScriptStep + '/' + bCarregaHistoricoOcorrencia + '/' + bCarregaInteracao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProcessoExecucao = function (oProcessoExecucao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProcessoExecucao',
            data: oProcessoExecucao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProcessoExecucao = function (nIdProcessoExecucao, oProcessoExecucao) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProcessoExecucao/' + nIdProcessoExecucao,
            data: oProcessoExecucao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});