﻿angular.module('automationApp').service("ScriptSistemaService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaScriptSistema = function (nIdScriptSistemaTipo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptSistema/' + nIdScriptSistemaTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaScriptSistema = function (nIdScriptSistema) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaScriptSistema/' + nIdScriptSistema,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereScriptSistema = function (oScriptSistema) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereScriptSistema',
            data: oScriptSistema,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaScriptSistema = function (nIdScriptSistema, oScriptSistema) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaScriptSistema/' + nIdScriptSistema,
            data: oScriptSistema,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaScriptSistemaTipo = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptSistemaTipo',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaScriptSistemaPorScriptRelacionado = function (nIdScript, nIdScriptSistemaTipo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptSistemaPorScriptRelacionado/' + nIdScript + '/' + nIdScriptSistemaTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaScriptSistemaPorClienteRelacionado = function (nIdCliente, nIdScriptSistemaTipo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptSistemaPorClienteRelacionado/' + nIdCliente + '/' + nIdScriptSistemaTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaPerfilAcessoPorClienteRelacionado = function (nIdPerfilAcesso, nIdScriptSistemaTipo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaPerfilAcessoPorClienteRelacionado/' + nIdPerfilAcesso + '/' + nIdScriptSistemaTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
});