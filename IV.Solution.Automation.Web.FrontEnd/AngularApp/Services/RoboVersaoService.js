﻿angular.module('automationApp').service("RoboVersaoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaRoboVersao = function (nIdRoboVersaoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaRoboVersao/' + nIdRoboVersaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaRoboVersao = function (nIdRoboVersao, bCarregaRobo, bCarregaArquivo, bCarregaArquivoTipo, bCarregaArquivoTipoGrupo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaRoboVersao/' + nIdRoboVersao + '/' + bCarregaRobo + '/' + bCarregaArquivo + '/' + bCarregaArquivoTipo + '/' + bCarregaArquivoTipoGrupo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereRoboVersao = function (oRoboVersao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereRoboVersao',
            data: oRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaRoboVersao = function (nIdRoboVersao, oRoboVersao) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaRoboVersao/' + nIdRoboVersao,
            data: oRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.TrocaVersaoRobo = function (oRoboVersao) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/TrocaVersaoRobo',
            data: oRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS BASICOS - TABELAS AUXILIARES
    this.ListaRoboVersaoStatus = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaRoboVersaoStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };

});