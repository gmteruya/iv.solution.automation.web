﻿angular.module('automationApp').service("LogMensagemService", function ($http, UtilService) {
        
    // SERVICOS ESPECIFICOS
    this.InsereMensagemPerformance = function (sCodigo, dDataInicioExecucao, dDataFimExecucao, sOrigem, sAutor, nIdCliente, nIdScript, nIdGrupo, nIdRobo, nIdProcesso, nIdProcessoExecucao, nIdProcessoExecucaoScriptStep, sMensagem) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereMensagemPerformance/' + sCodigo + '/' + dDataInicioExecucao + '/' + dDataFimExecucao + '/' + sOrigem + '/' + sAutor + '/' + nIdCliente + '/' + nIdScript + '/' + nIdGrupo + '/' + nIdRobo + '/' + nIdProcesso + '/' + nIdProcessoExecucao + '/' + nIdProcessoExecucaoScriptStep + '/' + sMensagem,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    this.InsereMensagemTrace = function () {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereMensagemTrace',
            headers: { 'Content-Type': 'application/json' }
        });
    };

    this.InsereMensagemErro = function () {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereMensagemErro',
            headers: { 'Content-Type': 'application/json' }
        });
    };

    this.InsereMensagemDebug = function () {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereMensagemDebug',
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS

});