﻿angular.module('automationApp').service("GrupoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaGrupo = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaGrupo',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaGrupo = function (nIdGrupo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaGrupo/' + nIdGrupo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereGrupo = function (oGrupo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereGrupo',
            data: oGrupo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaGrupo = function (nIdGrupo, oGrupo) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaGrupo/' + nIdGrupo,
            data: oGrupo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaGrupoPorClienteRelacionado = function (nIdCliente) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaGrupoPorClienteRelacionado/' + nIdCliente,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaGrupoPorRoboRelacionado = function (nIdRobo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaGrupoPorRoboRelacionado/' + nIdRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaGrupoPorScriptRelacionado = function (nIdScript) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaGrupoPorScriptRelacionado/' + nIdScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});