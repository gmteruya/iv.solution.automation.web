﻿angular.module('automationApp').service("LogAplicacaoService", function ($http, UtilService) {

    // SERVICOS ESPECIFICOS
    this.ListaLogAplicacao = function (nIdCliente, nIdScript, nIdGrupo, nIdRobo, nIdProcesso, nIdProcessoExecucao, nIdProcessoExecucaoScriptStep) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaLogAplicacao/' + nIdCliente + '/' + nIdScript + '/' + nIdGrupo + '/' + nIdRobo + '/' + nIdProcesso + '/' + nIdProcessoExecucao + '/' + nIdProcessoExecucaoScriptStep,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});