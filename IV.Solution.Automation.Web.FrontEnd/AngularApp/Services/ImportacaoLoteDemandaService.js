﻿angular.module('automationApp').service("ImportacaoLoteDemandaService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaImportacaoLoteDemanda = function (nIdImportacaoLote, nIdImportacaoDetalheDemanda, nIdImportacaoStatus, bCarregaImportacaoLote, bCarregaImportacaoDetalheDemanda, bCarregaImportacaoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaImportacaoLoteDemanda/' + nIdImportacaoLote + '/' + nIdImportacaoDetalheDemanda + '/' + nIdImportacaoStatus + '/' + bCarregaImportacaoLote + '/' + bCarregaImportacaoDetalheDemanda + '/' + bCarregaImportacaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaImportacaoLoteDemanda = function (nIdImportacaoLoteDemanda, bCarregaImportacaoLote, bCarregaImportacaoDetalheDemanda, bCarregaImportacaoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaImportacaoLoteDemanda/' + nIdImportacaoLoteDemanda + '/' + bCarregaImportacaoLote + '/' + bCarregaImportacaoDetalheDemanda + '/' + bCarregaImportacaoStatus,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereImportacaoLoteDemanda = function (oImportacaoLoteDemanda) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereImportacaoLoteDemanda',
            data: oImportacaoLoteDemanda,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaImportacaoLoteDemanda = function (oImportacaoLoteDemanda) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaImportacaoLoteDemanda',
            data: oImportacaoLoteDemanda,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS

});