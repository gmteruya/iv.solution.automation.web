﻿angular.module('automationApp').service("BizflowService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaBizflow = function (nIdBizflowStatus, sChaveProcesso, dDataCriacaoInicial, dDataCriacaoFinal) {
        //alert("Service: \n" + "nIdBizflowStatus: " + nIdBizflowStatus + "\n sChaveProcesso: " + sChaveProcesso + "\n dDataCriacaoInicial:" + dDataCriacaoInicial + '\n dDataCriacaoFinal' + dDataCriacaoFinal);
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaBizflow/' + nIdBizflowStatus + '/' + dDataCriacaoInicial + '/' + dDataCriacaoFinal + '/' + sChaveProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaBizflow = function (nIdBizflow) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaBizflow/' + nIdBizflow,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaDados = function (nIdBizflow) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaDados/' + nIdBizflow,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});