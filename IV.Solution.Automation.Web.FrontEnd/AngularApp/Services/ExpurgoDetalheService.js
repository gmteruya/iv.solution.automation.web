﻿angular.module('automationApp').service("ExpurgoDetalheService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaExpurgoDetalhe = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaExpurgoDetalhe',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaExpurgoDetalhe = function (nIdExpurgoDetalhe) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaExpurgoDetalhe/' + nIdExpurgoDetalhe,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereExpurgoDetalhe = function (oExpurgoDetalhe) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereExpurgoDetalhe',
            data: oExpurgoDetalhe,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaExpurgoDetalhe = function (nIdExpurgoDetalhe, oExpurgoDetalhe) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaExpurgoDetalhe/' + nIdExpurgoDetalhe,
            data: oExpurgoDetalhe,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});