﻿angular.module('automationApp').service("ProcessoExecucaoScriptStepService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaProcessoExecucaoScriptStep = function (nIdProcessoExecucao, nIdScriptStep, nIdProcessoExecucaoScriptStepPai, nIdProcessoStatus, nIdRobo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaProcessoExecucaoScriptStep/' + nIdProcessoExecucao + '/' + nIdScriptStep + '/' + nIdProcessoExecucaoScriptStepPai + '/' + nIdProcessoStatus + '/' + nIdRobo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaProcessoExecucaoScriptStep = function (nIdProcessoExecucaoScriptStep) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaProcessoExecucaoScriptStep/' + nIdProcessoExecucaoScriptStep,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereProcessoExecucaoScriptStep = function (oProcessoExecucaoScriptStep) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereProcessoExecucaoScriptStep',
            data: oProcessoExecucaoScriptStep,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaProcessoExecucaoScriptStep = function (nIdProcessoExecucaoScriptStep, oProcessoExecucaoScriptStep) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaProcessoExecucaoScriptStep/' + nIdProcessoExecucaoScriptStep,
            data: oProcessoExecucaoScriptStep,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.CheckPoint = function (oProcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/CheckPoint',
            data: oProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ReceivePing = function (oProcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/ReceivePing',
            data: oProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});