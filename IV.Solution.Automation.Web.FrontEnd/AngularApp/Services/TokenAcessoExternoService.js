﻿angular.module('automationApp').service("TokenAcessoExternoService", function ($http, UtilService) {
    
    // SERVICOS ESPECIFICOS
    this.GeraTokenAcessoExterno = function (oTokenAcessoExterno) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/GeraTokenAcessoExterno/',
            data: oTokenAcessoExterno,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});