﻿angular.module('automationApp').service("ArquivoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaArquivo = function (nIdArquivoTipo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaArquivo/' + nIdArquivoTipo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaArquivo = function (nIdArquivo, CarregaArquivoTipo, CarregaArquivoTipoGrupo, CarregaArquivo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaArquivo/' + nIdArquivo + '/' + CarregaArquivoTipo + '/' + CarregaArquivoTipoGrupo + '/' + CarregaArquivo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereArquivo = function (oArquivo) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereArquivo',
            data: oArquivo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaArquivo = function (nIdArquivo, oArquivo) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaArquivo/' + nIdArquivo,
            data: oArquivo,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaArquivoTipo = function (nIdArquivoTipoGrupo) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaArquivoTipo/' + nIdArquivoTipoGrupo,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaArquivoTipoGrupo = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaArquivoTipoGrupo',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaArquivoPorProcessoRelacionado = function (nIdProcesso) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaArquivoPorProcessoRelacionado/' + nIdProcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaArquivoPorProcessoExecucaoRelacionado = function (nIdProcessoExecucao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaArquivoPorProcessoExecucaoRelacionado/' + nIdProcessoExecucao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaArquivoPorRoboVersaoRelacionado = function (nIdRoboVersao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaArquivoPorRoboVersaoRelacionado/' + nIdRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaArquivoPorServicoRoboVersaoRelacionado = function (nIdServicoRoboVersao) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaArquivoPorServicoRoboVersaoRelacionado/' + nIdServicoRoboVersao,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ExcluiSelecionados = function (sAutor, lstArquivosSelecionados) {
        return $http({
            method: 'DELETE', url: UtilService.AutomationServiceURI() + '/ExcluiSelecionados/' + sAutor,
            data: lstArquivosSelecionados,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.DownloadSelecionados = function (sAutor, lstArquivosSelecionados) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/DownloadSelecionados/' + sAutor,
            data: lstArquivosSelecionados,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});