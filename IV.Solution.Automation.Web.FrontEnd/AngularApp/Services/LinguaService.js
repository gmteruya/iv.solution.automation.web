﻿angular.module('automationApp').service("LinguaService", function ($http, UtilService) {
    // SERVICOS BASICOS - CRUD
    this.ListaTraducao = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaTraducao/',
            headers: { 'Content-Type': 'application/json' }
        });
    };
});