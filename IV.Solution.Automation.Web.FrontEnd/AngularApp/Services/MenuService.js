﻿angular.module('automationApp').service("MenuService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaMenu = function (nIdMenuTipo, nIdMenuPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaMenu/' + nIdMenuTipo + '/' + nIdMenuPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaMenu = function (nIdMenu, bCarregaMenuTipo, bCarregaMenuPai, bCarregaUsuario, bCarregaCliente) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaMenu/' + nIdMenu,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereMenu = function (oMenu) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereMenu',
            data: oMenu,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaMenu = function (nIdMenu, oMenu) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaMenu/' + nIdMenu,
            data: oMenu,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    
    // SERVICOS ESPECIFICOS
    this.ListaMenuEncadeado = function (nIdMenuTipo, nIdMenuPai) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaMenuEncadeado/' + nIdMenuTipo + '/' + nIdMenuPai,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaMenuPorClienteRelacionado = function (nIdCliente, nIdMenuTipo, nIdMenuPai, bCarregaMenuEncadeado) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaMenuPorClienteRelacionado/' + nIdCliente + '/' + nIdMenuTipo + '/' + nIdMenuPai + '/' + bCarregaMenuEncadeado,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaMenuPorPerfilAcessoRelacionado = function (nIdPerfilAcesso, nIdMenuTipo, nIdMenuPai, bCarregaMenuEncadeado) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaMenuPorPerfilAcessoRelacionado/' + nIdPerfilAcesso + '/' + nIdMenuTipo + '/' + nIdMenuPai + '/' + bCarregaMenuEncadeado,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    
    // SERVICOS BASICOS - TABELAS AUXILIARES
    this.ListaMenuTipo = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaMenuTipo',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    
});