﻿angular.module('automationApp').service("ScriptStepService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaScriptStep = function (nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptStep/' + nIdScriptStepTipo + '/' + nIdScriptStepPai + '/' + nIdScriptStepReinicioErro + '/' + nIdScript + '/' + nIdScriptSistema,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.RetornaScriptStep = function (nIdScriptStep, bCarregaScriptStepTipo, bCarregaScriptStepPai, bCarregaScriptStepReinicioErro, bCarregaScript) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaScriptStep/' + nIdScriptStep + '/' + bCarregaScriptStepTipo + '/' + bCarregaScriptStepPai + '/' + bCarregaScriptStepReinicioErro + '/' + bCarregaScript,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereScriptStep = function (oScriptStep) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereScriptStep',
            data: oScriptStep,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaScriptStep = function (nIdScriptStep, oScriptStep) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaScriptStep/' + nIdScriptStep,
            data: oScriptStep,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaScriptStepTipo = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptStepTipo',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaScriptStepPorClienteRelacionado = function (nIdCliente, nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptStepPorClienteRelacionado/' + nIdCliente + '/' + nIdScriptStepTipo + '/' + nIdScriptStepPai + '/' + nIdScriptStepReinicioErro + '/' + nIdScript + '/' + nIdScriptSistema,
            headers: { 'Content-Type': 'application/json' }
        });
    };

});