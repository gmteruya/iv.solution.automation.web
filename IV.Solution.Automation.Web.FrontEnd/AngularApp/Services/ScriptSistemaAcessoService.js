﻿angular.module('automationApp').service("ScriptSistemaAcessoService", function ($http, UtilService) {

    // SERVICOS BASICOS - CRUD
    this.ListaScriptSistemaAcesso = function (nIdScriptSistema, nIdScriptSistemaAcessoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptSistemaAcesso/' + nIdScriptSistema + '/' + nIdScriptSistemaAcessoStatus,
        });
    };
    this.RetornaScriptSistemaAcesso = function (nIdScriptSistemaAcesso) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/RetornaScriptSistemaAcesso/' + nIdScriptSistemaAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.InsereScriptSistemaAcesso = function (oScriptSistemaAcesso) {
        return $http({
            method: 'POST', url: UtilService.AutomationServiceURI() + '/InsereScriptSistemaAcesso',
            data: oScriptSistemaAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.AtualizaScriptSistemaAcesso = function (nIdScriptSistemaAcesso, oScriptSistemaAcesso) {
        return $http({
            method: 'PUT', url: UtilService.AutomationServiceURI() + '/AtualizaScriptSistemaAcesso/' + nIdScriptSistemaAcesso,
            data: oScriptSistemaAcesso,
            headers: { 'Content-Type': 'application/json' }
        });
    };

    // SERVICOS ESPECIFICOS
    this.ListaScriptSistemaAcessoStatus = function () {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptSistemaAcessoStatus',
            headers: { 'Content-Type': 'application/json' }
        });
    };
    this.ListaScriptSistemaAcessoPorPerfilAcesso = function (nIdPerfilAcesso, nIdScriptSistema, nIdScriptSistemaAcessoStatus) {
        return $http({
            method: 'GET', url: UtilService.AutomationServiceURI() + '/ListaScriptSistemaAcessoPorPerfilAcesso/' + nIdPerfilAcesso + '/' + nIdScriptSistema + '/' + nIdScriptSistemaAcessoStatus,
        });
    };
    
});