﻿angular.module('automationApp').controller('HomeController', function ($scope, $rootScope, $routeParams, $cookies, $timeout, UtilService, UsuarioService, AuthenticationFactory, UsuarioSessaoService, $translate) {
    
    //Default
    delete $scope.usuario_codigo_retorno;
    $scope.btn_login = true;
    $scope.b_troca_senha = false;
    $scope.b_esqueceu_senha = false;
    $scope.chavemudancasenha = undefined
    $scope.datasolicitacao = undefined;
    $scope.bConfimaSenha = true;

    //Logar via Form
    $scope.frmUsuarioLoginSubmit = function (frmUsuarioLoginModel) {
        try {

            //Zera Variaveis
            $scope.error_login = '';
            $scope.loading = true;

            //Limpa Credenciais
            AuthenticationFactory.ClearCredentials();



            //Login
            UsuarioService.Login(frmUsuarioLoginModel.txtLogin, frmUsuarioLoginModel.txtSenha).then(function (success_login) {

                //Armazena retorno do login
                $scope.usuario_codigo_retorno = success_login.data.usuario_codigo_retorno;
                
                //Retorna a Sessão do Usuário
                UsuarioSessaoService.RetornaUsuarioSessao(success_login.data.id_usuario_sessao, success_login.data.access_token).then(function (usuario_sessao) {

                    // Seta as Credenciais
                    AuthenticationFactory.SetCredentials(success_login.data, usuario_sessao.data.Retorno);

                    //Se codigo de retorno do login for igual a 2 - OK, faz o direcionamento, se não mostra opções de troca de senha.
                    if ($scope.usuario_codigo_retorno === '2' || $scope.usuario_codigo_retorno === 2) {

                        //Redireciona para area logada.
                        UtilService.DOM.PageRedirect('IndexLogged.html');

                    } else if ($scope.usuario_codigo_retorno === '3' || $scope.usuario_codigo_retorno === 3) {

                        //Solicita nova senha.
                        $scope.error_login = 'Crie uma nova senha.';
                        $scope.btn_login = false;
                        $scope.b_troca_senha = true;
                        $scope.loading = false;
                        $scope.footerPosition();

                    } else if ($scope.usuario_codigo_retorno === '4' || $scope.usuario_codigo_retorno === 4) {

                        //Solicita nova senha.
                        $scope.error_login = 'Senha expirada. Troque sua senha.';
                        $scope.btn_login = false;
                        $scope.b_troca_senha = true;
                        $scope.loading = false;
                        $scope.footerPosition();

                    } else if ($scope.usuario_codigo_retorno === '5' || $scope.usuario_codigo_retorno === 5) {

                        //Solicita nova senha.
                        $scope.error_login = 'Crie uma nova senha.';
                        $scope.btn_login = false;
                        $scope.b_troca_senha = true;
                        $scope.loading = false;
                        $scope.footerPosition();

                    }
                }, function (error) {
                    $scope.error_login = 'Erro ao carregar a sessão do usuário.';
                    $scope.loading = false;
                    $scope.footerPosition();
                });
            }, function (error) {
                if (error.data != null && error.data.error_description != null) {
                    $scope.error_login = error.data.error_description;
                }
                else {
                    $scope.error_login = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
                }
                $scope.loading = false;
                $scope.footerPosition();
            });
        } catch (e) {
            $scope.error_login = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
            $scope.loading = false;
            $scope.footerPosition();
        }
    };

    //Troca Senha
    $scope.frmTrocaSenhaUsuarioSubmit = function (frmUsuarioLoginModel) {
        try {

            //Zera Variaveis de Scope
            $scope.error_login = ''
            $scope.loading = true

            //Variaveis
            var sLogin;
            var sSenha;
            var sNovaSenha;
            var sConfirmacaoNovaSenha;

            //Pega login e senha
            sLogin = frmUsuarioLoginModel.txtLogin;
            sSenha = frmUsuarioLoginModel.txtSenha;

            //Se os campos da nova senha e confirmação da senha nao estão visiveis então mostra campos.
            if (frmUsuarioLoginModel.txtNovaSenha == undefined && frmUsuarioLoginModel.txtConfirmacaoNovaSenha == undefined) {
                $scope.error_login = 'Crie uma nova senha.';
                $scope.btn_login = false;
                $scope.b_troca_senha = true;
                $scope.loading = false;
                $scope.footerPosition();
            } else {

                //Pega nova senha e confirmação da senha
                if (frmUsuarioLoginModel.txtNovaSenha !== undefined && frmUsuarioLoginModel.txtNovaSenha !== null) {
                    sNovaSenha = frmUsuarioLoginModel.txtNovaSenha;
                }
                if (frmUsuarioLoginModel.txtConfirmacaoNovaSenha !== undefined && frmUsuarioLoginModel.txtConfirmacaoNovaSenha !== null) {
                    sConfirmacaoNovaSenha = frmUsuarioLoginModel.txtConfirmacaoNovaSenha;
                }

                //Troca Senha
                UsuarioService.TrocaSenhaUsuario(sLogin, sSenha, sNovaSenha, sConfirmacaoNovaSenha, $scope.b_esqueceu_senha, $scope.chavemudancasenha, $scope.datasolicitacao).then(function (success_troca_senha) {

                    //Armazena retorno do login
                    $scope.usuario_codigo_retorno = success_troca_senha.data.Retorno;

                    //Se codigo de retorno do login for igual a 2 - OK, faz o direcionamento, se não mostra opções de troca de senha.
                    if ($scope.usuario_codigo_retorno === '2' || $scope.usuario_codigo_retorno === 2) {

                        //Login
                        UsuarioService.Login(sLogin, sNovaSenha).then(function (success_login) {

                            //Armazena retorno do login
                            $scope.usuario_codigo_retorno = success_login.data.usuario_codigo_retorno;

                            //Retorna a Sessão do Usuário
                            UsuarioSessaoService.RetornaUsuarioSessao(success_login.data.id_usuario_sessao).then(function (usuario_sessao) {

                                // Seta as Credenciais
                                AuthenticationFactory.SetCredentials(success_login.data, usuario_sessao.data.Retorno);

                                //Redireciona para area logada.
                                UtilService.DOM.PageRedirect('IndexLogged.html');

                            }, function (error) {
                                $scope.error_login = 'Erro ao carregar a sessão do usuário.';
                                $scope.loading = false;
                                $scope.footerPosition();
                            });
                        }, function (error) {
                            if (error.data != null) {
                                $scope.error_login = error.data.error_description;
                            }
                            else {
                                $scope.error_login = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
                            }
                            $scope.loading = false;
                            $scope.footerPosition();
                        });
                    } else {

                        //informa motivo que nao pode trocar a senha.
                        $scope.error_login = success_troca_senha.data.MsgRetorno;
                        $scope.btn_login = false;
                        $scope.b_troca_senha = true;
                        $scope.loading = false;
                        $scope.footerPosition();
                    };
                }, function (error) {
                    $scope.error_login = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
                    $scope.loading = false;
                    $scope.footerPosition();
                });
            }
        } catch (e) {
            $scope.error_login = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
            $scope.loading = false;
            $scope.footerPosition();
        }
    };

    $scope.ValidaConfirmacaoSenha = function () {
        try {
            if ($scope.frmUsuarioLogin.txtConfirmacaoNovaSenha.$modelValue != $scope.frmUsuarioLogin.txtNovaSenha.$modelValue) {
                $scope.frmUsuarioLogin.txtConfirmacaoNovaSenha.$setValidity("txtConfirmacaoNovaSenha", false);
                $scope.bConfimaSenha = false
            } else {
                $scope.frmUsuarioLogin.txtConfirmacaoNovaSenha.$setValidity("txtConfirmacaoNovaSenha", true);
                $scope.bConfimaSenha = true
            }
        } catch (e) {
            $scope.error_login = e.message;
        }
    };

    //Esqueci Minha Senha
    $scope.frmEsqueciMinhaSenhaSubmit = function (frmUsuarioLoginModel) {
        try {

            //Zera Variaveis
            $scope.error_login = ''
            $scope.loading = true

            //Valida Login
            if (frmUsuarioLoginModel === undefined || frmUsuarioLoginModel === null || frmUsuarioLoginModel.txtLogin === undefined || frmUsuarioLoginModel.txtLogin === null || frmUsuarioLoginModel.txtLogin === '') {
                $scope.error_login = '"O campo de [LOGIN] é obrigatório.';
                $scope.loading = false
            } else {

                //Esqueci minha senha.
                UsuarioService.EsqueciMinhaSenha(frmUsuarioLoginModel.txtLogin).then(function (success_troca_senha) {

                    //Link juntamente com a chave de acesso enviada por e-mail ou erro.
                    $scope.error_login = success_troca_senha.data.MsgRetorno;
                    $scope.btn_login = true;
                    $scope.b_troca_senha = false;
                    $scope.loading = false;
                    $scope.footerPosition();
                }, function (error) {
                    $scope.error_login = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
                    $scope.loading = false;
                    $scope.footerPosition();
                });
            }
        } catch (e) {
            $scope.error_login = e.message;
            $scope.loading = false;
            $scope.footerPosition();
        }
    };

    //Verifica se usuário esqueceu a senha.
    if ($routeParams.tipoacesso !== undefined && $routeParams.usuario !== undefined && $routeParams.chavemudancasenha !== undefined && $routeParams.datasolicitacao !== undefined) {
        $scope.btn_login = false;
        $scope.b_troca_senha = true;
        $scope.b_esqueceu_senha = true;
        $scope.frmUsuarioLoginModel = {};
        $scope.frmUsuarioLoginModel.txtLogin = $routeParams.usuario;
        $scope.frmUsuarioLoginModel.txtSenha = $routeParams.usuario;
        $scope.chavemudancasenha = $routeParams.chavemudancasenha;
        $scope.chavemudancasenha = $routeParams.chavemudancasenha;
        $scope.datasolicitacao = $routeParams.datasolicitacao;
        $scope.error_login = 'Crie uma nova senha.';
    }

    //Cancela Troca de Senha
    $scope.frmCancelaTrocaSenhaUsuarioSubmit = function (frmUsuarioLoginModel) {
        //Default
        delete $scope.usuario_codigo_retorno;
        $scope.btn_login = true;
        $scope.b_troca_senha = false;
        $scope.b_esqueceu_senha = false;
        $scope.chavemudancasenha = undefined
        $scope.datasolicitacao = undefined;
        $scope.error_login = '';
    };
    
    $scope.footerPosition = function () {
        $timeout(function () {
            fnFooterPosition();
        });
    };

    $scope.changeLanguage = function () {
        var language;

        if (window.navigator.languages) {
            language = window.navigator.languages[0];
        } else {
            language = window.navigator.userLanguage || window.navigator.language;
        }

        //language = 'es-MX';
        $translate.use(language);
        document.documentElement.lang = $translate.use();
    };

    $scope.changeLanguage();

});

function fnFooterPosition() {
    var wh = $(window).height();
    var LoginConteudo_ht = $('.LayoutController').outerHeight();
    var LoginFooter_ht = $('.Login_Footer').outerHeight();

    if (wh < (LoginConteudo_ht + LoginFooter_ht)) {
        $('.Login_Footer').attr('style', 'position:relative; float:left; margin-top:20px; width:100%;');
        $('body').attr('style', 'background-size: 100% ' + (LoginConteudo_ht * 1.1) + 'px;');
    }
    else {
        $('.Login_Footer').attr('style', 'position:fixed;bottom:10px;width: 90%;margin: 0px 5%;');
        $('body').attr('style','background-size:100% 100%;');
    }
}

$(window).load(function () {
    fnFooterPosition();
});

$(window).resize(function () {
    fnFooterPosition();
});
