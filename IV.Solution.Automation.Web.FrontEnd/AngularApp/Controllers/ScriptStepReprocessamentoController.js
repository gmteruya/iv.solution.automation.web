﻿angular.module('automationApp').controller('ScriptStepReprocessamentoController', function ($scope, $rootScope, ScriptStepReprocessamentoService, ClienteService, ScriptService, ScriptStepService, ScriptCodigoRetornoService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaScriptStepReprocessamento = function (nIdScriptStepReprocessamentoTipo, nIdScriptStepReprocessamentoPai, nIdScriptStepReprocessamentoReinicioErro, nIdScript) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ScriptStepReprocessamentoService.ListaScriptStepReprocessamentoPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, nIdScriptStepReprocessamentoTipo, nIdScriptStepReprocessamentoPai, nIdScriptStepReprocessamentoReinicioErro, nIdScript).then(function successCallback(response) {
                    $scope.Reprocessamentos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });

            } else {
                ScriptStepReprocessamentoService.ListaScriptStepReprocessamento(nIdScriptStepReprocessamentoTipo, nIdScriptStepReprocessamentoPai, nIdScriptStepReprocessamentoReinicioErro, nIdScript).then(function successCallback(response) {
                    $scope.Reprocessamentos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de configuração de reprocessamento de step!");
        }
    }
    $scope.InsereScriptStepReprocessamento = function () {
        try {
            var oScriptStepReprocessamento = $scope.oScriptStepReprocessamento;
            oScriptStepReprocessamento.Ativo = true;
            oScriptStepReprocessamento.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptStepReprocessamento.DataCriacao = new Date();
            ScriptStepReprocessamentoService.InsereScriptStepReprocessamento(oScriptStepReprocessamento).then(function successCallback(response) {
                $scope.Reprocessamentos.unshift(response.data.Retorno);
                delete $scope.oScriptStepReprocessamento;
                $scope.ListaScriptStep($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0, 0, 0, 0);
                $scope.$emit('atualizaTituloPagina', 'Configurações de Reprocessamento de Step');
                $scope.ViewMain = 'Views/Sistema/ScriptStepReprocessamento/ListaScriptStepReprocessamento.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir nova configuração de reprocessamento de step!");
        }
    }
    $scope.AtualizaScriptStepReprocessamento = function () {
        try {
            var IdScriptStepReprocessamento = $scope.oScriptStepReprocessamento.Id;
            var oScriptStepReprocessamento = $scope.oScriptStepReprocessamento;
            oScriptStepReprocessamento.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptStepReprocessamento.DataAlteracao = new Date();
            ScriptStepReprocessamentoService.AtualizaScriptStepReprocessamento(IdScriptStepReprocessamento, oScriptStepReprocessamento).then(function successCallback(response) {
                $scope.Reprocessamentos = response.data.Retorno;
                $scope.oScriptStepReprocessamento = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar a configuração de reprocessamento de step!");
        }
    }

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                     $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de clientes!");
        }
    }
    $scope.ListaScript = function (nIdCliente, nIdScriptStatus, nIdScriptPrioridade) {
        try {          
            ScriptService.ListaScriptPorClienteRelacionado(nIdCliente, nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                $scope.Scripts = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });         
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }
    $scope.ListaScriptStep = function (nIdCliente, nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema) {
        try {
            if (nIdCliente > 0) {
                ScriptStepService.ListaScriptStepPorClienteRelacionado(nIdCliente, nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema).then(function successCallback(response) {
                    $scope.Steps = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptStepService.ListaScriptStep(nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema).then(function successCallback(response) {
                    $scope.Steps = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de steps!");
        }
    }
    $scope.ListaScriptCodigoRetorno = function (nIdScript) {
        try {
            ScriptCodigoRetornoService.ListaScriptCodigoRetorno(nIdScript).then(function successCallback(response) {
                $scope.CodigosRetornos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de codigos de retorno!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {            
        $rootScope.loading = [true, 'Carregando'];
        $scope.oScriptStepReprocessamento = {};
        $scope.$emit('atualizaTituloPagina', 'Inserindo um nova configuração de reprocessamento.');
        $scope.ViewMain = 'Views/Sistema/ScriptStepReprocessamento/InsereScriptStepReprocessamento.html';
        $rootScope.loading = [false];
    }
    $scope.btnEditar = function () {
        var nIdScriptStepReprocessamento = this.oScriptStepReprocessamento.Id;
        try {
            ScriptStepReprocessamentoService.RetornaScriptStepReprocessamento(nIdScriptStepReprocessamento, true, true).then(function successCallback(response) {
                $scope.oScriptStepReprocessamento = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ScriptStepReprocessamento/DetalheScriptStepReprocessamento.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ScriptStepReprocessamento!");
        }
    }
     $scope.btnCancelarVoltar = function () {
        delete $scope.oScriptStepReprocessamento;
        $scope.ListaScriptStep($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0, 0, 0, 0);
        $scope.$emit('atualizaTituloPagina', 'Configurações de Reprocessamento de Step');
        $scope.ViewMain = 'Views/Sistema/ScriptStepReprocessamento/ListaScriptStepReprocessamento.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Configurações de Reprocessamento de Step');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ScriptStepReprocessamento/ListaScriptStepReprocessamento.html';
    $scope.ListaCliente();
    $scope.ListaScriptStep($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0, 0, 0, 0);
    $scope.ListaScriptCodigoRetorno(0);
    $scope.ListaScriptStepReprocessamento(0);

});

