﻿angular.module('automationApp').controller('ScriptController', function ($scope, $rootScope, ScriptService, ProcessoService, GrupoService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaScript = function (nIdScriptStatus, nIdScriptPrioridade) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ScriptService.ListaScriptPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }
    $scope.InsereScript = function () {
        try {
            var oScript = $scope.oScript;
            oScript.Ativo = true;
            oScript.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScript.DataCriacao = new Date();
            ScriptService.InsereScript(oScript).then(function successCallback(response) {
                $scope.Scripts.unshift(response.data.Retorno);
                delete $scope.oScript;
                $scope.$emit('atualizaTituloPagina', 'Scripts');
                $scope.ViewMain = 'Views/Sistema/Script/ListaScript.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo Script!");
        }
    }
    $scope.AtualizaScript = function () {
        try {
            var oScript = $scope.oScript;
            oScript.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScript.DataAlteracao = new Date();
            ScriptService.AtualizaScript(oScript.Id, oScript).then(function successCallback(response) {
                delete $scope.oScript;
                $scope.$emit('atualizaTituloPagina', 'Scripts');
                $scope.ViewMain = 'Views/Sistema/Script/ListaScript.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar Script!");
        }
    }

    // FUNCOES AUX
    $scope.ListaScriptStatus = function () {
        try {
            ScriptService.ListaScriptStatus().then(function successCallback(response) {
                $scope.ListaStatusScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de script!");
        }
    }
    $scope.ListaScriptPrioridade = function () {
        try {
            ScriptService.ListaScriptPrioridade().then(function successCallback(response) {
                $scope.ListaPrioridadeScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de script!");
        }
    }
    $scope.ListaProcessoClassificacaoPrioridade = function () {
        $scope.ClassificacoesPrioridade = {};
        try {
            ProcessoService.ListaProcessoClassificacaoPrioridade().then(function successCallback(response) {
                $scope.ClassificacoesPrioridade = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Processo!");
        }
    }
    $scope.ListaGrupo = function () {
        try {
            GrupoService.ListaGrupo().then(function successCallback(response) {
                $scope.Grupos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de grupo!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oScript = {};
        $scope.$emit('atualizaTituloPagina', 'Inserindo um novo script.');
        $scope.ViewMain = 'Views/Sistema/Script/InsereScript.html';
    }
    $scope.btnEditar = function () {
        var nIdScript = this.oScript.Id;
        try {
            ScriptService.RetornaScript(nIdScript, true, true, true, true, true, true, true).then(function successCallback(response) {
                $scope.oScript = response.data.Retorno;
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Script');
                $scope.ViewMain = 'Views/Sistema/Script/DetalheScript.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do Script!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oScript;
        $scope.$emit('atualizaTituloPagina', 'Scripts');
        $scope.ViewMain = 'Views/Sistema/Script/ListaScript.html';
    }
    $scope.btnFiltroPesquisaAvancada = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Script/PesquisaAvancadaScript.html',
            controller: 'ScriptPesquisaAvancadaController',
            size: sSize,
            backdrop: true,
            keyboard: true
        });
        modalInstance.result.then(function (oFiltroPesquisaAvancada) {
            $rootScope.loading = [true, 'Carregando'];
            var nIdCliente = 0;
            if (oFiltroPesquisaAvancada !== undefined && oFiltroPesquisaAvancada !== null) {
                if (oFiltroPesquisaAvancada.IdCliente !== undefined && oFiltroPesquisaAvancada.IdCliente !== null) {
                    nIdCliente = oFiltroPesquisaAvancada.IdCliente;
                }
            }
            //Pesquisa/Atualiza na base novamente.
            //$scope.ListaScript(nIdCliente, nIdPerfilAcesso);
            $rootScope.loading = [false];
        }, function () {

        });
    };

    //Titulo da Página
    $scope.$emit('atualizaTituloPagina', 'Scripts');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Script/ListaScript.html';
    
    $scope.ListaScriptStatus();
    $scope.ListaScriptPrioridade();
    $scope.ListaProcessoClassificacaoPrioridade();
    $scope.ListaScript(0, 0);

});

angular.module('automationApp').controller('ScriptPesquisaAvancadaController', function ($scope, $rootScope, $uibModalInstance, ClienteService) {

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        $scope.Clientes = {};
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de clientes!");
        }
    }

    //Init Dominio
    $scope.ListaCliente();

    //PESQUISAR
    $scope.btnPesquisar = function () {

        //Retorna Parametros da Pesquisa Avançada
        $uibModalInstance.close($scope.oFiltroPesquisaAvancada);
    };

    //FECHAR
    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };
});


