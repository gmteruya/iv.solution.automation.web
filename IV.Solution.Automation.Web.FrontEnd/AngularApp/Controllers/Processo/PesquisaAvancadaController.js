﻿angular.module('automationApp').controller('FiltroPesquisaAvancadaController', function ($scope, $rootScope, $uibModalInstance, ScriptService, GrupoService, ClienteService, ProcessoService, ScriptCodigoRetornoService) {

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes.push(response.data.Retorno);
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de cientes!";
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de cientes!";
                });
            }
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de cientes!";
        }
    }
    $scope.ListaGrupo = function (nIdCliente) {
        try {
            if (nIdCliente > 0) {
                GrupoService.ListaGrupoPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de grupos de processo!";
                });
            } else {
                $scope.Grupos = {};
            }
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de grupos de processo!";
        }
    }
    $scope.ListaScript = function (nIdGrupo) {
        try {
            if (nIdGrupo > 0) {
                ScriptService.ListaScriptPorGrupoRelacionado(nIdGrupo).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de tipos de processos!";
                });
            } else {
                $scope.Scripts = {};
            }
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de status de processo!";
        }
    }
    $scope.ListaHistoricoClassificacao = function () {
        try {
            ProcessoService.ListaHistoricoClassificacao().then(function successCallback(response) {
                $scope.ClassificacoesHistorico = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de classificações de histórico!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de classificações de histórico!";
        }
    }
    $scope.ListaInteracaoTipo = function () {
        try {
            ProcessoService.ListaInteracaoTipo().then(function successCallback(response) {
                $scope.TipoInteracoes = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de tipo de interacoes de processo!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de tipo de interacoes de processo!";
        }
    }
    $scope.ListaProcessoStatus = function () {
        try {
            ProcessoService.ListaProcessoStatus().then(function successCallback(response) {
                $scope.StatusProcessos = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de status de processo!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de status de processo!";
        }
    }
    $scope.ListaProcessoClassificacaoPrioridade = function () {
        try {
            ProcessoService.ListaProcessoClassificacaoPrioridade().then(function successCallback(response) {
                $scope.ClassificacoesPrioridade = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de tipos de Processo!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de status de Processo!";
        }
    }
    $scope.ListaScriptCodigoRetorno = function (nIdScript) {
        try {
            ScriptCodigoRetornoService.ListaScriptCodigoRetorno(nIdScript).then(function successCallback(response) {
                $scope.CodigosRetornos = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de codigos de retorno!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de codigos de retorno!";
        }
    }
    $scope.ControlaExibicaoPermissao = function (nIdGrupo, nIdScript, sEntidadeControlada, sPermissao) {
        var bExibeEntidade = false;
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente === 0) {
            bExibeEntidade = true;
        } else {
            var arPerfilAcessoScriptGrupo;
            arPerfilAcessoScriptGrupo = $rootScope.globals.Authentication.UsuarioSessao.Usuario.PerfilAcesso.PerfilAcessoScriptGrupo;
            if (sEntidadeControlada === 'Grupo') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
            if (sEntidadeControlada === 'Script') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo && oPerfilAcessoScriptGrupo.ScriptGrupo.IdScript === nIdScript) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
        };
        return bExibeEntidade;
    }
    
    //Init Dominio
    $scope.ListaHistoricoClassificacao();
    $scope.ListaInteracaoTipo();
    $scope.ListaProcessoStatus();
    $scope.ListaProcessoClassificacaoPrioridade();
    $scope.ListaScriptCodigoRetorno(0);
    $scope.ListaCliente();
    $scope.ListaGrupo($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);

    //Carrega label da chave do processo no scope.
    var sLabelChaveProcesso = 'Chave do Processo';
    if ($rootScope.globals != undefined && $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente != undefined) {
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'] != undefined) {
            sLabelChaveProcesso = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'];
        }
    }
    $scope.sLabelChaveProcesso = sLabelChaveProcesso;

    //PESQUISAR
    $scope.btnPesquisar = function () {
        
        //Retorna Parametros da Pesquisa Avançada
        $uibModalInstance.close($scope.oFiltroPesquisaAvancada);
    };

    //FECHAR
    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };

});
