﻿angular.module('automationApp').controller('ProcessoController', function ($scope, $rootScope, $routeParams, $uibModal, ProcessoService, ProcessoExternoService, ScriptService, GrupoService, ClienteService, ScriptCodigoRetornoService, LogAplicacaoService) {
    
    // FUNCOES ESPECIFICAS DA TELA
    $scope.init = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                $scope.EtapaInsereProcesso = { Titulo: 'Selecione o grupo', Etapa: 1, Visivel: true };
            } else {
                $scope.EtapaInsereProcesso = { Titulo: 'Selecione o cliente', Etapa: 0, Visivel: true };
            }            
        } catch (e) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Erro $scope.init=" + e.message);
        }
    };
    $scope.EtapaInsereProcesso_Click = function (Etapa) {
        try {
            $scope.EtapaInsereProcesso.Etapa = Etapa;
            switch (Etapa) {
                case 0:
                    $scope.EtapaInsereProcesso.Titulo = 'Selecione o cliente';
                    $scope.EtapaInsereProcesso.Visivel = true;
                    $scope.oProcesso.IdScript = 0;
                    $scope.oProcesso.Script = null;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
                    break;
                case 1:
                    $scope.EtapaInsereProcesso.Titulo = 'Selecione o grupo';
                    $scope.EtapaInsereProcesso.Visivel = true;
                    $scope.oProcesso.IdScript = 0;
                    $scope.oProcesso.Script = null;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
                    break;
                case 2:
                    $scope.EtapaInsereProcesso.Titulo = 'Selecione o tipo de processo';
                    $scope.EtapaInsereProcesso.Visivel = true;
                    $scope.oProcesso.IdScript = 0;
                    $scope.oProcesso.Script = null;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
                    break;
                case 3:
                    $scope.EtapaInsereProcesso.Titulo = '';
                    $scope.EtapaInsereProcesso.Visivel = false;
                    $scope.oProcesso.ChaveProcesso = null;
                    $scope.oProcesso.ScriptInfosAdicionais = {};
                    break;
            }
        } catch (e) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel selecionar o Card! params: nStage=" + Etapa + "|Error=" + e.message);
        };
    };
    $scope.ItemRetorno_Click = function (IdCliente, IdGrupo, IdScript, Etapa, Scripts) {
        try {
            if (Etapa == 0) {
                $scope.oProcesso.IdCliente = IdCliente;
                $scope.oProcesso.IdScript = 0;
                $scope.oProcesso.Script = null;
                $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
            }
            if (Etapa == 1) {
                $scope.oProcesso.IdGrupo = IdGrupo;
                $scope.oProcesso.IdScript = 0;
                $scope.oProcesso.Script = null;
                $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
            }
            $scope.oProcesso.IdScript = IdScript;
            $scope.oProcesso.ChaveProcesso = null;
            $scope.oProcesso.ScriptInfosAdicionais = {};
            $scope.EtapaInsereProcesso_Click(Etapa);
        } catch (e) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel selecionar o Card! params: nStage=" + Etapa + "|Error=" + e.message);
        };
    };

    //FUNCOES CRUD
    $scope.ListaProcesso = function (nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica, nIdImportacaoLote, sChaveProcesso, sServidorOrigem) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ProcessoService.ListaProcessoPorPerfilAcesso($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdPerfilAcesso, nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica, nIdImportacaoLote, sChaveProcesso, sServidorOrigem).then(function successCallback(response) {
                    $scope.Processos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ProcessoService.ListaProcesso(nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica, nIdImportacaoLote, sChaveProcesso, sServidorOrigem).then(function successCallback(response) {
                    $scope.Processos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de processo!");
        }
    }
    $scope.InsereProcesso = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando seu processo no robo do RPA!'];
        try {
            var oProcesso = $scope.oProcesso;
            oProcesso.IdProcessoStatus = 1; //Em Geração
            oProcesso.Ativo = true;
            oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oProcesso.DataCriacao = new Date();
            ProcessoService.InsereProcesso(oProcesso).then(function successCallback(response) {

                //Pega o Retorno
                oProcesso = response.data.Retorno;

                //Atualiza Scope
                $scope.oProcesso = oProcesso;

                if ($routeParams.stoken != undefined) {
                    $scope.AtualizaProcessoExterno(oProcesso.Id, 4);
                }

                //Trata Retorno
                if (response.data === null) {//Se retorna erro fatal
                    delete $scope.oProcesso;
                    $scope.error = response.data.MsgRetorno;
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal RPA Informa
                    $scope.showModalRPAInformaErroExecucao('lg');
                } else if (oProcesso === null) {//Se Processo retornar Nothing
                    delete $scope.oProcesso;
                    $scope.error = response.data.MsgRetorno;
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal RPA Informa
                    $scope.showModalRPAInformaErroExecucao('lg');
                } else if (oProcesso.IdProcessoStatus === 1) {//Em Geração
                    //Exibe Modal de Mensagem
                    $scope.showModalMensagemProcesso('lg', response.data.MsgRetorno);
                } else if (oProcesso.IdProcessoStatus === 2) {//Processo Gerado
                    //Detalhe do Processo
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                } else if (oProcesso.IdProcessoStatus === 3) {//Aguardando Execução
                    delete $scope.oProcesso;
                    $scope.ListaScript(0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
                    $scope.ListaProcesso(0, 0, 0, 0, 0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
                    $scope.$emit('atualizaTituloPagina', 'Processo - Consulta');
                    $scope.ViewMain = 'Views/Sistema/Processo/ListaProcesso.html';
                } else if (oProcesso.IdProcessoStatus === 5) {//Em Execução                     
                    //Detalhe do Processo
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal do Step
                    $scope.showModalScriptStep('lg', oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1], response.data.MsgRetorno);
                } else if (oProcesso.IdProcessoStatus === 6) {//Cancelado             
                    //Detalhe do Processo
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Exibe Mensagem
                    $scope.showModalMensagemProcesso('lg');
                } else if (oProcesso.IdProcessoStatus === 7) {//Excluido - Erro ao Inserir             
                    //Limpa Formulário
                    $scope.oProcesso.ChaveProcesso = null;
                    $scope.oProcesso.ScriptInfosAdicionais = {};
                    delete $rootScope.Arquivos;
                    //Exibe Mensagem
                    $scope.showModalMensagemProcesso('lg', response.data.MsgRetorno);
                } else if (oProcesso.IdProcessoStatus === 8 || oProcesso.IdProcessoStatus === 13) {//Executado com Sucesso - Executado com Sucesso Não Efetivo
                    //Detalhe do Processo
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Exibe Mensagem
                    $scope.showModalMensagemProcesso('lg');
                } else if (oProcesso.IdProcessoStatus === 9) {//Erro Execução
                    //Detalhe do Processo
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Exibe Mensagem
                    $scope.showModalMensagemProcesso('lg');
                }
                
                //Retira Lodear;
                $rootScope.loading = [false];
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
                $rootScope.loading = [false];
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo processo!");
            $rootScope.loading = [false];
        }      
    }
    $scope.AtualizaProcesso = function () {
        try {
            var IdProcesso = $scope.oProcesso.Id;
            var oProcesso = $scope.oProcesso;
            oProcesso.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oProcesso.DataAlteracao = new Date();
            ProcessoService.AtualizaProcesso(IdProcesso, oProcesso).then(function successCallback(response) {
                $scope.Processos = response.data.Retorno;
                $scope.oProcesso = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar Processo!");
        }
    }
    
    // FUNCOES AUX
    $scope.ListaCliente = function () {
        $scope.Clientes = {};
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de cientes!");
        }
    }
    $scope.ListaGrupo = function (nIdCliente) {
        $scope.Grupos = {};
        try {
            if (nIdCliente > 0) {
                GrupoService.ListaGrupoPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            } else {
                GrupoService.ListaGrupo().then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de grupos de processo!");
        }
    }
    $scope.ListaGrupoCliente = function (nIdCliente) {
        $scope.GruposCliente = {};
        try {
            if ($scope.oProcesso != undefined) {
                $scope.oProcesso.IdCliente = nIdCliente;
                $scope.EtapaInsereProcesso_Click(1);
            }
            if (nIdCliente > 0) {
                GrupoService.ListaGrupoPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.GruposCliente = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            } else {
                GrupoService.ListaGrupo().then(function successCallback(response) {
                    $scope.GruposCliente = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de grupos de processo do cliente relacionado!");
        }
    }
    $scope.ListaScript = function (nIdCliente) {
        $scope.Scripts = {};
        try {
            if (nIdCliente > 0) {
                ScriptService.ListaScriptPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            } else {
                ScriptService.ListaScript().then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaScriptGrupo = function (nIdGrupo, nIdCliente) {
        $scope.ScriptsGrupo = {};
        try {           
            $scope.oProcesso.IdGrupo = nIdGrupo;
            $scope.EtapaInsereProcesso_Click(2);
            ScriptService.ListaScriptPorGrupoRelacionado(nIdGrupo).then(function successCallback(response) {
                $scope.ScriptsGrupo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo do grupo relacionado!");
        }
    }
    $scope.ListaHistoricoClassificacao = function () {
        $scope.ListaClassificacoesHistorico = {};
        try {
            ProcessoService.ListaHistoricoClassificacao().then(function successCallback(response) {
                $scope.ListaClassificacoesHistorico = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de classificações de histórico!");
        }
    }
    $scope.ListaInteracaoTipo = function () {
        $scope.ListaInteracoesTipo = {};
        try {
            ProcessoService.ListaInteracaoTipo().then(function successCallback(response) {
                $scope.ListaInteracoesTipo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de tipo de interacoes de processo!");
        }
    }
    $scope.ListaProcessoStatus = function () {
        $scope.ListaStatusProcesso = {};
        try {
            ProcessoService.ListaProcessoStatus().then(function successCallback(response) {
                $scope.ListaStatusProcesso = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaProcessoClassificacaoPrioridade = function () {
        $scope.ClassificacoesPrioridade = {};
        try {
            ProcessoService.ListaProcessoClassificacaoPrioridade().then(function successCallback(response) {
                $scope.ClassificacoesPrioridade = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Processo!");
        }
    }
    $scope.ListaScriptCodigoRetorno = function (nIdScript) {
        $scope.CodigosRetornos = {};
        try {
            ScriptCodigoRetornoService.ListaScriptCodigoRetorno(nIdScript).then(function successCallback(response) {
                $scope.CodigosRetornos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de codigos de retorno!");
        }
    }
    $scope.ListaLogAplicacao = function (nIdCliente, nIdScript, nIdGrupo, nIdRobo, nIdProcesso, nIdProcessoExecucao, nIdProcessoExecucaoScriptStep) {
        $scope.LogAplicacaoProcesso = {};
        try {
            LogAplicacaoService.ListaLogAplicacao(nIdCliente, nIdScript, nIdGrupo, nIdRobo, nIdProcesso, nIdProcessoExecucao, nIdProcessoExecucaoScriptStep).then(function successCallback(response) {
                $scope.LogAplicacaoProcesso = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de logs do processo!");
        }
    }
    $scope.ControlaExibicaoPermissao = function (nIdGrupo, nIdScript, sEntidadeControlada, sPermissao) {
        var bExibeEntidade = false;
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente === 0) {
            bExibeEntidade = true;
        } else {
            var arPerfilAcessoScriptGrupo;
            arPerfilAcessoScriptGrupo = $rootScope.globals.Authentication.UsuarioSessao.Usuario.PerfilAcesso.PerfilAcessoScriptGrupo;
            if (sEntidadeControlada === 'Grupo') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo) {
                        switch(sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;                               
                        }                        
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
            if (sEntidadeControlada === 'Script') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo && oPerfilAcessoScriptGrupo.ScriptGrupo.IdScript === nIdScript) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
        };
        return bExibeEntidade;
    }

    //ABRE FORMULARIO
    $scope.RetornaScript = function (IdScript) {
        try {
            if (IdScript > 0) {               
                ScriptService.RetornaScript(IdScript, false, false, false, false, false, false, true).then(function successCallback(response) {
                    $scope.oProcesso.IdScript = IdScript;
                    $scope.oProcesso.Script = response.data.Retorno;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = $scope.oProcesso.Script.IdProcessoClassificacaoPrioridade;
                    $scope.EtapaInsereProcesso_Click(3);
                                        
                    $scope.InsereProcessoExterno();

                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            } else {
                $scope.oProcesso.Script = null;
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do Script!");
        }
    }
   
    //EVENTOS
    $scope.btnNovo = function () {
        delete $scope.oProcesso;
        $scope.oProcesso = {};
        $scope.oProcesso.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
        $scope.EtapaInsereProcesso_Click(1);
        $scope.ListaGrupoCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
        $scope.ScriptsGrupo = null;
        $scope.init();
        delete $rootScope.Arquivos;
        $rootScope.ControleSomenteLeitura = false;
        $scope.$emit('atualizaTituloPagina', 'Processo - Novo');
        $scope.ViewMain = 'Views/Sistema/Processo/InsereProcesso.html';
    }
    $scope.btnEditar = function () {
        try {
            var nIdProcesso = this.oProcesso.Id;
            var sAutor = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            ProcessoService.RetornaProcesso(nIdProcesso, sAutor, true, true, true, true, true, true, true, true, true, true).then(function successCallback(response) {
                $scope.oProcesso = response.data.Retorno;
                delete $rootScope.Arquivos;
                $rootScope.Arquivos = $scope.oProcesso.Arquivo;
                $rootScope.ControleSomenteLeitura = true;
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Processo de ' + $scope.oProcesso.Script.Descricao);
                $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            }).catch(function (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do Processo!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oProcesso;
        //$scope.ListaProcesso(0, 0, 0, 0, 0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
        $scope.$emit('atualizaTituloPagina', 'Processo - Consulta');
        $scope.ViewMain = 'Views/Sistema/Processo/ListaProcesso.html';
    }
    $scope.btnAbrirMaisInformacoes = function () {
        $('.divMaisInformacoes').show(200);
    }
    $scope.btnFecharMaisInformacoes = function () {
        $('.divMaisInformacoes').hide(200);
    }
    $scope.btnFiltroPesquisaAvancada = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/PesquisaAvancadaProcesso.html',
            controller: 'PesquisaAvancadaProcessoController',
            size: sSize,
            backdrop: true,
            keyboard: true,
            resolve: {
                oFiltroPesquisaAvancada: function () {
                    return $scope.oFiltroPesquisaAvancada;
                }
            }
        });
        modalInstance.result.then(function (oFiltroPesquisaAvancada) {
            $rootScope.loading = [true, 'Carregando'];
            var nIdProcessoStatus = 0;
            var nIdProcessoClassificacaoPrioridade = 0;
            var nIdProcessoPai = 0;
            var nIdScript = 0;
            var nIdGrupo = 0;
            var nIdCliente = 0;
            var nIdProcessoChaveUnica = 0;
            var nIdImportacaoLote = 0;
            var sChaveProcesso = undefined;
            var sServidorOrigem = undefined;
            
            if (oFiltroPesquisaAvancada !== undefined && oFiltroPesquisaAvancada !== null) {
                //Seta Pesquisa Avançada no Scope
                $scope.oFiltroPesquisaAvancada = oFiltroPesquisaAvancada;
                if (oFiltroPesquisaAvancada.IdProcessoStatus !== undefined && oFiltroPesquisaAvancada.IdProcessoStatus !== null) {
                    nIdProcessoStatus = oFiltroPesquisaAvancada.IdProcessoStatus;
                }
                if (oFiltroPesquisaAvancada.IdProcessoClassificacaoPrioridade !== undefined && oFiltroPesquisaAvancada.IdProcessoClassificacaoPrioridade !== null) {
                    nIdProcessoClassificacaoPrioridade = oFiltroPesquisaAvancada.IdProcessoClassificacaoPrioridade;
                }
                if (oFiltroPesquisaAvancada.IdProcessoPai !== undefined && oFiltroPesquisaAvancada.IdProcessoPai !== null) {
                    nIdProcessoPai = oFiltroPesquisaAvancada.IdProcessoPai;
                }
                if (oFiltroPesquisaAvancada.IdScript !== undefined && oFiltroPesquisaAvancada.IdScript !== null) {
                    nIdScript = oFiltroPesquisaAvancada.IdScript;
                }
                if (oFiltroPesquisaAvancada.IdGrupo !== undefined && oFiltroPesquisaAvancada.IdGrupo !== null) {
                    nIdGrupo = oFiltroPesquisaAvancada.IdGrupo;
                }
                if (oFiltroPesquisaAvancada.IdCliente !== undefined && oFiltroPesquisaAvancada.IdCliente !== null) {
                    nIdCliente = oFiltroPesquisaAvancada.IdCliente;
                }
                if (oFiltroPesquisaAvancada.IdProcessoChaveUnica !== undefined && oFiltroPesquisaAvancada.IdProcessoChaveUnica !== null) {
                    nIdProcessoChaveUnica = oFiltroPesquisaAvancada.IdProcessoChaveUnica;
                }
                if (oFiltroPesquisaAvancada.IdImportacaoLote !== undefined && oFiltroPesquisaAvancada.IdImportacaoLote !== null) {
                    nIdImportacaoLote = oFiltroPesquisaAvancada.IdImportacaoLote;
                }
                if (oFiltroPesquisaAvancada.ChaveProcesso !== undefined && oFiltroPesquisaAvancada.ChaveProcesso !== null) {
                    sChaveProcesso = oFiltroPesquisaAvancada.ChaveProcesso;
                }
                if (oFiltroPesquisaAvancada.ServidorOrigem !== undefined && oFiltroPesquisaAvancada.ServidorOrigem !== null) {
                    sServidorOrigem = oFiltroPesquisaAvancada.ServidorOrigem;
                }
            }           
            //Pesquisa/Atualiza na base novamente.
            $scope.ListaProcesso(nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica, nIdImportacaoLote, sChaveProcesso, sServidorOrigem);
            $rootScope.loading = [false];
        }, function () {

        });
    };

    //MODAL STEP DA EXECUCAO
    $scope.showModalScriptStep = function (sSize, oProcessoExecucaoScriptStep, sMsgRetorno) {
        //Pre
        var sController;
        if (!oProcessoExecucaoScriptStep.ScriptStep.Controller) {
            sController = 'ExecucaoStepProcessoController'
        }
        else {
            sController = oProcessoExecucaoScriptStep.ScriptStep.Controller
        }

        //Open
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: oProcessoExecucaoScriptStep.ScriptStep.TemplateUrl,
            controller: sController,
            size: sSize,
            backdrop: false,
            keyboard: false,
            resolve: {
                oProcesso: function () {
                    return $scope.oProcesso;
                },
                MsgRetorno: function () {
                    return sMsgRetorno;
                },
            }
        });
        modalInstance.result.then(function (oProcesso) {
            //Continua Execução
            $rootScope.loading = [true, 'Aguarde, estamos processando sua requisição ao robo!'];
            try {
                var oProcesso = $scope.oProcesso;
                oProcesso.IdProcessoStatus = 5; //Em Execução
                oProcesso.Ativo = true;
                oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oProcesso.DataCriacao = new Date();
                ProcessoService.ContinuaExecucaoProcesso(oProcesso).then(function successCallback(response) {
                    
                    //Pega o Retorno
                    oProcesso = response.data.Retorno;

                    //Atualiza Scope
                    $scope.oProcesso = oProcesso;

                    //Trata Status de Retorno
                    if (oProcesso.IdProcessoStatus === 5) {//Em Execução 
                        //Abre Modal do Step
                        $scope.showModalScriptStep('lg', oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1], response.data.MsgRetorno);
                    } else if (oProcesso.IdProcessoStatus === 8 || oProcesso.IdProcessoStatus === 13) {//Executado com Sucesso
                        //Exibe Modal de Mensagem
                        $scope.showModalMensagemProcesso('lg');
                    } else if (oProcesso.IdProcessoStatus === 9) {//Erro Execução
                        //Exibe Modal de Mensagem
                        $scope.showModalMensagemProcesso('lg');
                    }
                    
                    //Retira Lodear;
                    $rootScope.loading = [false];
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                    $rootScope.loading = [false];
                });
            } catch (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel possível executar seu processo!");
                $rootScope.loading = [false];
            }
        }, function () {
            //Cancela Execução
            $rootScope.loading = [true, 'Aguarde, estamos executando seu processo no robo do RPA!'];
            try {
                var oProcesso = $scope.oProcesso;
                oProcesso.IdProcessoStatus = 4; //Em Execução
                oProcesso.Ativo = true;
                oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oProcesso.DataCriacao = new Date();
                ProcessoService.CancelaExecucaoProcesso(oProcesso).then(function successCallback(response) {
                    //Pega o Retorno
                    oProcesso = response.data.Retorno;

                    //Atualiza Scope
                    $scope.oProcesso = oProcesso;

                    //Exibe Modal de Mensagem
                    $scope.showModalMensagemProcesso('lg');

                    //Retira Lodear;
                    $rootScope.loading = [false];
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                    $rootScope.loading = [false];
                });
            } catch (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel cancelar seu processo!");
                $rootScope.loading = [false];
            }

            $scope.oProcesso = $scope.oProcesso;
        });
    };

    //MODAL EXECUÇÂO
    $scope.showModalProcessoExecucao = function (sSize) {
        var oProcessoExecucao = this.oProcessoExecucao;
        oProcessoExecucao.Processo = $scope.oProcesso;
        $scope.oProcessoExecucao = oProcessoExecucao;
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/ProcessoExecucao/DetalheProcessoExecucao.html',
            controller: 'ProcessoExecucaoController',
            size: sSize,
            backdrop: true,
            keyboard: true,
            resolve: {
                oProcessoExecucao: function () {
                    return $scope.oProcessoExecucao;
                },
                ListaStatusProcesso: function () {
                    return $scope.ListaStatusProcesso;
                }
            }
        });
        modalInstance.result.then(function (oProcessoExecucao) {
            $rootScope.loading = [true, 'Carregando'];           
            $rootScope.loading = [false];
        }, function () {
            $rootScope.loading = [true, 'Carregando'];
            $rootScope.loading = [false];
        });
    };

    //MODAL ANOTACAO
    $scope.showModalNovaAnotacao = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/NovaAnotacaoProcesso.html',
            controller: 'AnotacaoProcessoController',
            size: sSize,
            backdrop: false,
            keyboard: false,
            resolve: {
                oProcesso: function () {
                    return $scope.oProcesso;
                }
            }
        });
        modalInstance.result.then(function (oProcesso) {
            //Salva Anotação
            try {
                var oProcesso = $scope.oProcesso;
                oProcesso.IdProcessoStatus = 4; //Em Execução
                oProcesso.Ativo = true;
                oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oProcesso.DataCriacao = new Date();

                ProcessoService.NovaAnotacao(oProcesso).then(function successCallback(response) {
                    //Pega o Retorno
                    oProcesso = response.data.Retorno;

                    //Atualiza Scope
                    $scope.oProcesso = oProcesso;
                    
                    //Retira Lodear;
                    $rootScope.loading = [false];
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                    $rootScope.loading = [false];
                });
            } catch (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo Processo!");
                $rootScope.loading = [false];
            }
        }, function () {
            //Cancela Anotação
            try {
                
            } catch (err) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo Processo!");
                $rootScope.loading = [false];
            }

            $scope.oProcesso = $scope.oProcesso;
        });
    };

    //MODAL MENSAGEM
    $scope.showModalMensagemProcesso = function (sSize, sMsgRetorno) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/MensagemProcesso.html',
            controller: 'MensagemProcessoController',
            size: sSize,
            backdrop: false,
            keyboard: false,
            resolve: {
                oProcesso: function () {
                    return $scope.oProcesso;
                },
                MsgRetorno: function () {
                    return sMsgRetorno;
                },
            }
        });       
    };

    //TRATAR PROCESSO EXTERNO
    $scope.RetornaProcessoExterno = function () {
        var oProcessoExterno = {};

        try {
            ProcessoExternoService.RetornaProcessoExterno(0, $routeParams.stoken).then(function successCallback(response) {

                if (response.data.Sucesso) {
                    //Pega o Retorno
                    oProcessoExterno = response.data.Retorno;

                    //Atualiza Scope
                    $rootScope.oProcessoExterno = oProcessoExterno;
                    $scope.oProcessoExterno = oProcessoExterno;

                    $scope.btnNovo();
                    $scope.ListaGrupoCliente(oProcessoExterno.IdCliente);
                    $scope.ListaScriptGrupo(oProcessoExterno.IdGrupo);
                    $scope.RetornaScript(oProcessoExterno.IdScript);

                }
                else {
                    $scope.Mensagem = response.data.MsgRetorno;
                }

                //Retira Loder;
                $rootScope.loading = [false];

            }, function errorCallback(response) {
                $scope.Mensagem = "Erro de callback: Não foi possivel encontrar o token!";
                $rootScope.TrataMensagemResponseRPA(response, false);
                $rootScope.loading = [false];
            });
        } catch (e) {
            $scope.Mensagem = "Não foi possivel encontrar o token!";
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel encontrar o token!");
            $rootScope.loading = [false];
        }
    };
    $scope.InsereProcessoExterno = function () {

        if ($routeParams.stoken != undefined) {

            $scope.oProcesso.ChaveProcesso = $scope.oProcessoExterno.ChaveProcesso;
            $scope.oProcesso.ScriptInfosAdicionais = $scope.oProcessoExterno.PropriedadesAdicionais;

            $scope.InsereProcesso();

        }

    };
    $scope.AtualizaProcessoExterno = function (nIdProcesso, nIdProcessoExternoStatus) {
        try {
            $scope.oProcessoExterno.IdProcesso = nIdProcesso;
            $scope.oProcessoExterno.IdProcessoExternoStatus = nIdProcessoExternoStatus;
            $scope.oProcessoExterno.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            $scope.oProcessoExterno.DataAlteracao = new Date();

            ProcessoExternoService.AtualizaProcessoExterno($scope.oProcessoExterno).then(function successCallback(response) {
                
                $scope.oProcessoExterno = response.data.Retorno;
                $rootScope.oProcessoExterno = $scope.oProcessoExterno

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar Processo Externo!");
        }
    };
 
    //LIMPA SCOPE
    delete $scope.oProcesso;
    
    //Carrega label da chave do processo no scope.
    var sLabelChaveProcesso = 'Chave do Processo';
    if ($rootScope.globals != undefined && $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente != undefined) {
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'] != undefined) {
            sLabelChaveProcesso = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'];
        }
    }
    $scope.sLabelChaveProcesso = sLabelChaveProcesso;

    // INICIALIZACAO
    $scope.ListaHistoricoClassificacao();
    $scope.ListaInteracaoTipo();
    $scope.ListaProcessoStatus();
    $scope.ListaProcessoClassificacaoPrioridade();
    $scope.ListaScriptCodigoRetorno(0);
    $scope.ListaCliente();
    $scope.ListaGrupo($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
    $scope.ListaScript(0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
    $scope.ListaProcesso(0, 0, 0, 0, 0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
    $scope.$emit('atualizaTituloPagina', 'Processo - Consulta');

    if ($routeParams.stoken === undefined) {
        $scope.ViewMain = 'Views/Sistema/Processo/ListaProcesso.html';
    }
    else {
        $scope.RetornaProcessoExterno();
    }

    

});

angular.module('automationApp').controller('PesquisaAvancadaProcessoController', function ($scope, $rootScope, $uibModalInstance, oFiltroPesquisaAvancada, ScriptService, GrupoService, ClienteService, ProcessoService, ScriptCodigoRetornoService) {
    //if (oFiltroPesquisaAvancada !== undefined && oFiltroPesquisaAvancada !== null) {
    //    $scope.oFiltroPesquisaAvancada = oFiltroPesquisaAvancada;
    //}

    // VARIAVEIS
    $scope.oFiltroPesquisaAvancada = {};
    if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
        $scope.oFiltroPesquisaAvancada.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
    }
   
    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de cientes!");
        }
    }
    $scope.ListaGrupo = function (nIdCliente) {
        try {
            if (nIdCliente > 0) {
                GrupoService.ListaGrupoPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                $scope.Grupos = {};
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de grupos de processo!");
        }
    }
    $scope.ListaScript = function (nIdGrupo) {
        try {
            if (nIdGrupo > 0) {
                ScriptService.ListaScriptPorGrupoRelacionado(nIdGrupo).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                $scope.Scripts = {};
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaHistoricoClassificacao = function () {
        try {
            ProcessoService.ListaHistoricoClassificacao().then(function successCallback(response) {
                $scope.ClassificacoesHistorico = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de classificações de histórico!");
        }
    }
    $scope.ListaInteracaoTipo = function () {
        try {
            ProcessoService.ListaInteracaoTipo().then(function successCallback(response) {
                $scope.TipoInteracoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de tipo de interacoes de processo!");
        }
    }
    $scope.ListaProcessoStatus = function () {
        try {
            ProcessoService.ListaProcessoStatus().then(function successCallback(response) {
                $scope.StatusProcessos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaProcessoClassificacaoPrioridade = function () {
        try {
            ProcessoService.ListaProcessoClassificacaoPrioridade().then(function successCallback(response) {
                $scope.ClassificacoesPrioridade = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Processo!");
        }
    }
    $scope.ListaScriptCodigoRetorno = function (nIdScript) {
        try {
            ScriptCodigoRetornoService.ListaScriptCodigoRetorno(nIdScript).then(function successCallback(response) {
                $scope.CodigosRetornos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de codigos de retorno!");
        }
    }
    $scope.ControlaExibicaoPermissao = function (nIdGrupo, nIdScript, sEntidadeControlada, sPermissao) {
        var bExibeEntidade = false;
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente === 0) {
            bExibeEntidade = true;
        } else {
            var arPerfilAcessoScriptGrupo;
            arPerfilAcessoScriptGrupo = $rootScope.globals.Authentication.UsuarioSessao.Usuario.PerfilAcesso.PerfilAcessoScriptGrupo;
            if (sEntidadeControlada === 'Grupo') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
            if (sEntidadeControlada === 'Script') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo && oPerfilAcessoScriptGrupo.ScriptGrupo.IdScript === nIdScript) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
        };
        return bExibeEntidade;
    }

    //INIT DOMINIOS
    $scope.ListaHistoricoClassificacao();
    $scope.ListaInteracaoTipo();
    $scope.ListaProcessoStatus();
    $scope.ListaProcessoClassificacaoPrioridade();
    $scope.ListaScriptCodigoRetorno(0);
    $scope.ListaCliente();
    $scope.ListaGrupo($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);

    //Carrega label da chave do processo no scope.
    var sLabelChaveProcesso = 'Chave do Processo';
    if ($rootScope.globals != undefined && $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente != undefined) {
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'] != undefined) {
            sLabelChaveProcesso = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'];
        }
    }
    $scope.sLabelChaveProcesso = sLabelChaveProcesso;

    //PESQUISAR
    $scope.btnPesquisar = function () {

        //Retorna Parametros da Pesquisa Avançada
        $uibModalInstance.close($scope.oFiltroPesquisaAvancada);
    };

    //FECHAR
    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

angular.module('automationApp').controller('ExecucaoStepProcessoController', function ($scope, $rootScope, $uibModalInstance, ProcessoExecucaoScriptStepService, oProcesso, MsgRetorno) {
    $scope.oProcesso = oProcesso;
    $scope.MsgRetorno = MsgRetorno;

    //Pega Execução Atual
    if (oProcesso.Script.ExecucaoInstantanea === true && oProcesso.ProcessoExecucao.length > 0) {
        $scope.oProcessoExecucaoAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1]
    }

    //Pega Step Atual
    $scope.oProcessoExecucaoScriptStepAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1]

    //Metodo Generico Para Parse strin to Json Object
    $scope.ParseJson = function (sJson) {
        return JSON.parse(sJson)
    };

    //Continua Execucao
    $scope.ok = function () {
        clearTimeout(timer);
        $uibModalInstance.close($scope.oProcesso);
    };

    //Cancela Execucao
    $scope.cancel = function () {
        clearTimeout(timer);
        $uibModalInstance.dismiss('cancel');
    };

    //Ping no servidor que confirma abertura da tela do assistente.
    var timer;
    $scope.sendPingProcessoExecucaoScriptStep = function () {

        //Prepara
        var oProcessoExecucaoScriptStepAtual = $scope.oProcessoExecucaoScriptStepAtual;
        oProcessoExecucaoScriptStepAtual.DataUltimoRegistroAtividadeAssistenteVirtual = new Date();
        oProcessoExecucaoScriptStepAtual.Ativo = true;
        oProcessoExecucaoScriptStepAtual.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
        oProcessoExecucaoScriptStepAtual.DataAlteracao = new Date();

        //Ping da Tela
        ProcessoExecucaoScriptStepService.ReceivePing(oProcessoExecucaoScriptStepAtual).then(function successCallback(response) {

            //Pega o Retorno
            oProcessoExecucaoScriptStepAtual = response.data.Retorno;

            //Atualiza Scope
            $scope.oProcessoExecucaoScriptStepAtual = oProcessoExecucaoScriptStepAtual;

            //Recursivo
            timer = setTimeout(function () { $scope.sendPingProcessoExecucaoScriptStep(); }, 30000);

        });
    };

    //Inicializa Ping Step    
    $scope.sendPingProcessoExecucaoScriptStep();

});

angular.module('automationApp').controller('MensagemProcessoController', function ($scope, $rootScope, $uibModalInstance, oProcesso, MsgRetorno, ScriptCodigoRetornoService) {
    $scope.oProcesso = oProcesso;
       
    //Trata Mensagem
    if (oProcesso.IdProcessoStatus === 1) {//Em Geração
        $scope.Titulo = 'Em Geração - Regras de Validação';
        $scope.Mensagem = MsgRetorno;
        // teste planilha de evolução (fatura)
        //$scope.Observacao = 'Resolva as regras e submeta novamente os dados para inclusão no RPA.';
        $scope.CssClass = 'bg-warning';
    } else if (oProcesso.IdProcessoStatus === 2) {//Processo Gerado
        $scope.Titulo = 'Processo gerado com sucesso';
        $scope.Mensagem = '';
        $scope.Observacao = '';
        $scope.CssClass = '';
    } else if (oProcesso.IdProcessoStatus === 3) {//Aguardando Execução
        $scope.Titulo = 'Processo aguardando execução';
        $scope.Mensagem = '';
        $scope.Observacao = '';
        $scope.CssClass = '';
    } else if (oProcesso.IdProcessoStatus === 6) {//Cancelado             
        $scope.Titulo = 'Processo cancelado';
        $scope.Mensagem = oProcesso.ConteudoAdicional;
        $scope.Observacao = '';
        $scope.CssClass = '';
    } else if (oProcesso.IdProcessoStatus === 7) {//Excluido - Erro ao Inserir             
        $scope.Titulo = 'Processo Excluído do RPA';
        $scope.Mensagem = MsgRetorno;
        $scope.Observacao = 'Valide o preenchimento do formulário e tente novamente.';
        $scope.CssClass = 'bg-warning';
    } else if (oProcesso.IdProcessoStatus === 8) {//Executado com Sucesso
        $scope.Titulo = 'Processo executado com sucesso';
        $scope.Mensagem = oProcesso.ConteudoAdicional;
        $scope.Observacao = '';
        $scope.CssClass = 'bg-success';
    } else if (oProcesso.IdProcessoStatus === 9) {//Erro Execução
        $scope.Titulo = 'Erro na execução do processo';
        $scope.Mensagem = oProcesso.ConteudoAdicional;
        $scope.Observacao = 'Não é necessária uma nova execução. O protocolo será tratado pela equipe de segundo nível.';
        $scope.CssClass = 'bg-danger';
    } else if (oProcesso.IdProcessoStatus === 13) {//Executado com Sucesso Não Efetivo
        $scope.Titulo = 'Processo executado com sucesso não efetivo';
        $scope.Mensagem = oProcesso.ConteudoAdicional;
        $scope.Observacao = 'Não é necessária uma nova execução. O protocolo será tratado pela equipe de segundo nível.';
        $scope.CssClass = 'bg-danger';
    }

    //Fecha Modal
    $scope.fechar = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

angular.module('automationApp').controller('AnotacaoProcessoController', function ($scope, $rootScope, $uibModalInstance, oProcesso) {
    $scope.oProcesso = oProcesso;

    //Inicializa Modal
    $scope.oHistoricoOcorrencia = {};

    //Salvar Anotacao
    $scope.SalvarAnotacao = function () {

        //Preenche Nova Anotação
        $scope.oHistoricoOcorrencia.IdHistoricoClassificacao = 2; //Anotações Gerais
        $scope.oHistoricoOcorrencia.IdProcesso = $scope.oProcesso.Id;
        $scope.oHistoricoOcorrencia.IdProcessoExecucao = 0;
        $scope.oHistoricoOcorrencia.Conteudo = $scope.oHistoricoOcorrencia.Conteudo;
        $scope.oHistoricoOcorrencia.IdStatusDe = 0;
        $scope.oHistoricoOcorrencia.IdStatusPara = 0;
        $scope.oHistoricoOcorrencia.Ativo = true;
        $scope.oHistoricoOcorrencia.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
        $scope.oHistoricoOcorrencia.DataCriacao = new Date();

        //Retorna Anotação para a controller principal.              
        $uibModalInstance.close($scope.oHistoricoOcorrencia);
    };

    //Cancela Anotacao
    $scope.CancalerAnotacao = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
