﻿angular.module('automationApp').controller('ProcessoExternoController', function ($scope, $rootScope, $routeParams, $cookies, $timeout, UtilService, AuthenticationFactory, ProcessoExternoService, UsuarioService, UsuarioSessaoService) {

    var oProcessoExterno = {};

    $scope.oProcessoExterno = {};
    $rootScope.oProcessoExterno = {};

    $scope.Mensagem = "";

    $scope.ValidaToken = function () {
        var sMensagem = '';

        try {
            ProcessoExternoService.ValidaTokenProcessoExterno($routeParams.stoken).then(function successCallback(response) {

                if (response.data.Sucesso) {
                    //Pega o Retorno
                    oProcessoExterno = response.data.Retorno;

                    //Atualiza Scope
                    $rootScope.oProcessoExterno = oProcessoExterno;
                    $scope.oProcessoExterno = oProcessoExterno;
                    $scope.Mensagem = "";

                    $scope.Login();

                }
                else {
                    $scope.Mensagem = response.data.MsgRetorno;
                }

                //Retira Loder;
                $rootScope.loading = [false];

            }, function errorCallback(response) {
                $scope.Mensagem = "Erro de callback: Não foi possivel encontrar o token!";
                $rootScope.TrataMensagemResponseRPA(response, false);
                $rootScope.loading = [false];
            });
        } catch (e) {
            $scope.Mensagem = "Não foi possivel encontrar o token!";
            //$rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel encontrar o token!");
            //$rootScope.loading = [false];
        }
    };

    $scope.Login = function () {
        var sUsuario = '';
        var sSenha = '';
        var sMensagem = '';

        try {

            //Limpa Credenciais
            AuthenticationFactory.ClearCredentials();

            if ($scope.oProcessoExterno.Usuario.Origem == 'I') {
                sUsuario = $scope.oProcessoExterno.Usuario.Dominio + '\\' + $scope.oProcessoExterno.Usuario.Login;
            }
            else {
                sUsuario = $scope.oProcessoExterno.Usuario.Login;
            }

            sSenha = $scope.oProcessoExterno.Senha;

            //Login
            UsuarioService.Login(sUsuario, sSenha).then(function (success_login) {

                //Armazena retorno do login
                $scope.usuario_codigo_retorno = success_login.data.usuario_codigo_retorno;

                //Retorna a Sessão do Usuário
                UsuarioSessaoService.RetornaUsuarioSessao(success_login.data.id_usuario_sessao, success_login.data.access_token).then(function (usuario_sessao) {

                    // Seta as Credenciais
                    AuthenticationFactory.SetCredentials(success_login.data, usuario_sessao.data.Retorno);

                    //Se codigo de retorno do login for igual a 2 - OK, faz o direcionamento, se não mostra opções de troca de senha.
                    if ($scope.usuario_codigo_retorno === '2' || $scope.usuario_codigo_retorno === 2) {

                        //Redireciona para area logada.
                        //UtilService.DOM.PageRedirect('IndexLogged.html');
                        UtilService.DOM.PageRedirect('IndexLogged.html#/Processo/' + $routeParams.stoken);

                    } else if ($scope.usuario_codigo_retorno === '3' || $scope.usuario_codigo_retorno === 3) {

                        //Solicita nova senha.
                        sMensagem = 'Crie uma nova senha.';

                    } else if ($scope.usuario_codigo_retorno === '4' || $scope.usuario_codigo_retorno === 4) {

                        //Solicita nova senha.
                        sMensagem = 'Senha expirada. Troque sua senha.';

                    } else if ($scope.usuario_codigo_retorno === '5' || $scope.usuario_codigo_retorno === 5) {

                        //Solicita nova senha.
                        sMensagem = 'Crie uma nova senha.';

                    }
                }, function (error) {
                    sMensagem = 'Erro ao carregar a sessão do usuário.';
                });
            }, function (error) {
                if (error.data != null && error.data.error_description != null) {
                    sMensagem = error.data.error_description;
                }
                else {
                    sMensagem = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
                }
            });
        } catch (e) {
            sMensagem = 'Servidor RPA em manutenção. Tente novamente em alguns minutos.';
        }

        return sMensagem;
    };

    $scope.ValidaToken();

});