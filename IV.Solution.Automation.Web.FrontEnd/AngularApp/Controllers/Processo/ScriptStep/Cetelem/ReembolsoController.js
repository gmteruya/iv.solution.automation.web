﻿angular.module('automationApp').controller('ReembolsoController', function ($scope, $rootScope, $uibModalInstance, ProcessoExecucaoScriptStepService, oProcesso, MsgRetorno) {
    $scope.oProcesso = oProcesso;
    $scope.MsgRetorno = MsgRetorno;

    //Pega Execução Atual
    if (oProcesso.Script.ExecucaoInstantanea === true && oProcesso.ProcessoExecucao.length > 0) {
        $scope.oProcessoExecucaoAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1]
    }

    //Pega Step Atual
    $scope.oProcessoExecucaoScriptStepAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1]

    //Metodo Generico Para Parse strin to Json Object
    $scope.ParseJson = function (sJson) {
        return JSON.parse(sJson)
    };

    //Continua Execucao
    $scope.ok = function () {
        clearTimeout(timer);
        $uibModalInstance.close($scope.oProcesso);
    };

    //Cancela Execucao
    $scope.cancel = function () {
        clearTimeout(timer);
        $uibModalInstance.dismiss('cancel');
    };

    $scope.selecionarTodosProdutos = function () {
        var vlrCalculado = 0.0;
        var vlrProduto = 0.0;
        alert('Entrou');
        if ($scope.selecionarTodosProdutos) {
            $scope.selecionarTodosProdutos = true;
        }
        else {
            $scope.selecionarTodosProdutos = false;
        }
        alert($scope.selecionarTodosProdutos);
        angular.forEach($scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ListaProduto, function (produto) {

            if (produto.PermiteQuitar) {

                produto.isSelected = $scope.selecionarTodosProdutos;

                if (produto.isSelected) {

                    vlrProduto = produto.ValorAmortizado;
                    vlrCalculado += vlrProduto;

                }
            }
        });

        if ($scope.selecionarTodosProdutos === false) {
            vlrCalculado = 0.0;
        }

        vlrCalculado = Number.parseFloat(vlrCalculado).toFixed(2);
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ValorTotalSelecionado = vlrCalculado;
    };

    //Ping no servidor que confirma abertura da tela do assistente.
    var timer;
    $scope.sendPingProcessoExecucaoScriptStep = function () {

        //Prepara
        var oProcessoExecucaoScriptStepAtual = $scope.oProcessoExecucaoScriptStepAtual;
        oProcessoExecucaoScriptStepAtual.DataUltimoRegistroAtividadeAssistenteVirtual = new Date();
        oProcessoExecucaoScriptStepAtual.Ativo = true;
        oProcessoExecucaoScriptStepAtual.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
        oProcessoExecucaoScriptStepAtual.DataAlteracao = new Date();

        //Ping da Tela
        ProcessoExecucaoScriptStepService.ReceivePing(oProcessoExecucaoScriptStepAtual).then(function successCallback(response) {

            //Pega o Retorno
            oProcessoExecucaoScriptStepAtual = response.data.Retorno;

            //Atualiza Scope
            $scope.oProcessoExecucaoScriptStepAtual = oProcessoExecucaoScriptStepAtual;

            //Recursivo
            timer = setTimeout(function () { $scope.sendPingProcessoExecucaoScriptStep(); }, 30000);

        });
    };

    //Inicializa Ping Step    
    $scope.sendPingProcessoExecucaoScriptStep();

    $scope.somaExtratoSelecionados = function () {
        var vlrCalculado = 0.0;
        var nQtdeTodu = 0;
        var lista;

        lista = $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ExtratoBancarioAssistente;

        for (var i = lista.length - 1; i >= 0; i--) {
            
            if (lista[i].IsTODU) {
                nQtdeTodu += 1;
                if (nQtdeTodu == 1) {
                    vlrCalculado += lista[i].Valor;
                }
                else {
                    vlrCalculado += lista[i].ValorSeguro;
                }
            }
            else {
                if (lista[i].isSelected && nQtdeTodu > 0 && lista[i].PermiteCalcular) {
                    vlrCalculado += lista[i].Valor;
                }
            }

        }
                
        vlrCalculado = parseFloat(vlrCalculado).toFixed(2);
        
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ValorReembolso = parseFloat(vlrCalculado).toFixed(2);

        if (vlrCalculado < 0) {
            $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ReembolsarCliente = true;
        }
        else {
            $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ReembolsarCliente = false;
        }

        return true;
    };

});
