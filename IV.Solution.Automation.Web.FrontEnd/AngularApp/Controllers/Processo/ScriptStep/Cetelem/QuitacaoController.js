﻿angular.module('automationApp').controller('QuitacaoController', function ($scope, $rootScope, $uibModalInstance, ProcessoExecucaoScriptStepService, oProcesso, MsgRetorno) {
    $scope.oProcesso = oProcesso;
    $scope.MsgRetorno = MsgRetorno;

    //Pega Execução Atual
    if (oProcesso.Script.ExecucaoInstantanea === true && oProcesso.ProcessoExecucao.length > 0) {
        $scope.oProcessoExecucaoAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1]
    }

    //Pega Step Atual
    $scope.oProcessoExecucaoScriptStepAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1]

    //Metodo Generico Para Parse strin to Json Object
    $scope.ParseJson = function (sJson) {
        return JSON.parse(sJson)
    };
    
    //Continua Execucao
    $scope.ok = function () {
        clearTimeout(timer);
        $uibModalInstance.close($scope.oProcesso);
    };

    //Cancela Execucao
    $scope.cancel = function () {
        clearTimeout(timer);
        $uibModalInstance.dismiss('cancel');
    };

    $scope.selectAllItens = function () {

        if ($scope.selectAll) {
            $scope.selectAll = false;
        }
        else {
            $scope.selectAll = true;
        }

        angular.forEach($scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ListaProduto, function (produto) {

            if (produto.PermiteQuitar) {

                produto.isSelected = $scope.selectAll;
            }

        });

        $scope.somaProdutosSelecionados();

    };

    //Ping no servidor que confirma abertura da tela do assistente.
    var timer;
    $scope.sendPingProcessoExecucaoScriptStep = function () {

        //Prepara
        var oProcessoExecucaoScriptStepAtual = $scope.oProcessoExecucaoScriptStepAtual;
        oProcessoExecucaoScriptStepAtual.DataUltimoRegistroAtividadeAssistenteVirtual = new Date();
        oProcessoExecucaoScriptStepAtual.Ativo = true;
        oProcessoExecucaoScriptStepAtual.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
        oProcessoExecucaoScriptStepAtual.DataAlteracao = new Date();

        //Ping da Tela
        ProcessoExecucaoScriptStepService.ReceivePing(oProcessoExecucaoScriptStepAtual).then(function successCallback(response) {

            //Pega o Retorno
            oProcessoExecucaoScriptStepAtual = response.data.Retorno;

            //Atualiza Scope
            $scope.oProcessoExecucaoScriptStepAtual = oProcessoExecucaoScriptStepAtual;

            //Recursivo
            timer = setTimeout(function () { $scope.sendPingProcessoExecucaoScriptStep(); }, 30000);

        });
    };

    //Inicializa Ping Step    
    $scope.sendPingProcessoExecucaoScriptStep();

    $scope.somaProdutosSelecionados = function () {
        var vlrCalculado = 0.0;
        var vlrProduto = 0.0;

        var lista;
        var qtdeProdutos;
        
        lista = $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ListaProduto;
        qtdeProdutos = lista.length;

        for (var i = 0; i < qtdeProdutos; i++) {
            if (lista[i].isSelected) {

                vlrProduto = lista[i].ValorAmortizado;
                vlrCalculado += vlrProduto;

            }
        }

        vlrCalculado = Number.parseFloat(vlrCalculado).toFixed(2);

        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ValorTotalSelecionado = vlrCalculado;

        return true;
    };

    $scope.exibeParcial = function (sTipo) {
        if (sTipo == 'Total') {
            $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ExibirOpcaoTotalDetalhe = true;
            $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ExibirOpcaoParcialDetalhe = false;
        }
        else {
            $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ExibirOpcaoTotalDetalhe = false;
            $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ExibirOpcaoParcialDetalhe = true;
        }
    };

    $scope.recalculaCrediario = function (dataSelecionada) {
        var dataHoje = new Date();
        var qtdeDias = 0;
        var percentual = 22.99 / 100;
        var tperm = 0.0;

        var vlrCrediarioCalculadoAmortizado = 0.0;
        var vlrCrediarioCalculadoSemAmortizado = 0.0;

        var vlrCrediarioAmortizado = 0.0;
        var vlrCrediarioSemAmortizado = 0.0;

        var vlrJuros = 0.0;

        dataHoje.setHours(0, 0, 0, 0);
        qtdeDias = Math.round((dataSelecionada - dataHoje) / (1000 * 60 * 60 * 24));

        vlrCrediarioAmortizado = $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ValorCrediarioAmortizado;
        vlrCrediarioAmortizado = Number.parseFloat(vlrCrediarioAmortizado).toFixed(2);

        vlrCrediarioSemAmortizado = $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ValorCrediarioSemAmortizado;
        vlrCrediarioSemAmortizado = Number.parseFloat(vlrCrediarioSemAmortizado).toFixed(2);

        if (qtdeDias > 0) {
            tperm = 1 + ((percentual / 30) * qtdeDias);
            if (tperm > 2) {
                tperm = 2.0;
            }

            tperm = Number.parseFloat(tperm).toFixed(4);

            vlrCrediarioCalculadoAmortizado = tperm * vlrCrediarioAmortizado;
            vlrCrediarioCalculadoAmortizado = Number.parseFloat(vlrCrediarioCalculadoAmortizado).toFixed(2);

            vlrCrediarioCalculadoSemAmortizado = tperm * vlrCrediarioSemAmortizado;
            vlrCrediarioCalculadoSemAmortizado = Number.parseFloat(vlrCrediarioCalculadoSemAmortizado).toFixed(2);

            vlrJuros = vlrCrediarioCalculadoAmortizado - vlrCrediarioAmortizado;
        }
        else {
            vlrCrediarioCalculadoAmortizado = vlrCrediarioAmortizado;
            vlrCrediarioCalculadoSemAmortizado = vlrCrediarioSemAmortizado;
            vlrJuros = 0.0;
        }

        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ValorTotalAmortizado = vlrCrediarioCalculadoAmortizado;
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ValorTotalSemAmortizado = vlrCrediarioCalculadoSemAmortizado;
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.DataVencimentoSelecionado = dataSelecionada;
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ValorTotalJurosPorAtraso = vlrJuros;
    };

    $scope.somaComprasAVista = function () {
        var total = 0;
        var lista;

        lista = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ListaComprasAVista;

        for (var i = 0; i < lista.length; i++) {
            var compra = lista[i];
            total += compra.Debito;
        }

        total = Number.parseFloat(total).toFixed(2);

        return total;
    }

    $scope.confirmar = function() {
        oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ExibirConfirmacao = true;
    }

    $scope.desconfirmar = function () {
        oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ExibirConfirmacao = false;
    }

    $scope.clear = function () {
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.DataVencimento = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true,
        language: 'pt-BR'
    };

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1,
        language: 'pt-BR'
    };

    function disabled(data) {
        var date = data.date,
          mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    };

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.DataVencimento = new Date(year, month, day);
    };

    $scope.formats = ['dd/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['d!/M!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
          date: tomorrow,
          status: 'full'
      },
      {
          date: afterTomorrow,
          status: 'partially'
      }
    ];

    function getDayClass(data) {
        var date = data.date,
          mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

});
