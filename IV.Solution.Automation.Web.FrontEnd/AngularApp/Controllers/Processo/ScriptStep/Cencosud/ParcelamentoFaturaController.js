﻿angular.module('automationApp').controller('ParcelamentoFaturaController', function ($scope, $rootScope, $uibModalInstance, ProcessoExecucaoScriptStepService, oProcesso, MsgRetorno) {
    $scope.oProcesso = oProcesso;
    $scope.MsgRetorno = MsgRetorno;
    $scope.lstListaExtrato;
    $scope.ValorParcela
    $scope.Antecipacao;
    $scope.NumeroParcelasCliente;
    

    // Init variáveis para o template
    $scope.ParseInit = function () {
        oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais['ClienteSolicitaExtrato'] = false


        if (oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.Antecipacao != null) {
            $scope.Antecipacao = parseFloat(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.Antecipacao.replace(',', '.'));
            $scope.Antecipacao = $scope.Antecipacao.toFixed(2);
        }

        if (oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.NumeroParcelasCliente != null) {
            $scope.NumeroParcelasCliente = parseInt(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.NumeroParcelasCliente);
        }

        if (oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.SaldoFinanciamento != null) {
            $scope.SaldoFinanciamento = parseFloat(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.SaldoFinanciamento.replace(',', '.'));
            $scope.SaldoFinanciamento = $scope.SaldoFinanciamento.toFixed(2);
        }

        if (oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PagamentoMinimo != null) {
            $scope.PagamentoMinimo = parseFloat(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PagamentoMinimo.replace(',', '.'));
            $scope.PagamentoMinimo = $scope.PagamentoMinimo.toFixed(2);
        }

        if (oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.MaximoParcelas != null) {
            $scope.MaximoParcelas = parseInt(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.MaximoParcelas);
        }

        if (oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.MinimoParcelas != null) {
            $scope.MinimoParcelas = parseInt(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.MinimoParcelas);
        }

        if ($scope.PagamentoMinimo != null && $scope.SaldoFinanciamento != null) {
            $scope.ValorFatura = Math.round((parseFloat($scope.PagamentoMinimo) + parseFloat($scope.SaldoFinanciamento)) * 100) / 100;
            $scope.ValorFatura = $scope.ValorFatura.toFixed(2);
        }

        if(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ParcelamentoFatura != 'null'){
            $scope.parcelamentoFatura = $scope.ParseJson(oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ParcelamentoFatura);
            $scope.ValorParcela = Math.round($scope.parcelamentoFatura.ValorTotal * 100 / $scope.parcelamentoFatura.NumeroFaturas) / 100;
            $scope.ValorParcela = $scope.ValorParcela.toFixed(2);

        }
    }

    //Pega Execução Atual
    if (oProcesso.Script.ExecucaoInstantanea === true && oProcesso.ProcessoExecucao.length > 0) {
        $scope.oProcessoExecucaoAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1]
    }

    //Pega Step Atual
    $scope.oProcessoExecucaoScriptStepAtual = oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1]

    //Metodo Generico Para Parse strin to Json Object
    $scope.ParseJson = function (sJson) {
        return JSON.parse(sJson)
    };

    //Continua Execucao
    $scope.ok = function () {
        clearTimeout(timer);
        $uibModalInstance.close($scope.oProcesso);
    };

    //Continua Execucao com Novo Calculo Parcelamento
    $scope.ok_calculo = function () {
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.Antecipacao = $scope.TrataValor($("#inputAntecipacao").val());
        $scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.NumeroParcelasCliente = $scope.TrataValor($("#inputNumeroParcelas").val());
        $scope.ok();
    }

    //Cancela Execucao
    $scope.cancel = function () {
        clearTimeout(timer);
        $uibModalInstance.dismiss('cancel');
    };

    $scope.selectAllItens = function () {

        if ($scope.selectAll) {
            $scope.selectAll = false;
        }
        else {
            $scope.selectAll = true;
        }

        angular.forEach($scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.PlanoQuitacao.ProdutoQuitacao.ListaProduto, function (produto) {

            if (produto.PermiteQuitar) {

                produto.isSelected = $scope.selectAll;
            }

        });

        $scope.somaProdutosSelecionados();

    };

    //Ping no servidor que confirma abertura da tela do assistente.
    var timer;
    $scope.sendPingProcessoExecucaoScriptStep = function () {

        //Prepara
        var oProcessoExecucaoScriptStepAtual = $scope.oProcessoExecucaoScriptStepAtual;
        oProcessoExecucaoScriptStepAtual.DataUltimoRegistroAtividadeAssistenteVirtual = new Date();
        oProcessoExecucaoScriptStepAtual.Ativo = true;
        oProcessoExecucaoScriptStepAtual.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
        oProcessoExecucaoScriptStepAtual.DataAlteracao = new Date();

        //Ping da Tela
        ProcessoExecucaoScriptStepService.ReceivePing(oProcessoExecucaoScriptStepAtual).then(function successCallback(response) {

            //Pega o Retorno
            oProcessoExecucaoScriptStepAtual = response.data.Retorno;

            //Atualiza Scope
            $scope.oProcessoExecucaoScriptStepAtual = oProcessoExecucaoScriptStepAtual;

            //Recursivo
            timer = setTimeout(function () { $scope.sendPingProcessoExecucaoScriptStep(); }, 30000);

        });
    };

    //Inicializa Ping Step    
    $scope.sendPingProcessoExecucaoScriptStep();

    
    $scope.ParseListaExtrato = function() {
        $scope.lstListaExtrato;

        if ($scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ListaExtrato != null) {
            console.log(JSON.parse($scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ListaExtrato));
            $scope.lstListaExtrato = JSON.parse($scope.oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ScriptInfosAdicionais.ListaExtrato);

            console.log(Object.entries($scope.lstListaExtrato));
            $scope.lstListaExtrato = Object.entries($scope.lstListaExtrato);
        };
    }

    $scope.confirmar = function () {
        $scope.mostrarConfirmacao = true;
    }

    $scope.desconfirmar = function () {
        $scope.mostrarConfirmacao= false;
    }

    $scope.TrataValor = function (valor) {
        valor = valor.replace('.', ',');

        return valor;

    }

});
