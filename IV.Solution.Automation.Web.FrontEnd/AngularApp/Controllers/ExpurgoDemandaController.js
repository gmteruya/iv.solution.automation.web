﻿angular.module('automationApp').controller('ExpurgoDemandaController', function ($scope, $rootScope, ExpurgoDemandaService, ExpurgoDetalheService) {

    //FUNCOES
    $scope.ListaExpurgoDemanda = function (nIdExpurgoDetalhe, nIdExpurgoStatus) {
        try {
            ExpurgoDemandaService.ListaExpurgoDemanda(nIdExpurgoDetalhe, nIdExpurgoStatus).then(function successCallback(response) {
                $scope.ExpurgoDemandas = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de demandas de expurgo de dados!");
        }
    }
    $scope.InsereExpurgoDemanda = function () {
        try {
            var oExpurgoDemanda = $scope.oExpurgoDemanda;
            oExpurgoDemanda.Ativo = true;
            oExpurgoDemanda.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oExpurgoDemanda.DataCriacao = new Date();
            ExpurgoDemandaService.InsereExpurgoDemanda(oExpurgoDemanda).then(function successCallback(response) {
                $scope.ExpurgoDemandas.unshift(response.data.Retorno);
                delete $scope.oExpurgoDemanda;
                $scope.$emit('atualizaTituloPagina', 'Demandas de Expurgo de Dados');
                $scope.ViewMain = 'Views/Sistema/ExpurgoDemanda/ListaExpurgoDemanda.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ExpurgoDemanda!");
        }
    }
    $scope.AtualizaExpurgoDemanda = function () {
        try {
            var IdExpurgoDemanda = $scope.oExpurgoDemanda.Id;
            var oExpurgoDemanda = $scope.oExpurgoDemanda;
            oExpurgoDemanda.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oExpurgoDemanda.DataAlteracao = new Date();
            ExpurgoDemandaService.AtualizaExpurgoDemanda(IdExpurgoDemanda, oExpurgoDemanda).then(function successCallback(response) {
                $scope.ExpurgoDemandas = response.data.Retorno;
                $scope.oExpurgoDemanda = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ExpurgoDemanda!");
        }
    }

    // FUNCOES AUX
    $scope.ListaExpurgoStatus = function () {
        $scope.ListaStatusExpurgo = {};
        try {
            ExpurgoDetalheService.ListaExpurgoStatus().then(function successCallback(response) {
                $scope.ListaStatusExpurgo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Expurgo!");
        }
    }
    $scope.ListaExpurgoDetalhe = function () {
        try {
            ExpurgoDetalheService.ListaExpurgoDetalhe().then(function successCallback(response) {
                $scope.ExpurgoDetalhes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ExpurgoDetalhe!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oExpurgoDemanda = {};
        $scope.ViewMain = 'Views/Sistema/ExpurgoDemanda/InsereExpurgoDemanda.html';
    }
    $scope.btnEditar = function () {
        var nIdExpurgoDemanda = this.oExpurgoDemanda.Id;
        try {
            ExpurgoDemandaService.RetornaExpurgoDemanda(nIdExpurgoDemanda, true, true, true, true).then(function successCallback(response) {
                $scope.oExpurgoDemanda = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ExpurgoDemanda/DemandaExpurgoDemanda.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ExpurgoDemanda!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oExpurgoDemanda;
        $scope.$emit('atualizaTituloPagina', 'Demandas de Expurgo de Dados');
        $scope.ViewMain = 'Views/Sistema/ExpurgoDemanda/ListaExpurgoDemanda.html';
    }
    $scope.btnFiltroPesquisaAvancada = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/ExpurgoDemanda/PesquisaAvancadaExpurgoDemanda.html',
            controller: 'ExpurgoDemandaPesquisaAvancadaController',
            size: sSize,
            backdrop: true,
            keyboard: true
        });
        modalInstance.result.then(function (oFiltroPesquisaAvancada) {
            $rootScope.loading = [true, 'Carregando'];
            var nIdExpurgoDetalhe = 0;
            var nIdExpurgoStatus = 0;
            if (oFiltroPesquisaAvancada !== undefined && oFiltroPesquisaAvancada !== null) {
                if (oFiltroPesquisaAvancada.IdExpurgoDetalhe !== undefined && oFiltroPesquisaAvancada.IdExpurgoDetalhe !== null) {
                    nIdExpurgoDetalhe = oFiltroPesquisaAvancada.IdExpurgoDetalhe;
                }
                if (oFiltroPesquisaAvancada.IdExpurgoStatus !== undefined && oFiltroPesquisaAvancada.IdExpurgoStatus !== null) {
                    nIdExpurgoStatus = oFiltroPesquisaAvancada.IdExpurgoStatus;
                }
            }
            //Pesquisa/Atualiza na base novamente.
            $scope.ListaUsuario(nIdExpurgoDetalhe, nIdExpurgoStatus);
            $rootScope.loading = [false];
        }, function () {

        });
    };

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Demandas de Expurgo de Dados');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ExpurgoDemanda/ListaExpurgoDemanda.html';
    $scope.ListaExpurgoStatus();
    $scope.ListaExpurgoDetalhe();
    $scope.ListaExpurgoDemanda(0, 0);

});

angular.module('automationApp').controller('ExpurgoDemandaPesquisaAvancadaController', function ($scope, $rootScope, $uibModalInstance, ExpurgoDemandaService, ExpurgoDetalheService) {

    // FUNCOES AUX
    $scope.ListaExpurgoStatus = function () {
        $scope.ListaStatusExpurgo = {};
        try {
            ExpurgoDetalheService.ListaExpurgoStatus().then(function successCallback(response) {
                $scope.ListaStatusExpurgo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Expurgo!");
        }
    }
    $scope.ListaExpurgoDetalhe = function () {
        try {
            ExpurgoDetalheService.ListaExpurgoDetalhe().then(function successCallback(response) {
                $scope.ExpurgoDetalhes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ExpurgoDetalhe!");
        }
    }

    //Init Dominio
    $scope.ListaExpurgoStatus();
    $scope.ListaExpurgoDetalhe();

    //PESQUISAR
    $scope.btnPesquisar = function () {

        //Retorna Parametros da Pesquisa Avançada
        $uibModalInstance.close($scope.oFiltroPesquisaAvancada);
    };

    //FECHAR
    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };
});