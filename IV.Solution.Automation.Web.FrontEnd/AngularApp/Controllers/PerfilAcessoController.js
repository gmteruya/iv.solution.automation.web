﻿angular.module('automationApp').controller('PerfilAcessoController', function ($scope, $rootScope, PerfilAcessoService, ClienteService, MenuService, ScriptService, GrupoService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaPerfilAcesso = function (nIdPerfilAcessoPai) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                PerfilAcessoService.ListaPerfilAcessoPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, nIdPerfilAcessoPai).then(function successCallback(response) {
                    $scope.PerfisAcesso = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                PerfilAcessoService.ListaPerfilAcesso(nIdPerfilAcessoPai).then(function successCallback(response) {
                    $scope.PerfisAcesso = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de perfil de acesso!");
        }
    }
    $scope.InserePerfilAcesso = function () {
        try {
            var oPerfilAcesso = $scope.oPerfilAcesso;
            oPerfilAcesso.Ativo = true;
            oPerfilAcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oPerfilAcesso.DataCriacao = new Date();
            PerfilAcessoService.InserePerfilAcesso(oPerfilAcesso).then(function successCallback(response) {
                $scope.PerfisAcesso.unshift(response.data.Retorno);
                delete $scope.oPerfilAcesso;
                $scope.$emit('atualizaTituloPagina', 'Perfis de Acesso');
                $scope.ViewMain = 'Views/Sistema/PerfilAcesso/ListaPerfilAcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo PerfilAcesso!");
        }
    }
    $scope.AtualizaPerfilAcesso = function () {
        try {
            var IdPerfilAcesso = $scope.oPerfilAcesso.Id;
            var oPerfilAcesso = $scope.oPerfilAcesso;
            oPerfilAcesso.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oPerfilAcesso.DataAlteracao = new Date();
            PerfilAcessoService.AtualizaPerfilAcesso(IdPerfilAcesso, oPerfilAcesso).then(function successCallback(response) {
                $scope.PerfisAcesso = response.data.Retorno;
                $scope.oPerfilAcesso = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar PerfilAcesso!");
        }
    }

    // FUNCOES AUX    
    $scope.ListaMenu = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                MenuService.ListaMenuPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Menus = {};
                    $scope.Menus = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                MenuService.ListaMenu().then(function successCallback(response) {
                    $scope.Menus = {};
                    $scope.Menus = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de menus!");
        }
    }
    $scope.ListaScriptStatus = function () {
        try {
            ScriptService.ListaScriptStatus().then(function successCallback(response) {
                $scope.ListaStatusScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de script!");
        }
    }
    $scope.ListaScriptPrioridade = function () {
        try {
            ScriptService.ListaScriptPrioridade().then(function successCallback(response) {
                $scope.ListaPrioridadeScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de prioridades de script!");
        }
    }
    $scope.ListaScript = function (nIdScriptStatus, nIdScriptPrioridade) {
        try {
            ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                $scope.Scripts = [];
                $scope.Scripts = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }
    $scope.ListaGrupo = function (nIdCliente) {
        try {
            if (nIdCliente > 0) {
                GrupoService.ListaGrupoPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.Grupos = {};
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                $scope.Grupos = null;
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $rootScope.loading = [true, 'Carregando'];
        $scope.oPerfilAcesso = {};
        $scope.$emit('atualizaTituloPagina', 'Inserindo um novo perfil de acesso.');
        $scope.ViewMain = 'Views/Sistema/PerfilAcesso/InserePerfilAcesso.html';
        $rootScope.loading = [false];
    }
    $scope.btnEditar = function () {
        var nIdPerfilAcesso = this.oPerfilAcesso.Id;
        try {
            PerfilAcessoService.RetornaPerfilAcesso(nIdPerfilAcesso, true, true, true, true, true).then(function successCallback(response) {
                $scope.oPerfilAcesso = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/PerfilAcesso/DetalhePerfilAcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do PerfilAcesso!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oPerfilAcesso;
        $scope.$emit('atualizaTituloPagina', 'Perfis de Acesso');
        $scope.ViewMain = 'Views/Sistema/PerfilAcesso/ListaPerfilAcesso.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Perfis de Acesso');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/PerfilAcesso/ListaPerfilAcesso.html';
    $scope.ListaPerfilAcesso(0);

});

