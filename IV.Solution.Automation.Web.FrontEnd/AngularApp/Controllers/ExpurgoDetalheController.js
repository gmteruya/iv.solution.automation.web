﻿angular.module('automationApp').controller('ExpurgoDetalheController', function ($scope, $rootScope, ExpurgoDetalheService) {

    //FUNCOES
    $scope.ListaExpurgoDetalhe = function () {
        try {
            ExpurgoDetalheService.ListaExpurgoDetalhe().then(function successCallback(response) {
                $scope.ExpurgoDetalhes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ExpurgoDetalhe!");
        }
    }
    $scope.InsereExpurgoDetalhe = function () {
        try {
            var oExpurgoDetalhe = $scope.oExpurgoDetalhe;
            oExpurgoDetalhe.Ativo = true;
            oExpurgoDetalhe.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oExpurgoDetalhe.DataCriacao = new Date();
            ExpurgoDetalheService.InsereExpurgoDetalhe(oExpurgoDetalhe).then(function successCallback(response) {
                $scope.ExpurgoDetalhes.unshift(response.data.Retorno);
                delete $scope.oExpurgoDetalhe;
                $scope.$emit('atualizaTituloPagina', 'Robôs');
                $scope.ViewMain = 'Views/Sistema/ExpurgoDetalhe/ListaExpurgoDetalhe.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ExpurgoDetalhe!");
        }
    }
    $scope.AtualizaExpurgoDetalhe = function () {
        try {
            var IdExpurgoDetalhe = $scope.oExpurgoDetalhe.Id;
            var oExpurgoDetalhe = $scope.oExpurgoDetalhe;
            oExpurgoDetalhe.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oExpurgoDetalhe.DataAlteracao = new Date();
            ExpurgoDetalheService.AtualizaExpurgoDetalhe(IdExpurgoDetalhe, oExpurgoDetalhe).then(function successCallback(response) {
                $scope.ExpurgoDetalhes = response.data.Retorno;
                $scope.oExpurgoDetalhe = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ExpurgoDetalhe!");
        }
    }
    
    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oExpurgoDetalhe = {};
        $scope.ViewMain = 'Views/Sistema/ExpurgoDetalhe/InsereExpurgoDetalhe.html';
    }
    $scope.btnEditar = function () {
        var nIdExpurgoDetalhe = this.oExpurgoDetalhe.Id;
        try {
            ExpurgoDetalheService.RetornaExpurgoDetalhe(nIdExpurgoDetalhe, true, true, true, true).then(function successCallback(response) {
                $scope.oExpurgoDetalhe = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ExpurgoDetalhe/DetalheExpurgoDetalhe.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ExpurgoDetalhe!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oExpurgoDetalhe;
        $scope.$emit('atualizaTituloPagina', 'Schedule de Expurgo de Dados');
        $scope.ViewMain = 'Views/Sistema/ExpurgoDetalhe/ListaExpurgoDetalhe.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Schedule de Expurgo de Dados');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ExpurgoDetalhe/ListaExpurgoDetalhe.html';
    $scope.ListaExpurgoDetalhe();

});

