﻿angular.module('automationApp').controller('EstruturaController', function ($scope, $rootScope, $uibModal, EstruturaService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaEstrutura = function () {
        try {
            EstruturaService.ListaEstrutura().then(function successCallback(response) {
                $scope.ListaEstrutura = response.data.Retorno;

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de Estrutura!");
        }
    }
    $scope.RetornaEstrutura = function (nIdEstruturaMarco) {
        try {
            EstruturaService.RetornaEstrutura(nIdEstruturaMarco).then(function successCallback(response) {
                $scope.Estrutura = response.data.Retorno;

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a Estrutura!");
        }
    }
    $scope.RetornaDados = function (nIdEstruturaMarco) {
        try {
            EstruturaService.RetornaDados(nIdEstruturaMarco).then(function successCallback(response) {
                $scope.Estrutura = response.data.Retorno;

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados da Estrutura!");
        }
    }

    //EVENTOS
    $scope.btnEditar = function (nIdEstruturaMarco) {
        try {
            EstruturaService.RetornaDados(nIdEstruturaMarco).then(function successCallback(response) {
                $scope.Estrutura = response.data.Retorno;
                $scope.Dados = angular.fromJson($scope.Estrutura.Dados);
                $scope.Estrutura = $scope.Estrutura.EstruturaMacro;

                $scope.$emit('atualizaTituloPagina', 'Estrutura - ' + $scope.Estrutura.Name);

                $scope.ViewMain = 'Views/Sistema/Estrutura/DetalheEstruturaDados.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados da Estrutura!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oRobo;
        $scope.$emit('atualizaTituloPagina', 'Robôs');
        $scope.ViewMain = 'Views/Sistema/Estrutura/ListaEstrutura.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Estrutura');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Estrutura/ListaEstrutura.html';
    $scope.ListaEstrutura();

});