﻿angular.module('automationApp').controller('ScriptSistemaController', function ($scope, $rootScope, ScriptSistemaService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaScriptSistema = function () {
        try {
            ScriptSistemaService.ListaScriptSistema().then(function successCallback(response) {
                $scope.SistemasScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ScriptSistema!");
        }
    }
    $scope.InsereScriptSistema = function () {
        try {
            var oScriptSistema = $scope.oScriptSistema;
            oScriptSistema.Ativo = true;
            oScriptSistema.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptSistema.DataCriacao = new Date();
            ScriptSistemaService.InsereScriptSistema(oScriptSistema).then(function successCallback(response) {
                $scope.SistemasScript.unshift(response.data.Retorno);
                delete $scope.oScriptSistema;
                $scope.$emit('atualizaTituloPagina', 'Sistemas Automatizados');
                $scope.ViewMain = 'Views/Sistema/ScriptSistema/ListaScriptSistema.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ScriptSistema!");
        }
    }
    $scope.AtualizaScriptSistema = function () {
        try {
            var IdScriptSistema = $scope.oScriptSistema.Id;
            var oScriptSistema = $scope.oScriptSistema;
            oScriptSistema.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptSistema.DataAlteracao = new Date();
            ScriptSistemaService.AtualizaScriptSistema(IdScriptSistema, oScriptSistema).then(function successCallback(response) {
                $scope.SistemasScript = response.data.Retorno;
                $scope.oScriptSistema = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ScriptSistema!");
        }
    }

    // FUNCOES AUX    
    $scope.ListaScriptSistemaTipo = function () {
        try {
            ScriptSistemaService.ListaScriptSistemaTipo().then(function successCallback(response) {
                $scope.TiposSistemaScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de tipos de sistemas automatizados!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {

        //Nova Intancia
        $scope.oScriptSistema = {};
        $scope.ViewMain = 'Views/Sistema/ScriptSistema/InsereScriptSistema.html';
    }
    $scope.btnEditar = function () {
        var nIdScriptSistema = this.oScriptSistema.Id;
        try {
            ScriptSistemaService.RetornaScriptSistema(nIdScriptSistema).then(function successCallback(response) {
                $scope.oScriptSistema = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ScriptSistema/DetalheScriptSistema.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ScriptSistema!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oScriptSistema;
        $scope.$emit('atualizaTituloPagina', 'Sistemas Automatizados');
        $scope.ViewMain = 'Views/Sistema/ScriptSistema/ListaScriptSistema.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Sistemas Automatizados');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ScriptSistema/ListaScriptSistema.html';
    $scope.ListaScriptSistemaTipo();
    $scope.ListaScriptSistema();

});

