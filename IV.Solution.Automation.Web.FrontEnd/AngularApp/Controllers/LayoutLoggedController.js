﻿angular.module('automationApp').controller('LayoutLoggedController', function ($scope, $rootScope, $uibModal, AuthenticationFactory, MenuService, UtilService, ClienteService, ScriptService, ProcessoService, PerfilAcessoService) {
    //VARIAVEIS DE SCOPE
    $scope.ServiceAtivo = true;
    $scope.DataAtual = new Date();

    //TITULO DA PAGINA
    $scope.$on('atualizaTituloPagina', function (event, args) {

        //Titulo da Pagina
        $scope.TituloPagina = args;

        //ATUALIZA PROCESSOS
        //$scope.ListaScriptPermissaoInclusao(0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
    });

    //CREDENCIAIS DE ACESSO
    $rootScope.globals = AuthenticationFactory.GetCredentials();
    var loggedIn = false;
    if ($rootScope.globals != undefined && $rootScope.globals.Authentication != undefined) {
        loggedIn = true;
    }
    if (loggedIn) {
        $scope.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
        $scope.oPerfilAcesso = $rootScope.globals.Authentication.UsuarioSessao.Usuario.PerfilAcesso;   
    }
      
    //FUNCOES GENERICAS DO SISTEMA
    $scope.ListaMenu = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                MenuService.ListaMenuPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.ItensMenu = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            } else {
                MenuService.ListaMenu().then(function successCallback(response) {
                    $scope.ItensMenu = response.data.Retorno;
                }, function errorCallback(response) {
                   $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            }
        } catch (err) {
            $scope.ServiceAtivo = false;
        }
    }
    $scope.ControlaExibicaoMenuPermissao = function (nIdMenu, nIdMenuPai) {
        var bExibeEntidade = false;
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente === 0) {
            bExibeEntidade = true;
        } else {
            var arPerfilAcessoMenu;
            arPerfilAcessoMenu = $rootScope.globals.Authentication.UsuarioSessao.Usuario.PerfilAcesso.PerfilAcessoMenu;
            for (var i = 0; i < arPerfilAcessoMenu.length; i++) {
                var oPerfilAcessoMenu = arPerfilAcessoMenu[i];
                if (oPerfilAcessoMenu.Menu.Id === nIdMenu && oPerfilAcessoMenu.Menu.IdMenuPai === nIdMenuPai) {
                    bExibeEntidade = true;                    
                }
            }  
        };
        return bExibeEntidade;
    }
    $scope.RecuperaListaSimNao = function () {
        $scope.ListaSimNao = UtilService.GetListaSimNao();
    }    
    $scope.ListaScriptPermissaoInclusao = function (nIdGrupo, nIdCliente) {
        try {
            delete $scope.ScriptsInclusaoSituacao;
            if (nIdGrupo > 0) {
                $scope.IdGrupo = nIdGrupo;
                ScriptService.ListaScriptPorGrupoRelacionado(nIdGrupo).then(function successCallback(response) {
                    $scope.ScriptsInclusaoSituacao = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                }).catch(function (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                });
            } else {
                if (nIdCliente > 0) {
                    $scope.IdCliente = nIdCliente;
                    ScriptService.ListaScriptPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                        $scope.ScriptsInclusaoSituacao = response.data.Retorno;
                    }, function errorCallback(response) {
                        $rootScope.TrataMensagemResponseRPA(response, false);
                    }).catch(function (err) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                    });
                } else {
                    ScriptService.ListaScript().then(function successCallback(response) {
                        $scope.ScriptsInclusaoSituacao = response.data.Retorno;
                    }, function errorCallback(response) {
                        $rootScope.TrataMensagemResponseRPA(response, false);
                    }).catch(function (err) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possível completar a operação!");
                    });
                }
            }
        } catch (err) {
            $scope.ServiceAtivo = false;
        }
    }
    $scope.ControlaExibicaoPermissao = function (nIdGrupo, nIdScript, sEntidadeControlada, sPermissao) {
        var bExibeEntidade = false;
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente === 0) {
            bExibeEntidade = true;
        } else {
            var arPerfilAcessoScriptGrupo;
            arPerfilAcessoScriptGrupo = $rootScope.globals.Authentication.UsuarioSessao.Usuario.PerfilAcesso.PerfilAcessoScriptGrupo;
            if (sEntidadeControlada === 'Grupo') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
            if (sEntidadeControlada === 'Script') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdScript === nIdScript && (nIdGrupo === 0 || oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo)) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
        };
        return bExibeEntidade;
    }
    $scope.findArrayIndex = function (array, predicateFunction) {
        var length = array == null ? 0 : array.length;
        if (!length) {
            return -1;
        }
        var index = -1;
        for (var i = 0; i < array.length; ++i) {
            if (predicateFunction(array[i])) {
                index = i;
                break;
            }
        }
        return index;
    };

    // INICIALIZACAO
    $scope.ListaMenu();
    $scope.RecuperaListaSimNao();

    //SE VISUALIZA STATUS DOS SCRIPTS, CARREGA AS LISTAS
    $scope.ListaScriptPermissaoInclusao(0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);

    //COMPONENTE DE MENSAGEM
    $rootScope.arMensagens = [];
    $rootScope.TrataMensagemResponseRPA = function (oResponseRPA, bShowMensagens, bUseLogicalMultiMensagemRetorno, bErroEspecifico, sMensagemEspecifica) {
        var bDeuErro = true;
        var sTipo = 'text-error';
        var sMensagem = 'Falha ao salvar dados.';
        var oMensagem = { };
        var arMensagem =[];
        if (oResponseRPA !== undefined && oResponseRPA !== null && oResponseRPA.data !== undefined && oResponseRPA.data !== null) {
            if (oResponseRPA.data.Sucesso === true) {
                sTipo = 'text-sucess';
                sMensagem = 'Operação realizada com sucesso.'
                bDeuErro = false;
            };
            if (oResponseRPA.data.MsgRetorno !== undefined && oResponseRPA.data.MsgRetorno !== null && oResponseRPA.data.MsgRetorno !== '') {
                if (bUseLogicalMultiMensagemRetorno !== undefined && bUseLogicalMultiMensagemRetorno !== null && bUseLogicalMultiMensagemRetorno === true) {
                    var arMensagenRetorno = oResponseRPA.data.MsgRetorno.split('§');
                    if (arMensagenRetorno.length > 0) {
                        var oMensagemRetorno;
                        for (var i = 0; i < arMensagenRetorno.length; i++) {
                            oMensagemRetorno = arMensagenRetorno[i];
                            if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                arMensagemRetorno = oMensagemRetorno.split('&');
                                if (arMensagemRetorno.length = 2) {
                                    oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                    arMensagem.push(oMensagem);
                                };
                            };
                        };
                    };
                } else {
                    oMensagem = { Tipo: sTipo, Mensagem: oResponseRPA.data.MsgRetorno };
                    arMensagem.push(oMensagem);
                };
            } else {
                oMensagem = { Tipo: sTipo, Mensagem: sMensagem };
                arMensagem.push(oMensagem);
            };
        } else if (bErroEspecifico !== undefined && sMensagemEspecifica !== null && sMensagemEspecifica !== '') {
            sMensagem = sMensagemEspecifica;
            if (bErroEspecifico === false) {
                sTipo = 'text-sucess';
                bDeuErro = false;
            };
            oMensagem = { Tipo: sTipo, Mensagem: sMensagem };
            arMensagem.push(oMensagem);
        };
        if (arMensagem.length > 0) {
            for (var i = 0; i < arMensagem.length; i++) {
                //'p-3 mb-2 bg-success text-white'
                //'p-3 mb-2 bg-danger text-white'
                //'p-3 mb-2 bg-warning text-dark'
                //'p-3 mb-2 bg-info text-white'
                $rootScope.arMensagens.push(arMensagem[i]);
            };
            if (bShowMensagens === true) {
                $rootScope.ShowModalMensagemRPA('lg');
            }            
        };
        return bDeuErro;
    };
    $rootScope.ShowModalMensagemRPA = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/MensagemRPA.html',
            controller: 'MensagemRPAController',
            size: sSize,
            backdrop: false,
            keyboard: false
        });       
    };
        
});