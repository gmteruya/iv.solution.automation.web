﻿angular.module('automationApp').controller('ScriptStepController', function ($scope, $rootScope, ScriptStepService, ClienteService, ScriptService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaScriptStep = function (nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ScriptStepService.ListaScriptStepPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema).then(function successCallback(response) {
                    $scope.Steps = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptStepService.ListaScriptStep(nIdScriptStepTipo, nIdScriptStepPai, nIdScriptStepReinicioErro, nIdScript, nIdScriptSistema).then(function successCallback(response) {
                    $scope.Steps = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de steps!");
        }
    }
    $scope.InsereScriptStep = function () {
        try {
            var oScriptStep = $scope.oScriptStep;
            oScriptStep.Ativo = true;
            oScriptStep.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptStep.DataCriacao = new Date();
            ScriptStepService.InsereScriptStep(oScriptStep).then(function successCallback(response) {
                $scope.Steps.unshift(response.data.Retorno);
                delete $scope.oScriptStep;
                $scope.$emit('atualizaTituloPagina', 'Steps de Processamento');
                $scope.ViewMain = 'Views/Sistema/ScriptStep/ListaScriptStep.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo step!");
        }
    }
    $scope.AtualizaScriptStep = function () {
        try {
            var IdScriptStep = $scope.oScriptStep.Id;
            var oScriptStep = $scope.oScriptStep;
            oScriptStep.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptStep.DataAlteracao = new Date();
            ScriptStepService.AtualizaScriptStep(IdScriptStep, oScriptStep).then(function successCallback(response) {
                $scope.Steps = response.data.Retorno;
                $scope.oScriptStep = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ScriptStep!");
        }
    }

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = {};
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaScriptStepTipo = function () {
        try {
            ScriptStepService.ListaScriptStepTipo().then(function successCallback(response) {
                $scope.TipoStepScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de tipos de step!");
        }
    }    
    $scope.ListaScript = function (nIdCliente, nIdScriptStatus, nIdScriptPrioridade) {
        try {
            if (nIdCliente > 0) {
                ScriptService.ListaScriptPorClienteRelacionado(nIdCliente, nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });

            } else {
                ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $rootScope.loading = [true, 'Carregando'];
        $scope.oScriptStep = {};
        $scope.$emit('atualizaTituloPagina', 'Inserindo um novo step de processamento.');
        $scope.ViewMain = 'Views/Sistema/ScriptStep/InsereScriptStep.html';
        $rootScope.loading = [false];

    }
    $scope.btnEditar = function () {
        var nIdScriptStep = this.oScriptStep.Id;
        try {
            ScriptStepService.RetornaScriptStep(nIdScriptStep, true, true).then(function successCallback(response) {
                $scope.oScriptStep = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ScriptStep/DetalheScriptStep.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ScriptStep!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oScriptStep;
        $scope.$emit('atualizaTituloPagina', 'Steps de Processamento');
        $scope.ViewMain = 'Views/Sistema/ScriptStep/ListaScriptStep.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Steps de Processamento');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ScriptStep/ListaScriptStep.html';
    $scope.ListaCliente();
    $scope.ListaScriptStepTipo(0);
    $scope.ListaScript($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
    $scope.ListaScriptStep(0, 0, 0, 0, 0)

});

