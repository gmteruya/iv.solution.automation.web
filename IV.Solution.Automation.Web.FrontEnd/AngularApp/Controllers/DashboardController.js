﻿angular.module('automationApp').controller('DashboardController', function ($scope, $rootScope, $sce, TokenAcessoExternoService, UtilService) {

    //Token Dash
    $scope.GeraTokenAcessoExterno = function () {
        try {
            //Gera Token Acesso Externo
            $scope.oTokenAcessoExterno = {};
            $scope.oTokenAcessoExterno.IdUsuario = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Id;
            $scope.oTokenAcessoExterno.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
            $scope.oTokenAcessoExterno.IdPerfilAcesso = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdPerfilAcesso;
            $scope.oTokenAcessoExterno.Ativo = true;
            $scope.oTokenAcessoExterno.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            $scope.oTokenAcessoExterno.DataCriacao = new Date();
            TokenAcessoExternoService.GeraTokenAcessoExterno($scope.oTokenAcessoExterno).then(function successCallback(response) {
                //Pega o Retorno
                $scope.oTokenAcessoExterno = response.data.Retorno;
                
                //Url do dash
                $scope.link_dashboard = $sce.trustAsResourceUrl(UtilService.DashboardURLBase() + 'DashboardMenu.aspx?Token=' + $scope.oTokenAcessoExterno.Token);

                // INICIALIZACAO
                $scope.$emit('atualizaTituloPagina', 'Dashboard');
                $scope.ViewMain = 'Views/Sistema/Dashboard.html';

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel gerar um token de acesso externo!");
        }
    }

    //Gera Token de Acesso
    $scope.GeraTokenAcessoExterno();
    
});