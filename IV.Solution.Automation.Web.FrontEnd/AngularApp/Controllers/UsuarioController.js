﻿angular.module('automationApp').controller('UsuarioController', function ($scope,
                                                                          $rootScope,
                                                                          $timeout,
                                                                          $uibModal,
                                                                          UsuarioService,
                                                                          ClienteService, 
                                                                          PerfilAcessoService) {
    
    // FUNCOES CRUD
    $scope.ListaUsuario = function (nIdCliente, nIdPerfilAcesso) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                UsuarioService.ListaUsuarioPorPerfilAcesso(nIdCliente, nIdPerfilAcesso).then(function successCallback(response) {
                    $scope.Usuarios = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                UsuarioService.ListaUsuario(nIdCliente, nIdPerfilAcesso).then(function successCallback(response) {
                    $scope.Usuarios = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de usuário!");
        }
    }
    $scope.InsereUsuario = function () {
        try {
            var oUsuario = $scope.oUsuario;
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                oUsuario.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
            }
            oUsuario.Senha = '123ABC';
            oUsuario.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oUsuario.DataCriacao = new Date();
            UsuarioService.InsereUsuario(oUsuario).then(function successCallback(response) {
                var bDeuErro = $rootScope.TrataMensagemResponseRPA(response, true);
                if (bDeuErro === false) {
                    $scope.Usuarios.unshift(response.data.Retorno);
                }              
                delete $scope.oUsuario;
                $scope.$emit('atualizaTituloPagina', 'Usuários do Assistente Virtual');
                $scope.ViewMain = 'Views/Sistema/Usuario/ListaUsuario.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo usuário!");
        }
    }
    $scope.AtualizaUsuario = function () {
        try {
            var nIdUsuario = $scope.oUsuario.Id;
            var oUsuario = $scope.oUsuario;
            if (oUsuario.ReiniciaSenhaInicial === true) {
                oUsuario.Senha = '123ABC';
            }
            oUsuario.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oUsuario.DataAlteracao = new Date();
            UsuarioService.AtualizaUsuario(nIdUsuario, oUsuario).then(function successCallback(response) {
                var bDeuErro = $rootScope.TrataMensagemResponseRPA(response, true);
                if (bDeuErro === false) {
                    var nIndexArrayElement = $scope.findArrayIndex($scope.Usuarios, function (oElement) { return oElement.Id == nIdUsuario; });
                    $scope.Usuarios.splice(nIndexArrayElement, 1);
                    $scope.Usuarios.unshift(response.data.Retorno);
                }
                delete $scope.oUsuario;
                $scope.$emit('atualizaTituloPagina', 'Usuários do Assistente Virtual');
                $scope.ViewMain = 'Views/Sistema/Usuario/ListaUsuario.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar usuário!");
        }
    }

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        $scope.Clientes = {};
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de clientes!");
        }
    }
    $scope.ListaPerfilAcesso = function (nIdCliente, nIdPerfilAcessoPai) {
        try {
            if (nIdCliente > 0) {
                if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                    PerfilAcessoService.ListaPerfilAcessoPorPerfilAcesso($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdPerfilAcesso, nIdPerfilAcessoPai).then(function successCallback(response) {
                        $scope.Perfis = {};
                        $scope.Perfis = response.data.Retorno;
                    }, function errorCallback(response) {
                        $rootScope.TrataMensagemResponseRPA(response, false);
                    });
                } else {
                    PerfilAcessoService.ListaPerfilAcessoPorClienteRelacionado(nIdCliente, nIdPerfilAcessoPai).then(function successCallback(response) {
                        $scope.Perfis = {};
                        $scope.Perfis = response.data.Retorno;
                    }, function errorCallback(response) {
                        $rootScope.TrataMensagemResponseRPA(response, false);
                    });
                }
            } else {
                PerfilAcessoService.ListaPerfilAcesso(nIdPerfilAcessoPai).then(function successCallback(response) {
                    $scope.Perfis = {};
                    $scope.Perfis = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de perfis de acesso!");
        }
    }
    $scope.VerificaUsuarioExistente = function (sLogin) {
        try {
            UsuarioService.RetornaUsuarioPorLogin(sLogin, true, true, true).then(function successCallback(response) {
                $scope.oUsuarioExistente = response.data.Retorno;
                if ($scope.oUsuarioExistente !== undefined && $scope.oUsuarioExistente !== null) {
                    $scope.oUsuario.Login = '';
                    alert('Usuário de Login [' + sLogin + '] já existe na base de dados');
                };
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi verificar se usuário existe na base de dados!");
        }
    }
    $scope.ControlaVisibilidadeOrigem = function (sOrigen) {
        $scope.oUsuario.Dominio = undefined;
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $rootScope.loading = [true, 'Carregando'];
        $scope.oUsuario = {};
        $scope.$emit('atualizaTituloPagina', 'Inserindo um novo usuário.');
        $scope.ViewMain = 'Views/Sistema/Usuario/InsereUsuario.html';
        $rootScope.loading = [false];
    }
    $scope.btnEditar = function () {
        var nIdUsuario = this.oUsuario.Id;
        try {
            UsuarioService.RetornaUsuario(nIdUsuario, true, true, true).then(function successCallback(response) {
                $scope.oUsuario = response.data.Retorno;
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Usuário');
                $scope.ViewMain = 'Views/Sistema/Usuario/DetalheUsuario.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do usuário!");
        }        
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oUsuario;
        $scope.$emit('atualizaTituloPagina', 'Usuários do Assistente Virtual');
        $scope.ViewMain = 'Views/Sistema/Usuario/ListaUsuario.html';
    }
    $scope.btnFiltroPesquisaAvancada = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Usuario/PesquisaAvancadaUsuario.html',
            controller: 'UsuarioPesquisaAvancadaController',
            size: sSize,
            backdrop: true,
            keyboard: true
        });
        modalInstance.result.then(function (oFiltroPesquisaAvancada) {
            $rootScope.loading = [true, 'Carregando'];
            var nIdCliente = 0;
            var nIdPerfilAcesso = 0;
            if (oFiltroPesquisaAvancada !== undefined && oFiltroPesquisaAvancada !== null) {
                if (oFiltroPesquisaAvancada.IdCliente !== undefined && oFiltroPesquisaAvancada.IdCliente !== null) {
                    nIdCliente = oFiltroPesquisaAvancada.IdCliente;
                }
                if (oFiltroPesquisaAvancada.IdPerfilAcesso !== undefined && oFiltroPesquisaAvancada.IdPerfilAcesso !== null) {
                    nIdPerfilAcesso = oFiltroPesquisaAvancada.IdPerfilAcesso;
                }             
            }
            if (nIdCliente === 0 && $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                nIdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0
            }
            $scope.ListaUsuario(nIdCliente, nIdPerfilAcesso);
            $rootScope.loading = [false];
        }, function () {

        });
    };

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Usuários do Assistente Virtual');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Usuario/ListaUsuario.html';
    $scope.ListaCliente();
    $scope.ListaPerfilAcesso($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0);
    var nIdPerfilAcesso = 0;
    if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
        nIdPerfilAcesso = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdPerfilAcesso;
    }
    $scope.ListaUsuario($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, nIdPerfilAcesso);
        
});

angular.module('automationApp').controller('UsuarioPesquisaAvancadaController', function ($scope,
                                                                                          $rootScope,
                                                                                          $uibModalInstance,
                                                                                          ClienteService,
                                                                                          PerfilAcessoService) {

    // VARIAVEIS
    $scope.oFiltroPesquisaAvancada = {};
    if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
        $scope.oFiltroPesquisaAvancada.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
    }

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        $scope.Clientes = {};
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de clientes!");
        }
    }
    $scope.ListaPerfilAcesso = function (nIdCliente, nIdPerfilAcessoPai) {
        $scope.Perfis = { };
        try {
            if (nIdCliente > 0) {
                if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                    PerfilAcessoService.ListaPerfilAcessoPorPerfilAcesso($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdPerfilAcesso, nIdPerfilAcessoPai).then(function successCallback(response) {
                        $scope.Perfis = response.data.Retorno;
                    }, function errorCallback(response) {
                        $rootScope.TrataMensagemResponseRPA(response, false);
                    });
                } else {
                    PerfilAcessoService.ListaPerfilAcessoPorClienteRelacionado(nIdCliente, nIdPerfilAcessoPai).then(function successCallback(response) {
                        $scope.Perfis = response.data.Retorno;
                    }, function errorCallback(response) {
                        $rootScope.TrataMensagemResponseRPA(response, false);
                    });
                }
            } else {
                PerfilAcessoService.ListaPerfilAcesso(nIdPerfilAcessoPai).then(function successCallback(response) {
                    $scope.Perfis = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de perfis de acesso!");
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de perfis de acesso!");
        }
    }
    
    //INIT DOMINIOS
    $scope.ListaCliente();
    $scope.ListaPerfilAcesso($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0);
        
    //PESQUISAR
    $scope.btnPesquisar = function () {

        //Retorna Parametros da Pesquisa Avançada
        $uibModalInstance.close($scope.oFiltroPesquisaAvancada);
    };

    //FECHAR
    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };
});


