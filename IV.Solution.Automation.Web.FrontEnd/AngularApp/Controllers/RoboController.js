﻿angular.module('automationApp').controller('RoboController', function ($scope, $rootScope, $uibModal, RoboService, RoboVersaoService, ServicoRoboService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaRobo = function (IdRoboEstado, IdRoboStatus, IdRoboVersao, DominioRede) {
        try {            
            RoboService.ListaRobo(IdRoboEstado, IdRoboStatus, IdRoboVersao, DominioRede).then(function successCallback(response) {
                $scope.Robos = response.data.Retorno;

                angular.forEach($scope.Robos, function (oRobo) {

                    angular.forEach($scope.ListaEstadoRobo, function (oRoboEstado) {
                        
                        if (oRoboEstado.Id === oRobo.IdRoboEstado) {
                            oRobo.RoboEstado = oRoboEstado;
                        }
                    });

                    angular.forEach($scope.ListaStatusRobo, function (oRoboStatus) {
                        if (oRoboStatus.Id === oRobo.IdRoboStatus) {
                            oRobo.RoboStatus = oRoboStatus;
                        }
                    });

                });

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de Robo!");
        }
    }
    $scope.InsereRobo = function () {
        try {
            var oRobo = $scope.oRobo;
            //oRobo.IdRoboEstado = 0;
            //oRobo.IdRoboStatus = 0;    
            oRobo.Ativo = true;
            oRobo.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oRobo.DataCriacao = new Date();
            RoboService.InsereRobo(oRobo).then(function successCallback(response) {
                $scope.Robos.unshift(response.data.Retorno);
                delete $scope.oRobo;
                $scope.$emit('atualizaTituloPagina', 'Robôs');
                $scope.ViewMain = 'Views/Sistema/Robo/ListaRobo.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo Robo!");
        }
    }
    $scope.AtualizaRobo = function () {
        try {
            var IdRobo = $scope.oRobo.Id;
            var oRobo = $scope.oRobo;
            oRobo.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oRobo.DataAlteracao = new Date();
            RoboService.AtualizaRobo(IdRobo, oRobo).then(function successCallback(response) {
                $scope.Robos = response.data.Retorno;
                $scope.oRobo = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar Robo!");
        }
    }

    // FUNCOES AUX
    $scope.ListaRoboEstado = function () {
        try {
            RoboService.ListaRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {            
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de robo!");
        }
    }
    $scope.ListaRoboStatus = function () {
        try {
            RoboService.ListaRoboStatus().then(function successCallback(response) {
                $scope.ListaStatusRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Robo!");
        }
    }
    $scope.ListaRoboVersao = function (nIdRoboVersaoStatus) {
        try {
            RoboVersaoService.ListaRoboVersao(nIdRoboVersaoStatus).then(function successCallback(response) {
                $scope.RoboVersoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de versão do robo!");
        }
    }
    $scope.ListaServicoRoboEstado = function () {
        try {
            ServicoRoboService.ListaServicoRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoServicoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de estado do serviço de robô!");
        }
    }
    $scope.ListaServicoRobo = function (nIdServicoRoboEstado, nIdServicoRoboVersao) {
        try {
            ServicoRoboService.ListaServicoRobo(nIdServicoRoboEstado, nIdServicoRoboVersao).then(function successCallback(response) {
                $scope.ServicosRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de serviços de robõ!");
        }
    }
    
    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oRobo = {};
        $scope.ViewMain = 'Views/Sistema/Robo/InsereRobo.html';
    }
    $scope.btnEditar = function () {
        var nIdRobo = this.oRobo.Id;
        try {
            RoboService.RetornaRobo(nIdRobo, true, true, true, true).then(function successCallback(response) {
                $scope.oRobo = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/Robo/DetalheRobo.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do Robo!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oRobo;
        $scope.$emit('atualizaTituloPagina', 'Robôs');
        $scope.ViewMain = 'Views/Sistema/Robo/ListaRobo.html';
    }
    $scope.btnReiniciaRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.ReiniciaRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar os robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar os robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnLigaRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.LigaRobo(lstRobosSelecionados).then(function (response) {
                        setTimeout(function(){ //Define um timeout para que o banco tenha tempo de atualizar o status do robô
                            var oMensagem = {};
                            var arMensagem = [];
                            if (response.data.Sucesso === false) {
                                $scope.oRoboVersao = $scope.oRoboVersao;
                                oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                                arMensagem.push(oMensagem);
                            } else {
                                if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                    var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                    if (arMensagenRetorno.length > 0) {
                                        var oMensagemRetorno;
                                        for (var i = 0; i < arMensagenRetorno.length; i++) {
                                            oMensagemRetorno = arMensagenRetorno[i];
                                            if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                                arMensagemRetorno = oMensagemRetorno.split('&');
                                                if (arMensagemRetorno.length = 2) {
                                                    oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                    arMensagem.push(oMensagem);
                                                };
                                            };
                                        };
                                    };
                                };
                            };
                            if (arMensagem.length > 0) {
                                for (var i = 0; i < arMensagem.length; i++) {
                                    $rootScope.arMensagens.push(arMensagem[i]);
                                };
                                $rootScope.ShowModalMensagemRPA('lg');
                            };
                            $scope.ListaServicoRobo(0, 0);
                            $scope.ListaRobo(0, 0, 0, '');
                            $rootScope.loading = [false];
                    }, lstRobosSelecionados.length*500)
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar os robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar os robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnDesligaRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.DesligaRobo(lstRobosSelecionados).then(function (response) {
                        setTimeout(function () {
                            var oMensagem = {};
                            var arMensagem = [];
                            if (response.data.Sucesso === false) {
                                $scope.oRoboVersao = $scope.oRoboVersao;
                                oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                                arMensagem.push(oMensagem);
                            } else {
                                if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                    var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                    if (arMensagenRetorno.length > 0) {
                                        var oMensagemRetorno;
                                        for (var i = 0; i < arMensagenRetorno.length; i++) {
                                            oMensagemRetorno = arMensagenRetorno[i];
                                            if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                                arMensagemRetorno = oMensagemRetorno.split('&');
                                                if (arMensagemRetorno.length = 2) {
                                                    oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                    arMensagem.push(oMensagem);
                                                };
                                            };
                                        };
                                    };
                                };
                            };
                            if (arMensagem.length > 0) {
                                for (var i = 0; i < arMensagem.length; i++) {
                                    $rootScope.arMensagens.push(arMensagem[i]);
                                };
                                $rootScope.ShowModalMensagemRPA('lg');
                            };
                            $scope.ListaServicoRobo(0, 0);
                            $scope.ListaRobo(0, 0, 0, '');
                            $rootScope.loading = [false];
                        }, lstRobosSelecionados.length*500)
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar os robôs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar os robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnAtualizaVersaoBot = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de atualização de versão dos robôs selecionados!'];
        
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.AtualizaVersaoBot(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar os robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar os robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnIniciarMonitoramentoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de inicializar o monitoramento dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.IniciarMonitoramentoRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento dos robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento de nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento dos robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnPararMonitoramentoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de parar o monitoramento dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.PararMonitoramentoRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento dos robôs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento de nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento dos robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnFiltroPesquisaAvancada = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Robo/PesquisaAvancadaRobo.html',
            controller: 'PesquisaAvancadaRoboController',
            size: sSize,
            backdrop: true,
            keyboard: true,
            resolve: {
                oFiltroPesquisaAvancada: function () {
                    return $scope.oFiltroPesquisaAvancada;
                }
            }
        });
        modalInstance.result.then(function (oFiltroPesquisaAvancada) {
            $rootScope.loading = [true, 'Carregando'];
            var nIdRoboEstado = 0;
            var nIdRoboStatus = 0;

            if (oFiltroPesquisaAvancada !== undefined && oFiltroPesquisaAvancada !== null) {
                //Seta Pesquisa Avançada no Scope
                $scope.oFiltroPesquisaAvancada = oFiltroPesquisaAvancada;
                if (oFiltroPesquisaAvancada.IdRoboEstado !== undefined && oFiltroPesquisaAvancada.IdRoboEstado !== null) {
                    nIdRoboEstado = oFiltroPesquisaAvancada.IdRoboEstado;
                }
                if (oFiltroPesquisaAvancada.IdRoboStatus !== undefined && oFiltroPesquisaAvancada.IdRoboStatus !== null) {
                    nIdRoboStatus = oFiltroPesquisaAvancada.IdRoboStatus;
                }
            }

            //Pesquisa/Atualiza na base novamente.
            $scope.ListaRobo(nIdRoboEstado, nIdRoboStatus, 0, '');

            $rootScope.loading = [false];
        }, function () {

        });
    };

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Robôs');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Robo/ListaRobo.html';
    $scope.ListaRoboEstado();
    $scope.ListaRoboStatus();
    $scope.ListaRoboVersao(0);
    $scope.ListaServicoRoboEstado();
    $scope.ListaServicoRobo(0, 0);
    $scope.ListaRobo(0, 0, 0, '');

});

angular.module('automationApp').controller('PesquisaAvancadaRoboController', function ($scope, $rootScope, $uibModalInstance, oFiltroPesquisaAvancada, RoboService) {
    
    // VARIAVEIS
    

    // FUNCOES AUX
    $scope.ListaRoboEstado = function () {
        try {
            RoboService.ListaRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de robo!");
        }
    }
    $scope.ListaRoboStatus = function () {
        try {
            RoboService.ListaRoboStatus().then(function successCallback(response) {
                $scope.ListaStatusRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Robo!");
        }
    }

    //INIT DOMINIOS
    $scope.ListaRoboEstado();
    $scope.ListaRoboStatus();

    //PESQUISAR
    $scope.btnPesquisar = function () {

        //Retorna Parametros da Pesquisa Avançada
        $uibModalInstance.close($scope.oFiltroPesquisaAvancada);
    };

    //FECHAR
    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

