﻿angular.module('automationApp').controller('ScriptInfoAdicionalController', function ($scope, $rootScope, ScriptInfoAdicionalService, ClienteService, ScriptService, DominioService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaScriptInfoAdicional = function (nIdScript, sTabelaRelacionada) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ScriptInfoAdicionalService.ListaScriptInfoAdicionalPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, nIdScript, sTabelaRelacionada).then(function successCallback(response) {
                    $scope.InfosAdicionais = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptInfoAdicionalService.ListaScriptInfoAdicional(nIdScript, sTabelaRelacionada).then(function successCallback(response) {
                    $scope.InfosAdicionais = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }

        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de informações adicionais!");
        }
    }
    $scope.InsereScriptInfoAdicional = function () {
        try {
            var oScriptInfoAdicional = $scope.oScriptInfoAdicional;
            oScriptInfoAdicional.Ativo = true;
            oScriptInfoAdicional.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptInfoAdicional.DataCriacao = new Date();
            ScriptInfoAdicionalService.InsereScriptInfoAdicional(oScriptInfoAdicional).then(function successCallback(response) {
                $scope.InfosAdicionais.unshift(response.data.Retorno);
                delete $scope.oScriptInfoAdicional;
                $scope.$emit('atualizaTituloPagina', 'Informações Adicionais');
                $scope.ViewMain = 'Views/Sistema/ScriptInfoAdicional/ListaScriptInfoAdicional.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo step!");
        }
    }
    $scope.AtualizaScriptInfoAdicional = function () {
        try {
            var IdScriptInfoAdicional = $scope.oScriptInfoAdicional.Id;
            var oScriptInfoAdicional = $scope.oScriptInfoAdicional;
            oScriptInfoAdicional.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptInfoAdicional.DataAlteracao = new Date();
            ScriptInfoAdicionalService.AtualizaScriptInfoAdicional(IdScriptInfoAdicional, oScriptInfoAdicional).then(function successCallback(response) {
                $scope.InfosAdicionais = response.data.Retorno;
                $scope.oScriptInfoAdicional = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ScriptInfoAdicional!");
        }
    }

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = {};
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaScript = function (nIdCliente, nIdScriptStatus, nIdScriptPrioridade) {
        try {
            if (nIdCliente > 0) {
                ScriptService.ListaScriptPorClienteRelacionado(nIdCliente, nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });

            } else {
                ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }
    $scope.ListaDominio = function (nIdCliente, nIdDominioPai, nIdScript) {
        try {
            if (nIdCliente > 0) {
                DominioService.ListaDominio(nIdCliente, nIdDominioPai, nIdScript).then(function successCallback(response) {
                    $scope.Dominios = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                DominioService.ListaDominio(nIdDominioPai, nIdScript).then(function successCallback(response) {
                    $scope.Dominios = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de domínios!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {

        //Nova Intancia
        $scope.oScriptInfoAdicional = {};
        $scope.ViewMain = 'Views/Sistema/ScriptInfoAdicional/InsereScriptInfoAdicional.html';
    }
    $scope.btnEditar = function () {
        var nIdScriptInfoAdicional = this.oScriptInfoAdicional.Id;
        try {
            ScriptInfoAdicionalService.RetornaScriptInfoAdicional(nIdScriptInfoAdicional, true, true).then(function successCallback(response) {
                $scope.oScriptInfoAdicional = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ScriptInfoAdicional/DetalheScriptInfoAdicional.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ScriptInfoAdicional!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oScriptInfoAdicional;
        $scope.$emit('atualizaTituloPagina', 'Informações Adicionais');
        $scope.ViewMain = 'Views/Sistema/ScriptInfoAdicional/ListaScriptInfoAdicional.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Informações Adicionais');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ScriptInfoAdicional/ListaScriptInfoAdicional.html';
    $scope.ListaCliente();
    $scope.ListaScript($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
    $scope.ListaDominio($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
    $scope.ListaScriptInfoAdicional(0, '');

});

