﻿angular.module('automationApp').controller('GrupoController', function ($scope, $rootScope, GrupoService, ScriptService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaGrupo = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                GrupoService.ListaGrupoPorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                GrupoService.ListaGrupo().then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }

        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de grupo!");
        }
    }
    $scope.InsereGrupo = function () {
        try {
            var oGrupo = $scope.oGrupo;
            oGrupo.Ativo = true;
            oGrupo.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oGrupo.DataCriacao = new Date();
            GrupoService.InsereGrupo(oGrupo).then(function successCallback(response) {
                $scope.Grupos.unshift(response.data.Retorno);
                delete $scope.oGrupo;
                $scope.$emit('atualizaTituloPagina', 'Grupos de Script');
                $scope.ViewMain = 'Views/Sistema/Grupo/ListaGrupo.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo grupo!");
        }
    }
    $scope.AtualizaGrupo = function () {
        try {
            var IdGrupo = $scope.oGrupo.Id;
            var oGrupo = $scope.oGrupo;
            oGrupo.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oGrupo.DataAlteracao = new Date();
            GrupoService.AtualizaGrupo(IdGrupo, oGrupo).then(function successCallback(response) {
                $scope.Grupos = response.data.Retorno;
                $scope.oGrupo = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar grupo!");
        }
    }
 
    // FUNCOES AUX    
    $scope.ListaScriptStatus = function () {
        try {
            ScriptService.ListaScriptStatus().then(function successCallback(response) {
                $scope.ListaStatusScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de script!");
        }
    }
    $scope.ListaScriptPrioridade = function () {
        try {
            ScriptService.ListaScriptPrioridade().then(function successCallback(response) {
                $scope.ListaPrioridadeScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de prioridades de script!");
        }
    }
    $scope.ListaScript = function (nIdScriptStatus, nIdScriptPrioridade) {
        try {
            ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                $scope.Scripts = [];
                $scope.Scripts = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        //Nova Intancia
        $scope.oGrupo = {};
        $scope.ViewMain = 'Views/Sistema/Grupo/InsereGrupo.html';
    }
    $scope.btnEditar = function () {
        var nIdGrupo = this.oGrupo.Id;
        try {
            GrupoService.RetornaGrupo(nIdGrupo).then(function successCallback(response) {
                $scope.oGrupo = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/Grupo/DetalheGrupo.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do grupo!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oGrupo;
        $scope.$emit('atualizaTituloPagina', 'Grupos de Script');
        $scope.ViewMain = 'Views/Sistema/Grupo/ListaGrupo.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Grupos de Script');
  
    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Grupo/ListaGrupo.html';
    $scope.ListaScriptStatus();
    $scope.ListaScriptPrioridade();
    $scope.ListaScript(0, 0);
    $scope.ListaGrupo();

});

