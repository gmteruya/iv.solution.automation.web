﻿angular.module('automationApp').controller('ProdutoVersaoController', function ($scope, $rootScope, ProdutoService, ProdutoVersaoStatusService, ProdutoVersaoService, ProdutoVersaoArquivoService, ProdutoVersaoArquivoTipoService, ArquivoService) {

    //VARIAVEIS
    $scope.fileList = [];
    $scope.ArquivosEnviados = false;

    //FUNCOES
    $scope.ListaProdutoVersao = function () {
        try {
            ProdutoVersaoService.ListaProdutoVersao().then(function successCallback(response) {
                $scope.ProdutoVersoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ProdutoVersao!");
        }
    }
    $scope.InsereProdutoVersao = function () {
        try {

            if ($scope.ValidaProdutoVersaoNovo() == false) {
                alert('Faça upload de todos os arquivos antes de inserir.');
                return;
            }

            var oProdutoVersao = $scope.oProdutoVersao;
            oProdutoVersao.Ativo = true;
            oProdutoVersao.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oProdutoVersao.DataCriacao = new Date();
            ProdutoVersaoService.InsereProdutoVersao(oProdutoVersao).then(function successCallback(response) {
                $scope.ProdutoVersoes.unshift(response.data.Retorno);
                delete $scope.oProdutoVersao;
                $scope.$emit('atualizaTituloPagina', 'Produto Versões');
                $scope.ViewMain = 'Views/Sistema/ProdutoVersao/ListaProdutoVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ProdutoVersao!");
        }
    }
    $scope.AtualizaProdutoVersao = function () {
        try {
            oProdutoVersao = $scope.oProdutoVersao;
            oProdutoVersao.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oProdutoVersao.DataAlteracao = new Date();
            ProdutoVersaoService.AtualizaProdutoVersao(oProdutoVersao).then(function successCallback(response) {
                var bDeuErro = $rootScope.TrataMensagemResponseRPA(response, true);
                if (bDeuErro === false)
                {
                    var nIndexArrayElement = $scope.findArrayIndex($scope.ProdutoVersoes, function (oElement) { return oElement.Id == oProdutoVersao.Id; });
                    $scope.ProdutoVersoes.splice(nIndexArrayElement, 1);
                    $scope.ProdutoVersoes.unshift(response.data.Retorno);
                }

                delete $scope.oProdutoVersao;
                $scope.$emit('atualizaTituloPagina', 'Produto Versões');
                $scope.ViewMain = 'Views/Sistema/ProdutoVersao/ListaProdutoVersao.html';

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ProdutoVersao!");
        }
    }

    // FUNCOES AUX    
    $scope.ListaProduto = function () {
        try {
            ProdutoService.ListaProduto().then(function successCallback(response) {
                $scope.ListaProdutos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ProdutoVersao!");
        }
    }
    $scope.ListaProdutoVersaoStatus = function () {
        try {
            ProdutoVersaoStatusService.ListaProdutoVersaoStatus().then(function successCallback(response) {
                $scope.ListaProdutosVersaoStatus = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ProdutoVersao!");
        }
    }
    $scope.ListaProdutoVersaoArquivo = function () {
        try {
            ProdutoVersaoArquivoService.ListaProdutoVersaoArquivo().then(function successCallback(response) {
                $scope.ProdutoVersaoArquivos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ProdutoVersao!");
        }
    }
    $scope.ListaProdutoVersaoArquivoTipo = function () {
        try {
            ProdutoVersaoArquivoTipoService.ListaProdutoVersaoArquivoTipo().then(function successCallback(response) {
                $scope.ListaArquivoTipo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            alert(err);
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ProdutoVersao!");
        }
    }

    $scope.SetFilesUpload = function (element) {
        //Trata Inicialização do array de files
        if ($scope.fileList === undefined || $scope.fileList === null) {
            $scope.fileList = [];
        }
        var files = element.files;
        $scope.fileList.push(files[files.length - 1]);
        //for (var i = 0; i < files.length; i++) {
        //    $scope.fileList.push(files[i]);
        //}
    };
    $scope.UploadFile = function (fileToUpload, oProdutoVersaoArquivo) {
        var reader = new FileReader();
        reader.onload = function () {

            //File in Binary
            var fileBinary;
            if (reader.result !== null && reader.result !== undefined) {
                fileBinary = reader.result;
            } else {
                fileBinary = reader.content;
            }

            //Insere Arquivo
            oArquivo = {};
            oArquivo.Nome = fileToUpload.name;
            oArquivo.Tamanho = fileToUpload.size;
            oArquivo.Arquivo = btoa(fileBinary); //Base64
            $scope.InsereArquivo(oArquivo, oProdutoVersaoArquivo);

        };
        reader.onerror = function () {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel realizar upload do arquivo!");
        };
        //Implentação crossbrowser
        if (FileReader.prototype.readAsBinaryString === undefined) {
            FileReader.prototype.readAsBinaryString = function (fileToUpload) {
                var binary = '';
                var pt = this;
                var reader = new FileReader();
                reader.onload = function (e) {
                    var bytes = new Uint8Array(reader.result);
                    var length = bytes.byteLength;
                    for (var i = 0; i < length; i++) {
                        binary += String.fromCharCode(bytes[i]);
                    }
                    //pt.result  - readonly so assign content to another property
                    pt.content = binary;
                    pt.onload();
                }
                reader.readAsArrayBuffer(fileToUpload);
            }
        }
        reader.readAsBinaryString(fileToUpload);
    };
    $scope.DownloadFile = function (oArquivo) {
        try {
            var filename = oArquivo.Nome;
            var contentType = oArquivo.ArquivoTipo.MimeType;
            var blob = $scope.ConvertBase64ToBlob(oArquivo.Arquivo, contentType)
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob, filename);
            } else {
                var url = window.URL.createObjectURL(blob);
                var clickEvent;
                if (document.createEvent) {
                    clickEvent = document.createEvent("MouseEvent");
                    clickEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                }
                else {
                    clickEvent = new MouseEvent("click", { "view": window, "bubbles": true, "cancelable": false });
                }
                var linkElement = document.createElement('a');
                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", filename);
                linkElement.dispatchEvent(clickEvent);
            }
        } catch (ex) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel disponibilizar o arquivo para download!");
        }
    };
    $scope.ConvertBase64ToBlob = function (content, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(content);
        var byteArrays = [
        ];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    $scope.InsereArquivo = function (oArquivo, oProdutoVersaoArquivo) {
        try {
            oArquivo.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oArquivo.DataCriacao = new Date();
            ArquivoService.InsereArquivo(oArquivo).then(function successCallback(response) {
                oProdutoVersaoArquivo.IdArquivo = response.data.Retorno.Id;
                oProdutoVersaoArquivo.Arquivo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo arquivo!");
        }
    };

    //VALIDACOES
    $scope.ValidaProdutoVersaoNovo = function () {

        var retorno = true;

        angular.forEach($scope.oProdutoVersao.ProdutoVersaoArquivo, function (value, key) {
            
            value.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            value.DataCriacao = new Date();

            if (value.Enviado == false) {
                retorno = false;
            }

        });

        return retorno;
    };

    // EVENTOS
    $scope.btnNovo = function () {
        //Nova Intancia
        $scope.oProdutoVersao = { ProdutoVersaoArquivo:[] };
        
        angular.forEach($scope.ListaArquivoTipo, function (value, key) {
            var oProdutoVersaoArquivo = { ProdutoVersaoArquivoTipo: {} };
            oProdutoVersaoArquivo.ProdutoVersaoArquivoTipo = value;
            oProdutoVersaoArquivo.IdProdutoVersaoArquivoTipo = value.Id;
            oProdutoVersaoArquivo.Enviado = false;

            $scope.oProdutoVersao.ProdutoVersaoArquivo.push(oProdutoVersaoArquivo);
        });

        $scope.ViewMain = 'Views/Sistema/ProdutoVersao/InsereProdutoVersao.html';
    }
    $scope.btnEditar = function (oProdutoVersao) {
        $scope.oProdutoVersao = oProdutoVersao;
        var nIdProdutoVersao = oProdutoVersao.Id;
        try {
            ProdutoVersaoService.RetornaProdutoVersao(nIdProdutoVersao).then(function successCallback(response) {
                $scope.oProdutoVersao = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ProdutoVersao/DetalheProdutoVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados da versão do produto!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oProdutoVersao;
        $scope.$emit('atualizaTituloPagina', 'Produto Versões');
        $scope.ViewMain = 'Views/Sistema/ProdutoVersao/ListaProdutoVersao.html';
    }
    $scope.btnUpload = function (oProdutoVersaoArquivo) {
        //for (var i = 0; i < $scope.fileList.length; i++) {
        //    $scope.UploadFile($scope.fileList[i], oProdutoVersaoArquivo);
        //}
        $scope.UploadFile($scope.fileList[$scope.fileList.length - 1], oProdutoVersaoArquivo);
        oProdutoVersaoArquivo.Enviado = true;

        if ($scope.fileList.length == $scope.ListaArquivoTipo.length) {
            $scope.ArquivosEnviados = true;
        }
    };
    $scope.btnDownload = function (oArquivo) {
        try {
            var nIdArquivo = oArquivo.Id;
            ArquivoService.RetornaArquivo(nIdArquivo, true, true, true).then(function successCallback(response) {
                $scope.oArquivo = response.data.Retorno;
                $scope.DownloadFile($scope.oArquivo);
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar o arquivo para download!");
        }
    };

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Produto Versões');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ProdutoVersao/ListaProdutoVersao.html';
    $scope.ListaProdutoVersao();
    $scope.ListaProduto();
    $scope.ListaProdutoVersaoStatus();
    $scope.ListaProdutoVersaoArquivoTipo();

});