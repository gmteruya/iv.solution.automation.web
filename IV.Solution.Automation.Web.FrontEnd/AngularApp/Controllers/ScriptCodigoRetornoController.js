﻿angular.module('automationApp').controller('ScriptCodigoRetornoController', function ($scope, $rootScope, ScriptCodigoRetornoService, ClienteService, ScriptService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaScriptCodigoRetorno = function (nIdScript) {
        try {
            ScriptCodigoRetornoService.ListaScriptCodigoRetorno(nIdScript).then(function successCallback(response) {
                $scope.CodigosRetorno = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de códigos de retorno!");
        }
    }
    $scope.InsereScriptCodigoRetorno = function () {
        try {
            var oScriptCodigoRetorno = $scope.oScriptCodigoRetorno;
            oScriptCodigoRetorno.Ativo = true;
            oScriptCodigoRetorno.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptCodigoRetorno.DataCriacao = new Date();
            ScriptCodigoRetornoService.InsereScriptCodigoRetorno(oScriptCodigoRetorno).then(function successCallback(response) {
                $scope.CodigosRetorno.unshift(response.data.Retorno);
                delete $scope.oScriptCodigoRetorno;
                $scope.$emit('atualizaTituloPagina', 'Códigos de Retorno');
                $scope.ViewMain = 'Views/Sistema/ScriptCodigoRetorno/ListaScriptCodigoRetorno.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ScriptCodigoRetorno!");
        }
    }
    $scope.AtualizaScriptCodigoRetorno = function () {
        try {
            var IdScriptCodigoRetorno = $scope.oScriptCodigoRetorno.Id;
            var oScriptCodigoRetorno = $scope.oScriptCodigoRetorno;
            oScriptCodigoRetorno.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptCodigoRetorno.DataAlteracao = new Date();
            ScriptCodigoRetornoService.AtualizaScriptCodigoRetorno(IdScriptCodigoRetorno, oScriptCodigoRetorno).then(function successCallback(response) {
                $scope.CodigosRetorno = response.data.Retorno;
                $scope.oScriptCodigoRetorno = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ScriptCodigoRetorno!");
        }
    }

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = {};
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaScript = function (nIdCliente, nIdScriptStatus, nIdScriptPrioridade) {
        try {
            if (nIdCliente > 0) {
                ScriptService.ListaScriptPorClienteRelacionado(nIdCliente, nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });

            } else {
                ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {

        //Nova Intancia
        $scope.oScriptCodigoRetorno = {};
        $scope.ViewMain = 'Views/Sistema/ScriptCodigoRetorno/InsereScriptCodigoRetorno.html';
    }
    $scope.btnEditar = function () {
        var nIdScriptCodigoRetorno = this.oScriptCodigoRetorno.Id;
        try {
            ScriptCodigoRetornoService.RetornaScriptCodigoRetorno(nIdScriptCodigoRetorno, true).then(function successCallback(response) {
                $scope.oScriptCodigoRetorno = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ScriptCodigoRetorno/DetalheScriptCodigoRetorno.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ScriptCodigoRetorno!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oScriptCodigoRetorno;
        $scope.$emit('atualizaTituloPagina', 'Códigos de Retorno');
        $scope.ViewMain = 'Views/Sistema/ScriptCodigoRetorno/ListaScriptCodigoRetorno.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Códigos de Retorno');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ScriptCodigoRetorno/ListaScriptCodigoRetorno.html';
    $scope.ListaScript($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
    $scope.ListaScriptCodigoRetorno(0);

});

