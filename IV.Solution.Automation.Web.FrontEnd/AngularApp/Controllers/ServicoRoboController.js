﻿angular.module('automationApp').controller('ServicoRoboController', function ($scope, $rootScope, ServicoRoboService, ServicoRoboVersaoService, RoboService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaServicoRobo = function (IdServicoRoboEstado, IdServicoRoboVersao) {
        try {
            ServicoRoboService.ListaServicoRobo(IdServicoRoboEstado, IdServicoRoboVersao).then(function successCallback(response) {
                $scope.ServicoRobos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ServicoRobo!");
        }
    }
    $scope.InsereServicoRobo = function () {
        try {
            var oServicoRobo = $scope.oServicoRobo;
            oServicoRobo.Ativo = true;
            oServicoRobo.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oServicoRobo.DataCriacao = new Date();
            ServicoRoboService.InsereServicoRobo(oServicoRobo).then(function successCallback(response) {
                $scope.ServicoRobos.unshift(response.data.Retorno);
                delete $scope.oServicoRobo;
                $scope.$emit('atualizaTituloPagina', 'Watchdogs');
                $scope.ViewMain = 'Views/Sistema/ServicoRobo/ListaServicoRobo.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ServicoRobo!");
        }
    }
    $scope.AtualizaServicoRobo = function () {
        try {
            var IdServicoRobo = $scope.oServicoRobo.Id;
            var oServicoRobo = $scope.oServicoRobo;
            oServicoRobo.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oServicoRobo.DataAlteracao = new Date();
            ServicoRoboService.AtualizaServicoRobo(IdServicoRobo, oServicoRobo).then(function successCallback(response) {
                $scope.ServicoRobos = response.data.Retorno;
                $scope.oServicoRobo = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ServicoRobo!");
        }
    }

    // FUNCOES AUX
    $scope.ListaServicoRoboEstado = function () {
        try {
            ServicoRoboService.ListaServicoRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoServicoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de ServicoRobo!");
        }
    }
    $scope.ListaServicoRoboVersao = function (nIdServicoRoboVersaoStatus) {
        try {
            ServicoRoboVersaoService.ListaServicoRoboVersao(nIdServicoRoboVersaoStatus).then(function successCallback(response) {
                $scope.ServicoRoboVersoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de versão do ServicoRobo!");
        }
    }
    $scope.ListaRoboEstado = function () {
        try {
            RoboService.ListaRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de robo!");
        }
    }
    $scope.ListaRoboStatus = function () {
        try {
            RoboService.ListaRoboStatus().then(function successCallback(response) {
                $scope.ListaStatusRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Robo!");
        }
    }
    $scope.ListaRoboVersao = function (nIdRoboVersaoStatus) {
        try {
            RoboVersaoService.ListaRoboVersao(nIdRoboVersaoStatus).then(function successCallback(response) {
                $scope.RoboVersoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de versão do robo!");
        }
    }
    $scope.ListaRobo = function (IdRoboEstado, IdRoboStatus, IdRoboVersao, DominioRede) {
        try {
            RoboService.ListaRobo(IdRoboEstado, IdRoboStatus, IdRoboVersao, DominioRede).then(function successCallback(response) {
                $scope.Robos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de Robo!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oServicoRobo = {};
        $scope.$emit('atualizaTituloPagina', 'Inserindo um novo watchdog.');
        $scope.ViewMain = 'Views/Sistema/ServicoRobo/InsereServicoRobo.html';
    }
    $scope.btnEditar = function () {
        var nIdServicoRobo = this.oServicoRobo.Id;
        try {
            ServicoRoboService.RetornaServicoRobo(nIdServicoRobo, true, true, true, true).then(function successCallback(response) {
                $scope.oServicoRobo = response.data.Retorno;
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Watchdog');
                $scope.ViewMain = 'Views/Sistema/ServicoRobo/DetalheServicoRobo.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ServicoRobo!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oServicoRobo;
        $scope.$emit('atualizaTituloPagina', 'Watchdogs');
        $scope.ViewMain = 'Views/Sistema/ServicoRobo/ListaServicoRobo.html';
    }
    $scope.btnReiniciaServicoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de reinicialização dos watchdogs selecionados!'];
        try {
            var lstWatchdogsSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstWatchdogsSelecionados.length > 0) {
                try {
                    ServicoRoboService.ReiniciaServicoRobo(lstWatchdogsSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar os watchdogs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar nenhum watchdog selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Nenhum watchdog selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar os watchdogs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnLigaServicoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de inicialização dos watchdogs selecionados!'];
        try {
            var lstWatchdogsSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstWatchdogsSelecionados.length > 0) {
                try {
                    ServicoRoboService.LigaServicoRobo(lstWatchdogsSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar os watchdogs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar nenhum watchdog selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Nenhum watchdog selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar os watchdogs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnDesligaServicoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de desligamento dos watchdogs selecionados!'];
        try {
            var lstWatchdogsSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstWatchdogsSelecionados.length > 0) {
                try {
                    ServicoRoboService.DesligaServicoRobo(lstWatchdogsSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar os watchdogs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar nenhum watchdog selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Nenhum watchdog selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar os watchdogs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnIniciarMonitoramentoServicoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de inicializar o monitoramento dos watchdogs selecionados!'];
        try {
            var lstWatchdogsSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstWatchdogsSelecionados.length > 0) {
                try {
                    ServicoRoboService.IniciarMonitoramentoServicoRobo(lstWatchdogsSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaRobo(0, 0, 0, '');
                        $scope.ListaServicoRobo(0, 0);
                              $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento dos watchdogs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento de nenhum watchdog selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Nenhum watchdog selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento dos watchdogs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnPararMonitoramentoServicoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de parar o monitoramento dos watchdogs selecionados!'];
        try {
            var lstWatchdogsSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstWatchdogsSelecionados.length > 0) {
                try {
                    ServicoRoboService.PararMonitoramentoServicoRobo(lstWatchdogsSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaRobo(0, 0, 0, '');
                        $scope.ListaServicoRobo(0, 0);
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento dos watchdogs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento de nenhum watchdog selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Nenhum watchdog selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento dos watchdogs selecionados!");
            $rootScope.loading = [false];
        }
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Watchdogs');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ServicoRobo/ListaServicoRobo.html';
    $scope.ListaServicoRoboEstado();
    $scope.ListaServicoRoboVersao(0);
    $scope.ListaRoboEstado();
    $scope.ListaRoboStatus();
    $scope.ListaRoboVersao(0);
    $scope.ListaRobo(0, 0, 0, '');   
    $scope.ListaServicoRobo(0, 0);

});

