﻿angular.module('automationApp').controller('LayoutController', function ($scope, $rootScope, $cookies, $compile, $http) {
    $scope.DataAtual = new Date();
    //Titulo da Pagina
    $scope.TituloPagina = "Home";
    $scope.$on('atualizaTituloPagina', function (event, args) {
        $scope.TituloPagina = args;
    });
});