﻿angular.module('automationApp').controller('DominioController', function ($scope, $rootScope, DominioService, ClienteService, ScriptService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaDominio = function (nIdDominioPai, nIdScript) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                DominioService.ListaDominio($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, nIdDominioPai, nIdScript).then(function successCallback(response) {
                    $scope.Dominios = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                DominioService.ListaDominio(nIdDominioPai, nIdScript).then(function successCallback(response) {
                    $scope.Dominios = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de domínios!");
        }
    }
    $scope.InsereDominio = function () {
        try {
            var oDominio = $scope.oDominio;
            oDominio.Ativo = true;
            oDominio.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oDominio.DataCriacao = new Date();
            DominioService.InsereDominio(oDominio).then(function successCallback(response) {
                $scope.Dominios.unshift(response.data.Retorno);
                $scope.oDominio = '';
                $scope.$emit('atualizaTituloPagina', 'Domínios');
                $scope.ViewMain = 'Views/Sistema/Dominio/ListaDominio.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo Dominio!");
        }
    }
    $scope.AtualizaDominio = function () {
        try {
            var IdDominio = $scope.oDominio.Id;
            var oDominio = $scope.oDominio;
            oDominio.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oDominio.DataAlteracao = new Date();
            DominioService.AtualizaDominio(IdDominio, oDominio).then(function successCallback(response) {
                $scope.Dominios = response.data.Retorno;
                $scope.oDominio = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar Dominio!");
        }
    }

    // FUNCOES AUX    
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = {};
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de processo!");
        }
    }
    $scope.ListaScript = function (nIdCliente, nIdScriptStatus, nIdScriptPrioridade) {
        try {
            if (nIdCliente > 0) {
                ScriptService.ListaScriptPorClienteRelacionado(nIdCliente, nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });

            } else {
                ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oDominio = {};
        $scope.ViewMain = 'Views/Sistema/Dominio/InsereDominio.html';
    }
    $scope.btnEditar = function () {
        var nIdDominio = this.oDominio.Id;
        try {
            DominioService.RetornaDominio(nIdDominio).then(function successCallback(response) {
                $scope.oDominio = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/Dominio/DetalheDominio.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do Dominio!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oDominio;
        $scope.$emit('atualizaTituloPagina', 'Domínios');
        $scope.ViewMain = 'Views/Sistema/Dominio/ListaDominio.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Domínios');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Dominio/ListaDominio.html';
    $scope.ListaCliente();
    $scope.ListaScript($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0, 0);
    $scope.ListaDominio();

});

