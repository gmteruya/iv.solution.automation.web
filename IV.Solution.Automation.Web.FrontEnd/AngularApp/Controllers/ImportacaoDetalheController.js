﻿angular.module('automationApp').controller('ImportacaoDetalheController', function ($scope, $rootScope, ImportacaoDetalheService, ImportacaoLoteService, ImportacaoLoteDemandaService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaImportacaoDetalhe = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ImportacaoDetalheService.ListaImportacaoDetalhePorClienteRelacionado($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Importacoes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ImportacaoDetalheService.ListaImportacaoDetalhe(0).then(function successCallback(response) {
                    $scope.Importacoes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }

        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ImportacaoDetalhe!");
        }
    }
    $scope.InsereImportacaoDetalhe = function () {
        try {
            var oImportacaoDetalhe = $scope.oImportacaoDetalhe;
            oImportacaoDetalhe.Ativo = true;
            oImportacaoDetalhe.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oImportacaoDetalhe.DataCriacao = new Date();
            ImportacaoDetalheService.InsereImportacaoDetalhe(oImportacaoDetalhe).then(function successCallback(response) {
                $scope.ImportacaoDetalhes.unshift(response.data.Retorno);
                delete $scope.oImportacaoDetalhe;
                $scope.$emit('atualizaTituloPagina', 'Importação Detalhe');
                $scope.ViewMain = 'Views/Sistema/ImportacaoDetalhe/ListaImportacaoDetalhe.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ImportacaoDetalhe!");
        }
    }
    $scope.AtualizaImportacaoDetalhe = function () {
        try {
            var IdImportacaoDetalhe = $scope.oImportacaoDetalhe.Id;
            var oImportacaoDetalhe = $scope.oImportacaoDetalhe;
            oImportacaoDetalhe.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oImportacaoDetalhe.DataAlteracao = new Date();
            ImportacaoDetalheService.AtualizaImportacaoDetalhe(IdImportacaoDetalhe, oImportacaoDetalhe).then(function successCallback(response) {
                $scope.ImportacaoDetalhes = response.data.Retorno;
                $scope.oImportacaoDetalhe = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ImportacaoDetalhe!");
        }
    }
    $scope.ListaImportacaoLote = function (nIdImportacaoLote) {
        try {

            ImportacaoLoteService.RetornaImportacaoLote(nIdImportacaoLote, true, true).then(function successCallback(response) {
                $scope.ImportacaoLotes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });

        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ImportacaoDetalhe!");
        }
    }
    // FUNCOES AUX

    // EVENTOS
    
    $scope.btnDetalheImportacaoDetalhe = function () {
        var nIdImportacaoDetalhe = this.oImportacaoDetalhe.Id;
        try {
            ImportacaoLoteService.ListaImportacaoLote(nIdImportacaoDetalhe, true, true).then(function successCallback(response) {

                $scope.ImportacaoLotes = response.data.Retorno;
                $scope.$emit('atualizaTituloPagina', 'Lotes de Importação');
                $scope.ViewMain = 'Views/Sistema/ImportacaoDetalhe/DetalheImportacaoDetalhe.html';

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ImportacaoDetalhe!");
        }
    }
    $scope.btnVoltarListaImportacaoDetalhe = function () {
        $scope.$emit('atualizaTituloPagina', 'Importação Detalhe');
        $scope.ViewMain = 'Views/Sistema/ImportacaoDetalhe/ListaImportacaoDetalhe.html';
        $scope.ListaImportacaoDetalhe();
    }

    $scope.btnDetalheImportacaoLoteDemanda = function () {
        var nIdImportacaoLote = this.oImportacaoLote.Id;
        try {
            ImportacaoLoteDemandaService.ListaImportacaoLoteDemanda(nIdImportacaoLote, 0, 0, true, true, true).then(function successCallback(response) {

                $scope.ImportacaoLoteDemandas = response.data.Retorno;
                $scope.$emit('atualizaTituloPagina', 'Demandas do Lote');
                $scope.ViewMain = 'Views/Sistema/ImportacaoLoteDemanda/DetalheImportacaoLoteDemanda.html';

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ImportacaoLoteDemanda!");
        }
    }
    $scope.btnVoltarImportacaoDetalhe = function () {
        $scope.$emit('atualizaTituloPagina', 'Lotes de Importação');
        $scope.ViewMain = 'Views/Sistema/ImportacaoDetalhe/DetalheImportacaoDetalhe.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Importação Detalhe');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ImportacaoDetalhe/ListaImportacaoDetalhe.html';
    $scope.ListaImportacaoDetalhe();

});