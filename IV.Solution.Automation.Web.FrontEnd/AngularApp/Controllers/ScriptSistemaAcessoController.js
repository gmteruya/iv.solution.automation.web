﻿angular.module('automationApp').controller('ScriptSistemaAcessoController', function ($scope,
                                                                                      $rootScope,
                                                                                      $timeout,
                                                                                      $uibModal,
                                                                                      ScriptSistemaAcessoService,
                                                                                      ScriptSistemaService) {

    //VARIAVEIS
    $scope.SenhaAtual = '';

    //FUNCOES
    $scope.ListaScriptSistemaAcesso = function (nIdScriptSistema, nIdScriptSistemaAcessoStatus) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ScriptSistemaAcessoService.ListaScriptSistemaAcessoPorPerfilAcesso($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdPerfilAcesso, nIdScriptSistema, nIdScriptSistemaAcessoStatus).then(function successCallback(response) {
                    $scope.ScriptSistemaAcessos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptSistemaAcessoService.ListaScriptSistemaAcesso(nIdScriptSistema, nIdScriptSistemaAcessoStatus).then(function successCallback(response) {
                    $scope.ScriptSistemaAcessos = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ScriptSistemaAcesso!");
        }
    }
    $scope.InsereScriptSistemaAcesso = function () {
        try {
            var oScriptSistemaAcesso = $scope.oScriptSistemaAcesso;
            oScriptSistemaAcesso.Ativo = true;
            oScriptSistemaAcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptSistemaAcesso.DataCriacao = new Date();
            ScriptSistemaAcessoService.InsereScriptSistemaAcesso(oScriptSistemaAcesso).then(function successCallback(response) {
                var bDeuErro = $rootScope.TrataMensagemResponseRPA(response, true);
                if (bDeuErro === false) {
                    $scope.ScriptSistemaAcessos.unshift(response.data.Retorno);
                }
                delete $scope.oScriptSistemaAcesso;
                $scope.$emit('atualizaTituloPagina', 'Usuários de Sistemas Automatizados');
                $scope.ViewMain = 'Views/Sistema/ScriptSistemaAcesso/ListaScriptSistemaAcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ScriptSistemaAcesso!");
        }
    }
    $scope.AtualizaScriptSistemaAcesso = function () {
        try {
            if ($scope.ValidaFormulario() != true) {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Dados inválidos no formulário!");
                return;
            }

            var nIdScriptSistemaAcesso = $scope.oScriptSistemaAcesso.Id;
            var oScriptSistemaAcesso = $scope.oScriptSistemaAcesso;
            oScriptSistemaAcesso.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oScriptSistemaAcesso.DataAlteracao = new Date();
            ScriptSistemaAcessoService.AtualizaScriptSistemaAcesso(nIdScriptSistemaAcesso, oScriptSistemaAcesso).then(function successCallback(response) {
                var bDeuErro = $rootScope.TrataMensagemResponseRPA(response, true);
                if (bDeuErro === false) {
                    var nIndexArrayElement = $scope.findArrayIndex($scope.ScriptSistemaAcessos, function (oElement) { return oElement.Id == nIdScriptSistemaAcesso; });
                    $scope.ScriptSistemaAcessos.splice(nIndexArrayElement, 1);
                    $scope.ScriptSistemaAcessos.unshift(response.data.Retorno);
                }
                delete $scope.oScriptSistemaAcesso;
                $scope.$emit('atualizaTituloPagina', 'Usuários de Sistemas Automatizados');
                $scope.ViewMain = 'Views/Sistema/ScriptSistemaAcesso/ListaScriptSistemaAcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ScriptSistemaAcesso!");
        }
    }

    // FUNCOES AUX  
    $scope.ListaScriptSistema = function (nIdCliente) {
        try {
            if (nIdCliente > 0) {
                ScriptSistemaService.ListaScriptSistemaPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.ScriptSistemas = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptSistemaService.ListaScriptSistema().then(function successCallback(response) {
                    $scope.ScriptSistemas = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de perfis de acesso!");
        }
    }
    $scope.ListaScriptSistemaAcessoStatus = function () {
        try {
            ScriptSistemaAcessoService.ListaScriptSistemaAcessoStatus().then(function successCallback(response) {
                $scope.StatusAcessoSistema = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de sistema acesso!");
        }
    }
    $scope.ValidaConfirmacaoSenha = function () {
        try {
            if ($scope.SenhaAtual != $scope.oScriptSistemaAcesso.Senha) {
                if ($scope.oScriptSistemaAcesso.ConfirmacaoSenha != $scope.oScriptSistemaAcesso.Senha) {
                    $scope.$$childTail.frmScriptSistemaAcesso.$$controls[4].$setValidity("txtConfirmacaoSenha", false);
                } else {
                    $scope.$$childTail.frmScriptSistemaAcesso.$$controls[4].$setValidity("txtConfirmacaoSenha", true);
                }
            }
            else {
                $scope.$$childTail.frmScriptSistemaAcesso.$$controls[4].$setValidity("txtConfirmacaoSenha", true);
            }
        } catch (e) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel comparar as senhas!");
        }
    };
    $scope.ValidaFormulario = function () {
        try {
            if ($scope.SenhaAtual != $scope.oScriptSistemaAcesso.Senha) {
                if ($scope.oScriptSistemaAcesso.ConfirmacaoSenha != $scope.oScriptSistemaAcesso.Senha) {
                    $scope.$$childTail.frmScriptSistemaAcesso.$$controls[4].$setValidity("txtConfirmacaoSenha", false);
                    return false;
                } else {
                    $scope.$$childTail.frmScriptSistemaAcesso.$$controls[4].$setValidity("txtConfirmacaoSenha", true);
                    return true;
                }
            }
            else {
                $scope.$$childTail.frmScriptSistemaAcesso.$$controls[4].$setValidity("txtConfirmacaoSenha", true);
                return true;
            }
        } catch (e) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel comparar as senhas!");
            return false;
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $rootScope.loading = [true, 'Carregando'];
        $scope.oScriptSistemaAcesso = {};
        $scope.oScriptSistemaAcesso.IdScriptSistemaAcessoStatus = 1; //Desconectado
        $scope.$emit('atualizaTituloPagina', 'Inserindo um novo usuário de sistema automatizado.');
        $scope.ViewMain = 'Views/Sistema/ScriptSistemaAcesso/InsereScriptSistemaAcesso.html';
        $rootScope.loading = [false];
    }
    $scope.btnEditar = function () {
        var nIdScriptSistemaAcesso = this.oScriptSistemaAcesso.Id;
        try {
            $scope.SenhaAtual = '';
            ScriptSistemaAcessoService.RetornaScriptSistemaAcesso(nIdScriptSistemaAcesso).then(function successCallback(response) {
                $scope.oScriptSistemaAcesso = response.data.Retorno;
                $scope.SenhaAtual = $scope.oScriptSistemaAcesso.Senha;
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Usuário de Sistema Automatizado');
                $scope.ViewMain = 'Views/Sistema/ScriptSistemaAcesso/DetalheScriptSistemaAcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ScriptSistemaAcesso!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oScriptSistemaAcesso;
        $scope.$emit('atualizaTituloPagina', 'Usuários de Sistemas Automatizados');
        $scope.ViewMain = 'Views/Sistema/ScriptSistemaAcesso/ListaScriptSistemaAcesso.html';
    }
    $scope.btnFiltroPesquisaAvancada = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/ScriptSistemaAcesso/PesquisaAvancadaScriptSistemaAcesso.html',
            controller: 'ScriptSistemaAcessoPesquisaAvancadaController',
            size: sSize,
            backdrop: true,
            keyboard: true
        });
        modalInstance.result.then(function (oFiltroPesquisaAvancada) {
            $rootScope.loading = [true, 'Carregando'];
            var nIdScriptSistema = 0;
            var nIdScriptSistemaAcessoStatus = 0;
            if (oFiltroPesquisaAvancada !== undefined && oFiltroPesquisaAvancada !== null) {
                if (oFiltroPesquisaAvancada.IdScriptSistema !== undefined && oFiltroPesquisaAvancada.IdScriptSistema !== null) {
                    nIdScriptSistema = oFiltroPesquisaAvancada.IdScriptSistema;
                }
                if (oFiltroPesquisaAvancada.IdScriptSistemaAcessoStatus !== undefined && oFiltroPesquisaAvancada.IdScriptSistemaAcessoStatus !== null) {
                    nIdScriptSistemaAcessoStatus = oFiltroPesquisaAvancada.IdScriptSistemaAcessoStatus;
                }
            }
            //Pesquisa/Atualiza na base novamente.
            $scope.ListaScriptSistemaAcesso(nIdScriptSistema, nIdScriptSistemaAcessoStatus);
            $rootScope.loading = [false];
        }, function () {

        });
    };

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Usuários de Sistemas Automatizados');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ScriptSistemaAcesso/ListaScriptSistemaAcesso.html';
    $scope.ListaScriptSistema($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
    $scope.ListaScriptSistemaAcessoStatus();
    $scope.ListaScriptSistemaAcesso(0, 0);

});

angular.module('automationApp').controller('ScriptSistemaAcessoPesquisaAvancadaController', function ($scope,
                                                                                                      $rootScope,
                                                                                                      $uibModalInstance,
                                                                                                      ClienteService,
                                                                                                      ScriptSistemaAcessoService,
                                                                                                      ScriptSistemaService) {

    // VARIAVEIS
    $scope.oFiltroPesquisaAvancada = {};
    if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
        $scope.oFiltroPesquisaAvancada.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
    }

    // FUNCOES AUX
    $scope.ListaCliente = function () {
        $scope.Clientes = {};
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de clientes!");
        }
    }
    $scope.ListaScriptSistema = function (nIdCliente) {
        try {
            if (nIdCliente > 0) {
                ScriptSistemaService.ListaScriptSistemaPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.ScriptSistemas = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptSistemaService.ListaScriptSistema().then(function successCallback(response) {
                    $scope.ScriptSistemas = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de sistemas automatizados!");
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de perfis de acesso!");
        }
    }
    $scope.ListaScriptSistemaAcessoStatus = function () {
        try {
            ScriptSistemaAcessoService.ListaScriptSistemaAcessoStatus().then(function successCallback(response) {
                $scope.StatusAcessoSistema = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de sistema acesso!");
        }
    }

    //INIT DOMINIOS
    $scope.ListaCliente();
    $scope.ListaScriptSistema($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
    $scope.ListaScriptSistemaAcessoStatus();

    //PESQUISAR
    $scope.btnPesquisar = function () {

        //Retorna Parametros da Pesquisa Avançada
        $uibModalInstance.close($scope.oFiltroPesquisaAvancada);
    };

    //FECHAR
    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };
});