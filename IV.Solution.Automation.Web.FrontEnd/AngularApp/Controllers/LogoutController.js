﻿angular.module('automationApp').controller('LogoutController', function ($scope, $rootScope, $location, $cookies, $http, UtilService, AuthenticationFactory, UsuarioSessaoService) {
    //Pega Sessao
    var Authentication = $rootScope.globals.Authentication;
                        
    //Seta Data Logout
    oUsuarioSessao = Authentication.UsuarioSessao;
    oUsuarioSessao.DataLogout = new Date();

    //TODO: Ajuste de Gravacao
    UsuarioSessaoService.EncerraUsuarioSessao(oUsuarioSessao).then(function (usuario_sessao) {
                
        //Limpa Credenciais
        AuthenticationFactory.ClearCredentials();

        //Redireciona para area inicial.
        UtilService.DOM.PageRedirect('Index.html');

    }, function (error) {
        if (error.data != null) {
            $scope.error = error.data.error_description
        }               
    });
 });                       
       