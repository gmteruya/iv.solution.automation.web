﻿angular.module('automationApp').controller('MenuController', function ($scope, $rootScope, MenuService) {

    // FUNCOES CRUD
    $scope.ListaMenu = function (nIdMenuTipo) {
        try {
            MenuService.ListaMenu(nIdMenuTipo).then(function successCallback(response) {
                $scope.Menus = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de itens de menu!");
        }
    }
    $scope.InsereMenu = function () {
        try {
            var oMenu = $scope.oMenu;
            oMenu.Ativo = true;
            oMenu.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oMenu.DataCriacao = new Date();
            MenuService.InsereMenu(oMenu).then(function successCallback(response) {
                $scope.Menus.unshift(response.data.Retorno);
                delete $scope.oMenu;
                $scope.$emit('atualizaTituloPagina', 'Itens de Menu');
                $scope.ViewMain = 'Views/Sistema/Menu/ListaMenu.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo item de menu!");
        }
    }
    $scope.AtualizaMenu = function () {
        try {
            var IdMenu = $scope.oMenu.Id;
            var oMenu = $scope.oMenu;
            oMenu.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oMenu.DataAlteracao = new Date();
            MenuService.AtualizaMenu(IdMenu, oMenu).then(function successCallback(response) {
                $scope.Menus = response.data.Retorno;
                $scope.oMenu = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar o item de menu!");
        }
    }

    // FUNCOES AUX
    $scope.ListaMenuTipo = function () {
        try {
            MenuService.ListaMenuTipo().then(function successCallback(response) {
                $scope.TiposMenus = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de tipos de menu!");
        }
    }

    // FUNCOES ESPECIFICAS

    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oMenu = {};
        $scope.ViewMain = 'Views/Sistema/Menu/InsereMenu.html';
    }
    $scope.btnEditar = function () {
        var nIdMenu = this.oMenu.Id;
        try {
            MenuService.RetornaMenu(nIdMenu).then(function successCallback(response) {
                $scope.oMenu = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/Menu/DetalheMenu.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do usuário!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oMenu;
        $scope.$emit('atualizaTituloPagina', 'Itens de Menu');
        $scope.ViewMain = 'Views/Sistema/Menu/ListaMenu.html';
    }

    //Titulo da Página
    $scope.$emit('atualizaTituloPagina', 'Itens de Menu');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Menu/ListaMenu.html';
    $scope.ListaMenuTipo();
    $scope.ListaMenu();

});

