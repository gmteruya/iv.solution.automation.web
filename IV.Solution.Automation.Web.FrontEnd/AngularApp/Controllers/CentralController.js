﻿angular.module('automationApp').controller('CentralController', function ($scope, $rootScope, AuthenticationFactory, UsuarioService) {
    
    //EVENTOS
    $scope.btnSimulaAtualizacaoToken = function () {
        try {

            //Pega as Credenciais 
            var oCredentials = AuthenticationFactory.GetCredentials();
            if (oCredentials) {

                //Pega Token de Atualização
                var refresh_token = oCredentials.Authentication.UsuarioAuthentication.refresh_token;
                var client_id = oCredentials.Authentication.UsuarioAuthentication.id_retorno;

                //Atualiza o Token
                UsuarioService.AtualizaToken(refresh_token, client_id).then(function successCallback(response) {
                    $scope.Token = response;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }            
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualiza o token!");
        }
    }

    //TITULO DA PÁGINA / VIEW
    $scope.$emit('atualizaTituloPagina', 'Central do RPA');
    $scope.ViewMain = 'Views/Sistema/Central.html';
    
});