﻿angular.module('automationApp').controller('ProcessoController', function ($scope, $rootScope, $uibModal, ProcessoService, ScriptService, GrupoService, ClienteService, ScriptCodigoRetornoService, sAcao) {
    $scope.init = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                $scope.EtapaInsereProcesso = { Titulo: 'Selecione o grupo', Etapa: 1 };
            } else {
                $scope.EtapaInsereProcesso = { Titulo: 'Selecione o cliente', Etapa: 0 };
            }            
        } catch (e) {
            $scope.error = "Erro $scope.init=" + e.message;
        }
    };

    $scope.EtapaInsereProcesso_Click = function (Etapa) {
        try {
            $scope.EtapaInsereProcesso.Etapa = Etapa;
            switch (Etapa) {
                case 0:
                    $scope.EtapaInsereProcesso.Titulo = 'Selecione o cliente';
                    $scope.oProcesso.IdScript = 0;
                    $scope.oProcesso.Script = null;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
                    break;
                case 1:
                    $scope.EtapaInsereProcesso.Titulo = 'Selecione o grupo';
                    $scope.oProcesso.IdScript = 0;
                    $scope.oProcesso.Script = null;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
                    break;
                case 2:
                    $scope.EtapaInsereProcesso.Titulo = 'Selecione o tipo de processo';
                    $scope.oProcesso.IdScript = 0;
                    $scope.oProcesso.Script = null;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
                    break;
                case 3:
                    $scope.EtapaInsereProcesso.Titulo = 'Formulário';
                    $scope.oProcesso.ChaveProcesso = null;
                    $scope.oProcesso.ScriptInfosAdicionais = {};
                    break;
            }
        } catch (e) {
            $scope.error = "Não foi possivel selecionar o Card! params: nStage=" + Etapa + "|Error=" + e.message;
        };
    };

    $scope.ItemRetorno_Click = function (IdCliente, IdGrupo, IdScript, Etapa, Scripts) {
        try {
            if (Etapa == 0) {
                $scope.oProcesso.IdCliente = IdCliente;
                $scope.oProcesso.IdScript = 0;
                $scope.oProcesso.Script = null;
                $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
            }
            if (Etapa == 1) {
                $scope.oProcesso.IdGrupo = IdGrupo;
                $scope.oProcesso.IdScript = 0;
                $scope.oProcesso.Script = null;
                $scope.oProcesso.IdProcessoClassificacaoPrioridade = 0;
            }
            $scope.oProcesso.IdScript = IdScript;
            $scope.oProcesso.ChaveProcesso = null;
            $scope.oProcesso.ScriptInfosAdicionais = {};
            $scope.EtapaInsereProcesso_Click(Etapa);
        } catch (e) {
            $scope.error = "Não foi possivel selecionar o Card! params: nStage=" + Etapa + "|Error=" + e.message;
        };
    };

    //FUNCOES CRUD
    $scope.ListaProcesso = function (nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica) {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ProcessoService.ListaProcessoPerfilAcesso($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdPerfilAcesso, nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica).then(function successCallback(response) {
                    $scope.Processos = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de processo!";
                });
            } else {
                ProcessoService.ListaProcesso(nIdProcessoStatus, nIdProcessoClassificacaoPrioridade, nIdProcessoPai, nIdScript, nIdGrupo, nIdCliente, nIdProcessoChaveUnica).then(function successCallback(response) {
                    $scope.Processos = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de processo!";
                });
            }
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de processo!";
        }
    }
    $scope.InsereProcesso = function () {
        $rootScope.loading = [true, 'Carregando'];
        try {
            var oProcesso = $scope.oProcesso;
            oProcesso.IdProcessoStatus = 1; //Em Geração
            oProcesso.Ativo = true;
            oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oProcesso.DataCriacao = new Date();
            ProcessoService.InsereProcesso(oProcesso).then(function successCallback(response) {
                //Pega o Retorno
                oProcesso = response.data.Retorno;

                //Atualiza Scope
                $scope.oProcesso = oProcesso;

                //Trata Retorno
                if (response.data === null) {//Se retorna erro fatal
                    delete $scope.oProcesso;
                    $scope.error = response.data.MsgRetorno;
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal RPA Informa
                    $scope.showModalRPAInformaErroExecucao('lg');
                } else if (oProcesso.IdProcessoStatus === 2) {//Processo Gerado
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal RPA Informa
                } else if (oProcesso.IdProcessoStatus === 3) {//Aguardando Execução
                    delete $scope.oProcesso;
                    $scope.ListaScript(0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
                    $scope.ListaProcesso(0, 0, 0, 0, 0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0);
                    $scope.$emit('atualizaTituloPagina', 'Processo - Consulta');
                    $scope.ViewMain = 'Views/Sistema/Processo/ListaProcesso.html';
                } else if (oProcesso.IdProcessoStatus === 5) {//Em Execução                     
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal do Step
                    $scope.showModalScriptStep('lg', oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1], response.data.MsgRetorno);
                } else if (oProcesso.IdProcessoStatus === 8 || oProcesso.IdProcessoStatus === 13) {//Executado com Sucesso
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal RPA Informa
                    $scope.showModalRPAInforma('lg', 'Execução Finalizada com Sucesso :-)');
                } else if (oProcesso.IdProcessoStatus === 9) {//Erro Execução
                    $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                    //Abre Modal RPA Informa
                    $scope.showModalRPAInformaErroExecucao('lg');
                }
                
                //Retira Lodear;
                $rootScope.loading = [false];
            }, function errorCallback(response) {
                $scope.error = response.data.MsgRetorno;
                $rootScope.loading = [false];
            });
        } catch (err) {
            $scope.error = "Não foi possivel inserir novo processo!";
            $rootScope.loading = [false];
        }      
    }
    $scope.AtualizaProcesso = function () {
        try {
            var IdProcesso = $scope.oProcesso.Id;
            var oProcesso = $scope.oProcesso;
            oProcesso.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oProcesso.DataAlteracao = new Date();
            ProcessoService.AtualizaProcesso(IdProcesso, oProcesso).then(function successCallback(response) {
                $scope.Processos = response.data.Retorno;
                $scope.oProcesso = '';
            }, function errorCallback(response) {
                alert(response);
            });
        } catch (err) {
            $scope.error = "Não foi possivel atualizar Processo!";
        }
    }
    
    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.push(response.data.Retorno);
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de status de processo!";
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de status de processo!";
                });
            }
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de status de processo!";
        }
    }
    $scope.ListaGrupo = function (nIdCliente) {
        try {

            if ($scope.oProcesso != undefined) {
                $scope.oProcesso.IdCliente = nIdCliente;
                $scope.EtapaInsereProcesso_Click(1);
            }

            if (nIdCliente > 0) {
                GrupoService.ListaGrupoPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de grupos de processo!";
                });
            } else {
                GrupoService.ListaGrupo().then(function successCallback(response) {
                    $scope.Grupos = response.data.Retorno;
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de grupos de processo!";
                });
            }
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de grupos de processo!";
        }
    }
    $scope.ListaScript = function (nIdGrupo, nIdCliente) {
        try {
            if (nIdGrupo > 0) {
                //$rootScope.loading = [true, 'Carregando'];
                $scope.oProcesso.IdGrupo = nIdGrupo;
                $scope.Scripts = {};
                $scope.EtapaInsereProcesso_Click(2);

                //Carrega os scripts
                ScriptService.ListaScriptPorGrupoRelacionado(nIdGrupo).then(function successCallback(response) {
                    $scope.Scripts = response.data.Retorno;
                    //$rootScope.loading = [false];
                }, function errorCallback(response) {
                    $scope.error = "Não foi possivel carregar a lista de tipos de processos!";
                });
            } else {

                if ($scope.oProcesso != undefined) {
                    $scope.oProcesso.IdCliente = nIdCliente;
                }

                if (nIdCliente > 0) {
                    ScriptService.ListaScriptPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                        $scope.Scripts = response.data.Retorno;
                    }, function errorCallback(response) {
                        $scope.error = "Não foi possivel carregar a lista de tipos de processos!";
                    });
                } else {
                    ScriptService.ListaScript().then(function successCallback(response) {
                        $scope.Scripts = response.data.Retorno;
                    }, function errorCallback(response) {
                        $scope.error = "Não foi possivel carregar a lista de tipos de processos!";
                    });
                }
            } 
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de status de processo!";
        }
    }

    $scope.ListaHistoricoClassificacao = function () {
        try {
            ProcessoService.ListaHistoricoClassificacao().then(function successCallback(response) {
                $scope.ListaClassificacoesHistorico = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de classificações de histórico!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de classificações de histórico!";
        }
    }

    $scope.ListaInteracaoTipo = function () {
        try {
            ProcessoService.ListaInteracaoTipo().then(function successCallback(response) {
                $scope.ListaInteracoesTipo = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de tipo de interacoes de processo!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de tipo de interacoes de processo!";
        }
    }

    $scope.ListaProcessoStatus = function () {
        try {
            ProcessoService.ListaProcessoStatus().then(function successCallback(response) {
                $scope.ListaStatusProcesso = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de status de processo!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de status de processo!";
        }
    }

    $scope.ListaProcessoClassificacaoPrioridade = function () {
        try {
            ProcessoService.ListaProcessoClassificacaoPrioridade().then(function successCallback(response) {
                $scope.ClassificacoesPrioridade = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de tipos de Processo!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de status de Processo!";
        }
    }

    $scope.ListaScriptCodigoRetorno = function (nIdScript) {
        try {
            ScriptCodigoRetornoService.ListaScriptCodigoRetorno(nIdScript).then(function successCallback(response) {
                $scope.CodigosRetornos = response.data.Retorno;
            }, function errorCallback(response) {
                $scope.error = "Não foi possivel carregar a lista de codigos de retorno!";
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar a lista de codigos de retorno!";
        }
    }

    $scope.ControlaExibicaoPermissao = function (nIdGrupo, nIdScript, sEntidadeControlada, sPermissao) {
        var bExibeEntidade = false;
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente === 0) {
            bExibeEntidade = true;
        } else {
            var arPerfilAcessoScriptGrupo;
            arPerfilAcessoScriptGrupo = $rootScope.globals.Authentication.UsuarioSessao.Usuario.PerfilAcesso.PerfilAcessoScriptGrupo;
            if (sEntidadeControlada === 'Grupo') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo) {
                        switch(sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;                               
                        }                        
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
            if (sEntidadeControlada === 'Script') {
                for (var i = 0; i < arPerfilAcessoScriptGrupo.length; i++) {
                    var oPerfilAcessoScriptGrupo = arPerfilAcessoScriptGrupo[i];
                    if (oPerfilAcessoScriptGrupo.ScriptGrupo.IdGrupo === nIdGrupo && oPerfilAcessoScriptGrupo.ScriptGrupo.IdScript === nIdScript) {
                        switch (sPermissao) {
                            case 'Incluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Incluir;
                                break;
                            case 'Alterar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Alterar;
                                break;
                            case 'Excluir':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Excluir;
                                break;
                            case 'Desbloquear':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desbloquear;
                                break;
                            case 'Reprocessar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Reprocessar;
                                break;
                            case 'Associar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.bExibeEntidade;
                                break;
                            case 'Desassociar':
                                bExibeEntidade = oPerfilAcessoScriptGrupo.Desassociar;
                                break;
                            default:
                                bExibeEntidade = true;
                        }
                        if (bExibeEntidade === true) {
                            break;
                        }
                    }
                }
            };
        };
        return bExibeEntidade;
    }

    //ABRE FORMULARIO
    $scope.RetornaScript = function (IdScript) {
        try {
            if (IdScript > 0) {               
                ScriptService.RetornaScript(IdScript, false, false, false, false, false, false, true).then(function successCallback(response) {
                    $scope.oProcesso.IdScript = IdScript;
                    $scope.oProcesso.Script = response.data.Retorno;
                    $scope.oProcesso.IdProcessoClassificacaoPrioridade = $scope.oProcesso.Script.IdProcessoClassificacaoPrioridade;
                    $scope.EtapaInsereProcesso_Click(3);
                }, function errorCallback(response) {
                    alert(response);
                });
            } else {
                $scope.oProcesso.Script = null;
            }
        } catch (err) {
            $scope.error = "Não foi possivel carregar os dados do Script!";
        }
    }
   
    //EVENTOS
    $scope.btnNovo = function () {
        delete $scope.oProcesso;
        $scope.oProcesso = {};
        $scope.oProcesso.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
        $scope.EtapaInsereProcesso_Click(1);
        $scope.ListaCliente();
        $scope.ListaGrupo($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
        $scope.Scripts = null;
        $scope.init();
        $scope.$emit('atualizaTituloPagina', 'Processo - Novo');
        $scope.ViewMain = 'Views/Sistema/Processo/InsereProcesso.html';
    }
    $scope.btnEditar = function () {
        try {
            var nIdProcesso = this.oProcesso.Id;
            var sAutor = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            ProcessoService.RetornaProcesso(nIdProcesso, sAutor, true, true, true, true, true, true, true, true, true, true).then(function successCallback(response) {
                $scope.oProcesso = response.data.Retorno;
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Processo de ' + $scope.oProcesso.Script.Descricao);
                $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
                $scope.ViewMainAnterior = 'Views/Sistema/Processo/DetalheProcesso.html';
            }, function errorCallback(response) {
                alert(response);
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar os dados do Processo!";
        }
    }
    $scope.btnExcluir = function () {
        try {
            var nIdProcesso = $scope.oProcesso.Id;
            var sAutor = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            ProcessoService.RetornaProcesso(nIdProcesso, sAutor, false, false, false, false, false, false, false, false, false, false).then(function successCallback(response) {
                $scope.oProcesso = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/Processo/DetalheProcesso.html';
            }, function errorCallback(response) {
                alert(response);
            });
        } catch (err) {
            $scope.error = "Não foi possivel carregar os dados do Processo!";
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oProcesso;
        $scope.ListaCliente();
        $scope.ListaGrupo($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
        $scope.ListaScript(0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
        $scope.ListaProcesso(0, 0, 0, 0, 0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0);
        $scope.$emit('atualizaTituloPagina', 'Processo - Consulta');
        $scope.ViewMain = 'Views/Sistema/Processo/ListaProcesso.html';
    }
    $scope.btnAbrirMaisInformacoes = function () {
        $('.divMaisInformacoes').show(200);
    }
    $scope.btnFecharMaisInformacoes = function () {
        $('.divMaisInformacoes').hide(200);
    }

    //MODAL PESQUISA AVANÇADA
    $scope.showModalPesquisaAvancada = function (sSize) {
        var oProcessoExecucao = this.oProcessoExecucao;
        oProcessoExecucao.Processo = $scope.oProcesso;
        $scope.oProcessoExecucao = oProcessoExecucao;
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/ProcessoExecucao.html',
            controller: 'ProcessoExecucaoController',
            size: sSize,
            backdrop: true,
            keyboard: true,
            resolve: {
                oProcessoExecucao: function () {
                    return $scope.oProcessoExecucao;
                },
                ListaStatusProcesso: function () {
                    return $scope.ListaStatusProcesso;
                }
            }
        });
        modalInstance.result.then(function (oProcessoExecucao) {
            $rootScope.loading = [true, 'Carregando'];
            $rootScope.loading = [false];
        }, function () {
            $rootScope.loading = [true, 'Carregando'];
            $rootScope.loading = [false];
        });
    };

    //MODAL STEP DA EXECUCAO
    $scope.showModalScriptStep = function (sSize, oProcessoExecucaoScriptStep, sMsgRetorno) {
        //Pre
        var sController;
        if (!oProcessoExecucaoScriptStep.ScriptStep.Controller) {
            sController = 'ModalProcessoExecucaoStepController'
        }
        else {
            sController = oProcessoExecucaoScriptStep.ScriptStep.Controller
        }

        //Open
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: oProcessoExecucaoScriptStep.ScriptStep.TemplateUrl,
            controller: sController,
            size: sSize,
            backdrop: false,
            keyboard: false,
            resolve: {
                oProcesso: function () {
                    return $scope.oProcesso;
                },
                MsgRetorno: function () {
                    return sMsgRetorno;
                },
            }
        });
        modalInstance.result.then(function (oProcesso) {
            //Continua Execução
            $rootScope.loading = [true, 'Carregando'];
            try {
                var oProcesso = $scope.oProcesso;
                oProcesso.IdProcessoStatus = 5; //Em Execução
                oProcesso.Ativo = true;
                oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oProcesso.DataCriacao = new Date();
                ProcessoService.ContinuaExecucaoProcesso(oProcesso).then(function successCallback(response) {
                    
                    //Pega o Retorno
                    oProcesso = response.data.Retorno;

                    //Atualiza Scope
                    $scope.oProcesso = oProcesso;

                    //Trata Status de Retorno
                    if (oProcesso.IdProcessoStatus === 5) {//Em Execução 
                        //Abre Modal do Step
                        $scope.showModalScriptStep('lg', oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep[oProcesso.ProcessoExecucao[oProcesso.ProcessoExecucao.length - 1].ProcessoExecucaoScriptStep.length - 1], response.data.MsgRetorno);
                    } else if (oProcesso.IdProcessoStatus === 8 || oProcesso.IdProcessoStatus === 13) {//Executado com Sucesso
                        //Abre Modal RPA Informa
                        $scope.showModalRPAInforma('lg');
                    } else if (oProcesso.IdProcessoStatus === 9) {//Erro Execução
                        //Abre Modal RPA Informa
                        $scope.showModalRPAInformaErroExecucao('lg');
                    }
                    
                    //Retira Lodear;
                    $rootScope.loading = [false];
                }, function errorCallback(response) {
                    $scope.error = response.data.MsgRetorno;
                    $rootScope.loading = [false];
                });
            } catch (err) {
                $scope.error = "Não foi possivel inserir novo Processo!";
                $rootScope.loading = [false];
            }
        }, function () {
            //Cancela Execução
            $rootScope.loading = [true, 'Carregando'];
            try {
                var oProcesso = $scope.oProcesso;
                oProcesso.IdProcessoStatus = 4; //Em Execução
                oProcesso.Ativo = true;
                oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oProcesso.DataCriacao = new Date();
                ProcessoService.CancelaExecucaoProcesso(oProcesso).then(function successCallback(response) {
                    //Pega o Retorno
                    oProcesso = response.data.Retorno;

                    //Atualiza Scope
                    $scope.oProcesso = oProcesso;

                    //Exibe Modal de Execução Cancelada.
                    $scope.showModalRPAInformaCancelado('lg');

                    //Retira Lodear;
                    $rootScope.loading = [false];
                }, function errorCallback(response) {
                    $scope.error = response.data.MsgRetorno;
                    $rootScope.loading = [false];
                });
            } catch (err) {
                $scope.error = "Não foi possivel inserir novo Processo!";
                $rootScope.loading = [false];
            }

            $scope.oProcesso = $scope.oProcesso;
        });
    };

    //MODAL EXECUÇÂO
    $scope.showModalProcessoExecucao = function (sSize) {
        var oProcessoExecucao = this.oProcessoExecucao;
        oProcessoExecucao.Processo = $scope.oProcesso;
        $scope.oProcessoExecucao = oProcessoExecucao;
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/ProcessoExecucao.html',
            controller: 'ProcessoExecucaoController',
            size: sSize,
            backdrop: true,
            keyboard: true,
            resolve: {
                oProcessoExecucao: function () {
                    return $scope.oProcessoExecucao;
                },
                ListaStatusProcesso: function () {
                    return $scope.ListaStatusProcesso;
                }
            }
        });
        modalInstance.result.then(function (oProcessoExecucao) {
            $rootScope.loading = [true, 'Carregando'];           
            $rootScope.loading = [false];
        }, function () {
            $rootScope.loading = [true, 'Carregando'];
            $rootScope.loading = [false];
        });
    };

    //MODAL ANOTACAO
    $scope.showModalNovaAnotacao = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/NovaAnotacao.html',
            controller: 'ModalProcessoNovaAnotacaoController',
            size: sSize,
            backdrop: false,
            keyboard: false,
            resolve: {
                oProcesso: function () {
                    return $scope.oProcesso;
                }
            }
        });
        modalInstance.result.then(function (oProcesso) {
            //Salva Anotação
            try {
                var oProcesso = $scope.oProcesso;
                oProcesso.IdProcessoStatus = 4; //Em Execução
                oProcesso.Ativo = true;
                oProcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oProcesso.DataCriacao = new Date();

                ProcessoService.NovaAnotacao(oProcesso).then(function successCallback(response) {
                    //Pega o Retorno
                    oProcesso = response.data.Retorno;

                    //Atualiza Scope
                    $scope.oProcesso = oProcesso;
                    
                    //Retira Lodear;
                    $rootScope.loading = [false];
                }, function errorCallback(response) {
                    $scope.error = response.data.MsgRetorno;
                    $rootScope.loading = [false];
                });
            } catch (err) {
                $scope.error = "Não foi possivel inserir novo Processo!";
                $rootScope.loading = [false];
            }
        }, function () {
            //Cancela Anotação
            try {
                
            } catch (err) {
                $scope.error = "Não foi possivel inserir novo Processo!";
                $rootScope.loading = [false];
            }

            $scope.oProcesso = $scope.oProcesso;
        });
    };

    //MODAL RPA INFORMA (TODO DEIXAR GENERICO)
    $scope.showModalRPAInforma = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/Mensagem/ModalRPAInforma.html',
            size: sSize,
            backdrop: true,
            keyboard: true
        });       
    };
    $scope.showModalRPAInformaCancelado = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/Mensagem/ModalRPAInformaCancelado.html',
            size: sSize,
            backdrop: true,
            keyboard: true
        });
    };
    $scope.showModalRPAInformaErroExecucao = function (sSize) {
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'Views/Sistema/Processo/Mensagem/ModalRPAInformaErroExecucao.html',
            size: sSize,
            backdrop: true,
            keyboard: true
        });
    };
 
    //LIMPA SCOPE
    delete $scope.oProcesso;

    // INICIALIZACAO
    $scope.ListaHistoricoClassificacao();
    $scope.ListaInteracaoTipo();
    $scope.ListaProcessoStatus();
    $scope.ListaProcessoClassificacaoPrioridade();
    $scope.ListaScriptCodigoRetorno(0);
    
    //DIRECIONAMENTO
    if (sAcao === "Novo") {
        $scope.oProcesso = {};
        $scope.oProcesso.IdCliente = $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente;
        $scope.ListaCliente();
        $scope.ListaGrupo($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
        $scope.Scripts = null;
        $scope.init();
        $scope.$emit('atualizaTituloPagina', 'Processo - Novo');
        $scope.ViewMain = 'Views/Sistema/Processo/InsereProcesso.html';
    } else if (sAcao === "Consulta") {
        $scope.ListaCliente();
        $scope.ListaGrupo($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
        $scope.ListaScript(0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);
        $scope.ListaProcesso(0, 0, 0, 0, 0, $rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente, 0);
        $scope.$emit('atualizaTituloPagina', 'Processo - Consulta');
        $scope.ViewMain = 'Views/Sistema/Processo/ListaProcesso.html';
    };
    
});

