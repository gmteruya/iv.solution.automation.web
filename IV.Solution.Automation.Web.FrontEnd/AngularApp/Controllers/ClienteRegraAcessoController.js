﻿angular.module('automationApp').controller('ClienteRegraAcessoController', function ($scope, $rootScope, $timeout, ClienteRegraAcessoService, ClienteService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaClienteRegraAcesso = function () {
        try {
            ClienteRegraAcessoService.ListaClienteRegraAcesso().then(function successCallback(response) {
                $scope.ClienteRegrasAcesso = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ClienteRegraAcesso!");
        }
    }
    $scope.InsereClienteRegraAcesso = function () {
        try {
            var oClienteRegraAcesso = $scope.oClienteRegraAcesso;
            oClienteRegraAcesso.Ativo = true;
            oClienteRegraAcesso.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oClienteRegraAcesso.DataCriacao = new Date();
            ClienteRegraAcessoService.InsereClienteRegraAcesso(oClienteRegraAcesso).then(function successCallback(response) {
                $scope.ClienteRegrasAcesso.unshift(response.data.Retorno);
                delete $scope.oClienteRegraAcesso;
                $scope.$emit('atualizaTituloPagina', 'Regras de Acesso ao Assistente Virtual');
                $scope.ViewMain = 'Views/Sistema/ClienteRegraAcesso/ListaClienteRegraAcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo ClienteRegraAcesso!");
        }
    }
    $scope.AtualizaClienteRegraAcesso = function () {
        try {
            var IdClienteRegraAcesso = $scope.oClienteRegraAcesso.Id;
            var oClienteRegraAcesso = $scope.oClienteRegraAcesso;
            oClienteRegraAcesso.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oClienteRegraAcesso.DataAlteracao = new Date();
            ClienteRegraAcessoService.AtualizaClienteRegraAcesso(IdClienteRegraAcesso, oClienteRegraAcesso).then(function successCallback(response) {
                $scope.ClienteRegrasAcesso = response.data.Retorno;
                $scope.oClienteRegraAcesso = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar ClienteRegraAcesso!");
        }
    }
    
    // FUNCOES AUX
    $scope.ListaCliente = function () {
        try {
            if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente > 0) {
                ClienteService.RetornaCliente($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente).then(function successCallback(response) {
                    $scope.Clientes = [];
                    $scope.Clientes.unshift(response.data.Retorno);
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ClienteService.ListaCliente().then(function successCallback(response) {
                    $scope.Clientes = {};
                    $scope.Clientes = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de clientes!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $rootScope.loading = [true, 'Carregando'];
        $scope.oClienteRegraAcesso = {};
        $scope.$emit('atualizaTituloPagina', 'Inserindo uma nova regra de acesso do assistente virtual.');
        $scope.ViewMain = 'Views/Sistema/ClienteRegraAcesso/InsereClienteRegraAcesso.html';
        $rootScope.loading = [false];
    }
    $scope.btnEditar = function () {
        var nIdClienteRegraAcesso = this.oClienteRegraAcesso.Id;
        try {
            ClienteRegraAcessoService.RetornaClienteRegraAcesso(nIdClienteRegraAcesso).then(function successCallback(response) {
                $scope.oClienteRegraAcesso = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/ClienteRegraAcesso/DetalheClienteRegraAcesso.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do ClienteRegraAcesso!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oClienteRegraAcesso;
        $scope.$emit('atualizaTituloPagina', 'Regras de Acesso ao Assistente Virtual');
        $scope.ViewMain = 'Views/Sistema/ClienteRegraAcesso/ListaClienteRegraAcesso.html';
    }
  
    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Regras de Acesso ao Assistente Virtual');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ClienteRegraAcesso/ListaClienteRegraAcesso.html';
    $scope.ListaCliente();
    $scope.ListaClienteRegraAcesso();

});

