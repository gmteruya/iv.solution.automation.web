﻿angular.module('automationApp').controller('ClienteController', function ($scope, $rootScope, ivhTreeviewMgr, ClienteService, MenuService, ScriptService, GrupoService) {
    
    //VARIAVEIS

    //COMP
    var customOpts = {
        idAttribute: 'Id',
        labelAttribute: 'Descricao',
        childrenAttribute: 'MenuFilho',
        selectedAttribute: 'selected',
        useCheckboxes: true,
        expandToDepth: -1,
        indeterminateAttribute: '__ivhTreeviewIndeterminate',
        expandedAttribute: '__ivhTreeviewExpanded',
        defaultSelectedState: false,
        validate: true,
        twistieExpandedTpl: '(-)',
        twistieCollapsedTpl: '(+)',
        twistieLeafTpl: 'o',
        nodeTpl: [
          '<div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">',
            '<span ivh-treeview-toggle>',
              '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>',
            '</span>',
            '<span class="ivh-treeview-checkbox-wrapper" ng-if="trvw.shouldUseCheckboxes()"',
                'ivh-treeview-checkbox>',
            '</span>',
            '<span class="ivh-treeview-node-label" ivh-treeview-toggle>',
              '{{trvw.label(node)}}',
            '</span>',
            '<div ivh-treeview-children></div>',
          '</div>'
        ].join('\n')
    }
    $scope.customOpts = customOpts;
    ivhTreeviewMgr.userOptions = customOpts;
    
    //FUNCOES
    $scope.ListaCliente = function () {
        try {
            ClienteService.ListaCliente().then(function successCallback(response) {
                $scope.Clientes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de cliente!");
        }
    }
    $scope.InsereCliente = function () {
        try {
            var oCliente = $scope.oCliente;
            var MenusSelecionados = ivhTreeviewMgr.select($scope.Menus, null, null, true);
            oCliente.Ativo = true;
            oCliente.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oCliente.DataCriacao = new Date();
            ClienteService.InsereCliente(oCliente).then(function successCallback(response) {
                $scope.Clientes.unshift(response.data.Retorno);
                delete $scope.oCliente;
                $scope.$emit('atualizaTituloPagina', 'Clientes');
                $scope.ViewMain = 'Views/Sistema/Cliente/ListaCliente.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo cliente!");
        }
    }
    $scope.AtualizaCliente = function () {
        try {
            var IdCliente = $scope.oCliente.Id;
            var oCliente = $scope.oCliente;
            oCliente.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oCliente.DataAlteracao = new Date();
            ClienteService.AtualizaCliente(IdCliente, oCliente).then(function successCallback(response) {
                $scope.Clientes = response.data.Retorno;
                $scope.oCliente = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar cliente!");
        }
    }
    
    // FUNCOES AUX
    $scope.ListaScriptStatus = function () {
        try {
            ScriptService.ListaScriptStatus().then(function successCallback(response) {
                $scope.ListaStatusScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de script!");
        }
    }
    $scope.ListaScriptPrioridade = function () {
        try {
            ScriptService.ListaScriptPrioridade().then(function successCallback(response) {
                $scope.ListaPrioridadeScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de prioridades de script!");
        }
    }
    $scope.ListaScript = function (nIdScriptStatus, nIdScriptPrioridade) {
        try {
            ScriptService.ListaScript(nIdScriptStatus, nIdScriptPrioridade).then(function successCallback(response) {
                $scope.Scripts = [];
                var Scripts = response.data.Retorno;
                for (var i = 0; i < Scripts.length; i++) {
                    var Script = Scripts[i];
                    GrupoService.ListaGrupoPorScriptRelacionado(Script.Id).then(function successCallback(response) {
                        Script.Grupo = response.data.Retorno;
                        $scope.Scripts.push(Script);
                    }, function errorCallback(response) {
                        $rootScope.TrataMensagemResponseRPA(response, false);
                    });
                }
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de script!");
        }
    }
    $scope.ListaMenu = function () {
        try {
            MenuService.ListaMenu(0, 0).then(function successCallback(response) {
                $scope.Menus = response.data.Retorno
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de menus!");
        }
    }
   
    // EVENTOS
    $scope.btnNovo = function () {
        //Nova Intancia
        $scope.oCliente = {};
        $scope.ViewMain = 'Views/Sistema/Cliente/InsereCliente.html';
    }
    $scope.btnEditar = function () {
        var nIdCliente = this.oCliente.Id;
        try {
            ClienteService.RetornaCliente(nIdCliente).then(function successCallback(response) {
                $scope.oCliente = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/Cliente/DetalheCliente.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do cliente!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oCliente;
        $scope.$emit('atualizaTituloPagina', 'Clientes');
        $scope.ViewMain = 'Views/Sistema/Cliente/ListaCliente.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Clientes');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Cliente/ListaCliente.html';

    //Carrega
    $scope.ListaMenu();
    $scope.ListaScriptStatus();
    $scope.ListaScriptPrioridade();
    $scope.ListaScript(0, 0);
    $scope.ListaCliente();

});

