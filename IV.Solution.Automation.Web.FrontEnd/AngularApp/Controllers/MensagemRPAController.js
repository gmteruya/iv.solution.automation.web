﻿angular.module('automationApp').controller('MensagemRPAController', function ($scope, $rootScope, $uibModalInstance) {

    //Remove uma mensagem da lista
    $scope.RemoveMensagem = function (index) {
        $rootScope.arMensagens.splice(index, 1);
    };

    //Fecha Modal
    $scope.btnFecharMensagens = function () {
        //Limpa todas as mensagens
        $rootScope.arMensagens = [];
        $uibModalInstance.dismiss('cancel');
    };

});
