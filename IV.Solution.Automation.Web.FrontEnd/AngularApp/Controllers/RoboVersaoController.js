﻿angular.module('automationApp').controller('RoboVersaoController', function ($scope, $rootScope, RoboVersaoService, RoboService, ServicoRoboService) {
    
    // FUNCOES CRUD
    $scope.ListaRoboVersao = function (nIdRoboVersaoStatus) {
        try {
            RoboVersaoService.ListaRoboVersao(nIdRoboVersaoStatus).then(function successCallback(response) {
                $scope.RoboVersoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de versão do robo!");
        }
    }
    $scope.InsereRoboVersao = function () {
        try {
            var oRoboVersao = $scope.oRoboVersao;
            oRoboVersao.Versao = oRoboVersao.Faixa1 + '.' + oRoboVersao.Faixa2 + '.' + oRoboVersao.Faixa3 + '.' + oRoboVersao.Faixa4
            oRoboVersao.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oRoboVersao.DataCriacao = new Date();
            oRoboVersao.Arquivo = $rootScope.Arquivos; //$scope.$broadcast('RetornaListaArquivos');
            RoboVersaoService.InsereRoboVersao(oRoboVersao).then(function successCallback(response) {
                $scope.RoboVersoes.unshift(response.data.Retorno);
                delete $scope.oRoboVersao;
                $scope.$emit('atualizaTituloPagina', 'Versões do Robô');
                $scope.ViewMain = 'Views/Sistema/RoboVersao/ListaRoboVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo versão do robo!");
        }
    }
    $scope.AtualizaRoboVersao = function () {
        try {
            var oRoboVersao = $scope.oRoboVersao;
            oRoboVersao.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oRoboVersao.DataAlteracao = new Date();
            RoboVersaoService.AtualizaRoboVersao(oRoboVersao.Id, oRoboVersao).then(function successCallback(response) {
                delete $scope.oRoboVersao;
                $scope.ListaRoboVersao(0);
                $scope.$emit('atualizaTituloPagina', 'Versões do Robô');
                $scope.ViewMain = 'Views/Sistema/RoboVersao/ListaRoboVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar versão do robo!");
        }
    }
    
    // FUNCOES AUX
    $scope.ListaRoboVersaoStatus = function () {
        try {
            RoboVersaoService.ListaRoboVersaoStatus().then(function successCallback(response) {
                $scope.ListaStatusVersaoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status da versão do aplicativo!");
        }
    }
    $scope.ListaRoboEstado = function () {
        try {
            RoboService.ListaRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de robo!");
        }
    }
    $scope.ListaRoboStatus = function () {
        try {
            RoboService.ListaRoboStatus().then(function successCallback(response) {
                $scope.ListaStatusRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de Robo!");
        }
    }
    $scope.ListaRobo = function (nIdRoboEstado, nIdRoboStatus, nIdRoboVersao, sDominioRede) {
        try {
            RoboService.ListaRobo(nIdRoboEstado, nIdRoboStatus, nIdRoboVersao, sDominioRede).then(function successCallback(response) {
                $scope.Robos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de Robo!");
        }
    }
    $scope.ListaServicoRoboEstado = function () {
        try {
            ServicoRoboService.ListaServicoRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoServicoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de estado do serviço de robô!");
        }
    }
    $scope.ListaServicoRobo = function (nIdServicoRoboEstado, nIdServicoRoboVersao) {
        try {
            ServicoRoboService.ListaServicoRobo(nIdServicoRoboEstado, nIdServicoRoboVersao).then(function successCallback(response) {
                $scope.ServicosRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de serviços de robõ!");
        }
    }
    
    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oRoboVersao = {};
        $scope.oRoboVersao.Faixa1 = 1;
        $scope.oRoboVersao.Faixa2 = 0;
        $scope.oRoboVersao.Faixa3 = 0;
        $scope.oRoboVersao.Faixa4 = 0;
        delete $rootScope.Arquivos;
        $rootScope.ControleSomenteLeitura = false;
        $scope.$emit('atualizaTituloPagina', 'Inserindo uma nova versão de robô.');
        $scope.ViewMain = 'Views/Sistema/RoboVersao/InsereRoboVersao.html';
    }
    $scope.btnEditar = function () {
        var nIdRoboVersao = this.oRoboVersao.Id;
        try {
            RoboVersaoService.RetornaRoboVersao(nIdRoboVersao, false, true, true, true).then(function successCallback(response) {
                $scope.oRoboVersao = response.data.Retorno;
                delete $rootScope.Arquivos;
                $rootScope.Arquivos = $scope.oRoboVersao.Arquivo;
                $rootScope.ControleSomenteLeitura = true;
                var arFaixa = $scope.oRoboVersao.Versao.split('.');
                $scope.oRoboVersao.Faixa1 = parseInt(arFaixa[0]);
                $scope.oRoboVersao.Faixa2 = parseInt(arFaixa[1]);
                $scope.oRoboVersao.Faixa3 = parseInt(arFaixa[2]);
                $scope.oRoboVersao.Faixa4 = parseInt(arFaixa[3]);
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Versão do Robô');
                $scope.ViewMain = 'Views/Sistema/RoboVersao/DetalheRoboVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do versão do robo!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oRoboVersao;
        $scope.$emit('atualizaTituloPagina', 'Versões do Robô');
        $scope.ViewMain = 'Views/Sistema/RoboVersao/ListaRoboVersao.html';
    }
    $scope.btnReiniciaRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.ReiniciaRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar os robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel reiniciar os robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnLigaRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.LigaRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar os robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel ligar os robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnDesligaRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.DesligaRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar os robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel desligar os robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnIniciarMonitoramentoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de inicializar o monitoramento dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.IniciarMonitoramentoRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento dos robõs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento de nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inicializar o monitoramento dos robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnPararMonitoramentoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de parar o monitoramento dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                try {
                    RoboService.PararMonitoramentoRobo(lstRobosSelecionados).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0);
                        $scope.ListaRobo(0, 0, 0, '');
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento dos robôs selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento de nenhum robô selecionado!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robô selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel parar o monitoramento dos robôs selecionados!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnTrocaVersaoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstRobosSelecionados = $scope.Robos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstRobosSelecionados.length > 0) {
                var oRoboVersao = $scope.oRoboVersao;
                oRoboVersao.Robo = lstRobosSelecionados;
                oRoboVersao.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oRoboVersao.DataAlteracao = new Date();
                try {
                    RoboVersaoService.TrocaVersaoRobo(oRoboVersao).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oRoboVersao = $scope.oRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaRobo(0, 0, 0, '');
                        $scope.ListaServicoRobo(0, 0);
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel trocar a versão dos robos selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel trocar a versão dos robos selecionados!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, true, false, true, "Nenhum robo selecionado!");
                $rootScope.loading = [false];
            }           
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo versão do robo!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnAtualizaListaRobo = function () {
        try {
            $scope.ListaRobo(0, 0, 0, '');
            $scope.ListaServicoRobo(0, 0);
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar a lista de robôs!");
        }
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Versões do Robô');
    
    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/RoboVersao/ListaRoboVersao.html';
    $scope.ListaRoboVersaoStatus();
    $scope.ListaRoboEstado();
    $scope.ListaRoboStatus();
    $scope.ListaRobo(0, 0, 0, '');
    $scope.ListaServicoRoboEstado();
    $scope.ListaServicoRobo(0, 0);
    $scope.ListaRoboVersao(0);

});

