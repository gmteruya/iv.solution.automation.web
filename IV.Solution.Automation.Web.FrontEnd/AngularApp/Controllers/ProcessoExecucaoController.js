﻿angular.module('automationApp').controller('ProcessoExecucaoController', function ($scope, $rootScope, $uibModalInstance, ProcessoExecucaoService, ScriptService, oProcessoExecucao, ListaStatusProcesso, LogAplicacaoService, ScriptCodigoRetornoService, ScriptStepService, ScriptSistemaService, moment, UtilService) {
    $scope.oProcesso = oProcessoExecucao.Processo;
    $scope.ListaStatusProcesso = ListaStatusProcesso;

    //FUNCOES CRUD
    $scope.RetornaProcessoExecucao = function (nIdProcessoExecucao, bCarregaProcesso, bCarregaRobo, bCarregaArquivo, bCarregaArquivoTipo, bCarregaArquivoTipoGrupo, bCarregaProcessoExecucaoScriptStep, bCarregaScriptStep, bCarregaHistoricoOcorrencia, bCarregaInteracao) {
        try {
            ProcessoExecucaoService.RetornaProcessoExecucao(nIdProcessoExecucao, bCarregaProcesso, bCarregaRobo, bCarregaArquivo, bCarregaArquivoTipo, bCarregaArquivoTipoGrupo, bCarregaProcessoExecucaoScriptStep, bCarregaScriptStep, bCarregaHistoricoOcorrencia, bCarregaInteracao).then(function successCallback(response) {
                $scope.oProcessoExecucao = response.data.Retorno;
                $scope.oProcessoExecucao.Processo = $scope.oProcesso;
                $scope.CalculaTotalTempoSegundosAssistente();
                $scope.CalculaTotalTempoSegundosRobo();
                delete $rootScope.Arquivos;
                $rootScope.Arquivos = $scope.oProcessoExecucao.Arquivo;
                $rootScope.ControleSomenteLeitura = true;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de processo!");
        }
    }

    // FUNCOES AUX
    $scope.ListaLogAplicacao = function (nIdCliente, nIdScript, nIdGrupo, nIdRobo, nIdProcesso, nIdProcessoExecucao, nIdProcessoExecucaoScriptStep) {
        try {
            LogAplicacaoService.ListaLogAplicacao(nIdCliente, nIdScript, nIdGrupo, nIdRobo, nIdProcesso, nIdProcessoExecucao, nIdProcessoExecucaoScriptStep).then(function successCallback(response) {
                $scope.oLogAplicacaoExecucao = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel os logs da execução!");
        }
    }
    $scope.ListaScriptCodigoRetorno = function (nIdScript) {
        try {
            ScriptCodigoRetornoService.ListaScriptCodigoRetorno(nIdScript).then(function successCallback(response) {
                $scope.CodigosRetornos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de codigos de retorno!");
        }
    }
    $scope.ListaScriptStepTipo = function () {
        try {
            ScriptStepService.ListaScriptStepTipo().then(function successCallback(response) {
                $scope.TipoStepScript = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de tipos de step!");
        }
    }
    $scope.ListaScriptSistema = function (nIdCliente) {
        try {
            if (nIdCliente > 0) {
                ScriptSistemaService.ListaScriptSistemaPorClienteRelacionado(nIdCliente).then(function successCallback(response) {
                    $scope.ScriptSistemas = {};
                    $scope.ScriptSistemas = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            } else {
                ScriptSistemaService.ListaScriptSistema().then(function successCallback(response) {
                    $scope.ScriptSistemas = {};
                    $scope.ScriptSistemas = response.data.Retorno;
                }, function errorCallback(response) {
                    $rootScope.TrataMensagemResponseRPA(response, false);
                });
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de perfis de acesso!");
        }
    }
    $scope.CalculaTotalTempoSegundosAssistente = function () {
        var nTempoSegundosAssistente = 0;
        var arProcessoExecucaoScriptStep;
        arProcessoExecucaoScriptStep = $scope.oProcessoExecucao.ProcessoExecucaoScriptStep;
        for (var i = 0; i < arProcessoExecucaoScriptStep.length; i++) {
            var oProcessoExecucaoScriptStep = arProcessoExecucaoScriptStep[i];
            if (oProcessoExecucaoScriptStep.ScriptStep.IdScriptStepTipo == 1) {
                nTempoSegundosAssistente += UtilService.CalculaTempo.DiferencaMilesegundos(oProcessoExecucaoScriptStep.DataInicio, oProcessoExecucaoScriptStep.DataFim);
            }
        }
        $scope.TempoTotalSegundosAssistente = UtilService.CalculaTempo.ValorRetorno(nTempoSegundosAssistente);
    }
    $scope.CalculaTotalTempoSegundosRobo = function () {
        var nTempoTotalSegundosRobo = 20;
        var arProcessoExecucaoScriptStep;
        arProcessoExecucaoScriptStep = $scope.oProcessoExecucao.ProcessoExecucaoScriptStep;
        for (var i = 0; i < arProcessoExecucaoScriptStep.length; i++) {
            var oProcessoExecucaoScriptStep = arProcessoExecucaoScriptStep[i];
            if (oProcessoExecucaoScriptStep.ScriptStep.IdScriptStepTipo > 1 && oProcessoExecucaoScriptStep.ScriptStep.IdScriptStepTipo != 6) {
                nTempoTotalSegundosRobo += UtilService.CalculaTempo.DiferencaMilesegundos(oProcessoExecucaoScriptStep.DataInicio, oProcessoExecucaoScriptStep.DataFim);
            }
        }
        $scope.TempoTotalSegundosRobo = UtilService.CalculaTempo.ValorRetorno(nTempoTotalSegundosRobo);
    }
    $scope.CalculaTempo = function (DataInicio, DataFim) {
        try {
            return UtilService.CalculaTempo.Diferenca(DataInicio, DataFim);
        } catch (e) {

        }
    };

    //EVENTOS
    $scope.btnAbrirMaisInformacoes = function () {
        $('#divMaisInformacoesExecucao').show(200);
    }
    $scope.btnFecharMaisInformacoes = function () {
        $('#divMaisInformacoesExecucao').hide(200);
    }
    $scope.btnFecharProcessoExecucao = function () {
        $uibModalInstance.dismiss('cancel');
    };

    // INICIALIZACAO
    $scope.ListaScriptCodigoRetorno(0);
    $scope.ListaScriptStepTipo(0);
    $scope.ListaScriptSistema($rootScope.globals.Authentication.UsuarioSessao.Usuario.IdCliente);

    //Carrega label da chave do processo no scope.
    var sLabelChaveProcesso = 'Chave do Processo';
    if ($rootScope.globals != undefined && $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente != undefined) {
        if ($rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'] != undefined) {
            sLabelChaveProcesso = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Cliente.PropriedadesAdicionais['LabelChaveProcesso'];
        }
    }
    $scope.sLabelChaveProcesso = sLabelChaveProcesso;

    //Detalhe da Execução
    $scope.RetornaProcessoExecucao(oProcessoExecucao.Id, false, true, true, true, true, true, true, true, true);

});

