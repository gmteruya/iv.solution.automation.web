﻿
angular.module('automationApp').controller('AutomationConfiguracaoController', function ($scope, $rootScope, AutomationConfiguracaoService) {

    //VARIAVEIS

    //FUNCOES
    $scope.ListaAutomationConfiguracao = function () {
        try {
            AutomationConfiguracaoService.ListaAutomationConfiguracao().then(function successCallback(response) {
                $scope.AutomationConfiguracoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de chaves de configuração!");
        }
    }
    $scope.InsereAutomationConfiguracao = function () {
        try {
            var oAutomationConfiguracao = $scope.oAutomationConfiguracao;
            oAutomationConfiguracao.Ativo = true;
            oAutomationConfiguracao.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oAutomationConfiguracao.DataCriacao = new Date();
            AutomationConfiguracaoService.InsereAutomationConfiguracao(oAutomationConfiguracao).then(function successCallback(response) {
                $scope.AutomationConfiguracoes.unshift(response.data.Retorno);
                delete $scope.oAutomationConfiguracao;
                $scope.$emit('atualizaTituloPagina', 'Parâmetros da Plataforma');
                $scope.ViewMain = 'Views/Sistema/AutomationConfiguracao/ListaAutomationConfiguracao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir nova chave de configuração!");
        }
    }
    $scope.AtualizaAutomationConfiguracao = function () {
        try {
            var IdAutomationConfiguracao = $scope.oAutomationConfiguracao.Id;
            var oAutomationConfiguracao = $scope.oAutomationConfiguracao;
            oAutomationConfiguracao.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oAutomationConfiguracao.DataAlteracao = new Date();
            AutomationConfiguracaoService.AtualizaAutomationConfiguracao(IdAutomationConfiguracao, oAutomationConfiguracao).then(function successCallback(response) {
                $scope.AutomationConfiguracoes = response.data.Retorno;
                $scope.oAutomationConfiguracao = '';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar chave de configuração!");
        }
    }
 
    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oAutomationConfiguracao = {};
        $scope.ViewMain = 'Views/Sistema/AutomationConfiguracao/InsereAutomationConfiguracao.html';
    }
    $scope.btnEditar = function () {
        var IdAutomationConfiguracao = this.oAutomationConfiguracao.Id;
        try {
            AutomationConfiguracaoService.RetornaAutomationConfiguracao(IdAutomationConfiguracao).then(function successCallback(response) {
                $scope.oAutomationConfiguracao = response.data.Retorno;
                $scope.ViewMain = 'Views/Sistema/AutomationConfiguracao/DetalheAutomationConfiguracao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados da chave de configuração!");
        }
    }    
    $scope.btnCancelar = function () {       
        delete $scope.oAutomationConfiguracao;
        $scope.$emit('atualizaTituloPagina', 'Parâmetros da Plataforma');
        $scope.ViewMain = 'Views/Sistema/AutomationConfiguracao/ListaAutomationConfiguracao.html';
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Parâmetros da Plataforma');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/AutomationConfiguracao/ListaAutomationConfiguracao.html';
    $scope.ListaAutomationConfiguracao();

});

