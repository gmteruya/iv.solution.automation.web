﻿angular.module('automationApp').controller('ServicoRoboVersaoController', function ($scope, $rootScope, ServicoRoboVersaoService, ServicoRoboService) {

    // FUNCOES CRUD
    $scope.ListaServicoRoboVersao = function (nIdServicoRoboVersaoStatus) {
        try {
            ServicoRoboVersaoService.ListaServicoRoboVersao(nIdServicoRoboVersaoStatus).then(function successCallback(response) {
                $scope.ServicoRoboVersoes = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de versão do ServicoRobo!");
        }
    }
    $scope.InsereServicoRoboVersao = function () {
        try {
            var oServicoRoboVersao = $scope.oServicoRoboVersao;
            oServicoRoboVersao.Versao = oServicoRoboVersao.Faixa1 + '.' + oServicoRoboVersao.Faixa2 + '.' + oServicoRoboVersao.Faixa3 + '.' + oServicoRoboVersao.Faixa4
            oServicoRoboVersao.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oServicoRoboVersao.DataCriacao = new Date();
            oServicoRoboVersao.Arquivo = $rootScope.Arquivos; //$scope.$broadcast('RetornaListaArquivos');
            ServicoRoboVersaoService.InsereServicoRoboVersao(oServicoRoboVersao).then(function successCallback(response) {
                $scope.ServicoRoboVersoes.unshift(response.data.Retorno);
                delete $scope.oServicoRoboVersao;
                $scope.$emit('atualizaTituloPagina', 'Versões do Robô');
                $scope.ViewMain = 'Views/Sistema/ServicoRoboVersao/ListaServicoRoboVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo versão do ServicoRobo!");
        }
    }
    $scope.AtualizaServicoRoboVersao = function () {
        try {
            var oServicoRoboVersao = $scope.oServicoRoboVersao;
            oServicoRoboVersao.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oServicoRoboVersao.DataAlteracao = new Date();
            ServicoRoboVersaoService.AtualizaServicoRoboVersao(oServicoRoboVersao.Id, oServicoRoboVersao).then(function successCallback(response) {
                delete $scope.oServicoRoboVersao;
                $scope.ListaServicoRoboVersao(0);
                $scope.$emit('atualizaTituloPagina', 'Versões do Robô');
                $scope.ViewMain = 'Views/Sistema/ServicoRoboVersao/ListaServicoRoboVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar versão do ServicoRobo!");
        }
    }

    // FUNCOES AUX
    $scope.ListaServicoRoboVersaoStatus = function () {
        try {
            ServicoRoboVersaoService.ListaServicoRoboVersaoStatus().then(function successCallback(response) {
                $scope.ListaStatusVersaoServicoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status da versão do aplicativo!");
        }
    }
    $scope.ListaServicoRoboEstado = function () {
        try {
            ServicoRoboService.ListaServicoRoboEstado().then(function successCallback(response) {
                $scope.ListaEstadoServicoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de status de ServicoRobo!");
        }
    }
    $scope.ListaServicoRobo = function (nIdServicoRoboEstado, nIdServicoRoboStatus, nIdServicoRoboVersao, sDominioRede) {
        try {
            ServicoRoboService.ListaServicoRobo(nIdServicoRoboEstado, nIdServicoRoboStatus, nIdServicoRoboVersao, sDominioRede).then(function successCallback(response) {
                $scope.ServicoRobos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de ServicoRobo!");
        }
    }
    $scope.ListaRobo = function (nIdServicoServicoRoboEstado, nIdServicoServicoRoboVersao) {
        try {
            ServicoServicoRoboService.ListaServicoServicoRobo(nIdServicoServicoRoboEstado, nIdServicoServicoRoboVersao).then(function successCallback(response) {
                $scope.ServicosServicoRobo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de serviços de robõ!");
        }
    }

    // EVENTOS
    $scope.btnNovo = function () {
        $scope.oServicoRoboVersao = {};
        $scope.oServicoRoboVersao.Faixa1 = 1;
        $scope.oServicoRoboVersao.Faixa2 = 0;
        $scope.oServicoRoboVersao.Faixa3 = 0;
        $scope.oServicoRoboVersao.Faixa4 = 0;
        delete $rootScope.Arquivos;
        $rootScope.ControleSomenteLeitura = false;
        $scope.$emit('atualizaTituloPagina', 'Inserindo uma nova versão de robô.');
        $scope.ViewMain = 'Views/Sistema/ServicoRoboVersao/InsereServicoRoboVersao.html';
    }
    $scope.btnEditar = function () {
        var nIdServicoRoboVersao = this.oServicoRoboVersao.Id;
        try {
            ServicoRoboVersaoService.RetornaServicoRoboVersao(nIdServicoRoboVersao, false, true, true, true).then(function successCallback(response) {
                $scope.oServicoRoboVersao = response.data.Retorno;
                delete $rootScope.Arquivos;
                $rootScope.Arquivos = $scope.oServicoRoboVersao.Arquivo;
                $rootScope.ControleSomenteLeitura = true;
                var arFaixa = $scope.oServicoRoboVersao.Versao.split('.');
                $scope.oServicoRoboVersao.Faixa1 = parseInt(arFaixa[0]);
                $scope.oServicoRoboVersao.Faixa2 = parseInt(arFaixa[1]);
                $scope.oServicoRoboVersao.Faixa3 = parseInt(arFaixa[2]);
                $scope.oServicoRoboVersao.Faixa4 = parseInt(arFaixa[3]);
                $scope.$emit('atualizaTituloPagina', 'Detalhe do Versão do Robô');
                $scope.ViewMain = 'Views/Sistema/ServicoRoboVersao/DetalheServicoRoboVersao.html';
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do versão do ServicoRobo!");
        }
    }
    $scope.btnCancelarVoltar = function () {
        delete $scope.oServicoRoboVersao;
        $scope.$emit('atualizaTituloPagina', 'Versões do Robô');
        $scope.ViewMain = 'Views/Sistema/ServicoRoboVersao/ListaServicoRoboVersao.html';
    }
    $scope.btnTrocaVersaoServicoRobo = function () {
        $rootScope.loading = [true, 'Aguarde, estamos executando o procedimento de troca de versão dos robôs selecionados!'];
        try {
            var lstServicoRobosSelecionados = $scope.ServicoRobos.filter(function (el) {
                el.Ativo = true;
                return (el.isSelected === true);
            });
            if (lstServicoRobosSelecionados.length > 0) {
                var oServicoRoboVersao = $scope.oServicoRoboVersao;
                oServicoRoboVersao.ServicoRobo = lstServicoRobosSelecionados;
                oServicoRoboVersao.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                oServicoRoboVersao.DataAlteracao = new Date();
                try {
                    ServicoRoboVersaoService.TrocaVersaoServicoRobo(oServicoRoboVersao).then(function (response) {
                        var oMensagem = {};
                        var arMensagem = [];
                        if (response.data.Sucesso === false) {
                            $scope.oServicoRoboVersao = $scope.oServicoRoboVersao;
                            oMensagem = { Tipo: 'text-danger', Mensagem: response.data.MsgRetorno };
                            arMensagem.push(oMensagem);
                        } else {
                            if (response.data.MsgRetorno !== undefined && response.data.MsgRetorno !== null && response.data.MsgRetorno !== '') {
                                var arMensagenRetorno = response.data.MsgRetorno.split('§');
                                if (arMensagenRetorno.length > 0) {
                                    var oMensagemRetorno;
                                    for (var i = 0; i < arMensagenRetorno.length; i++) {
                                        oMensagemRetorno = arMensagenRetorno[i];
                                        if (oMensagemRetorno !== undefined && oMensagemRetorno !== null && oMensagemRetorno !== '') {
                                            arMensagemRetorno = oMensagemRetorno.split('&');
                                            if (arMensagemRetorno.length = 2) {
                                                oMensagem = { Tipo: arMensagemRetorno[0], Mensagem: arMensagemRetorno[1] };
                                                arMensagem.push(oMensagem);
                                            };
                                        };
                                    };
                                };
                            };
                        };
                        if (arMensagem.length > 0) {
                            for (var i = 0; i < arMensagem.length; i++) {
                                $rootScope.arMensagens.push(arMensagem[i]);
                            };
                            $rootScope.ShowModalMensagemRPA('lg');
                        };
                        $scope.ListaServicoRobo(0, 0, 0, '');
                        $scope.ListaServicoServicoRobo(0, 0);
                        $rootScope.loading = [false];
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel trocar a versão dos ServicoRobos selecionados!");
                        $rootScope.loading = [false];
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel trocar a versão dos ServicoRobos selecionados!");
                    $rootScope.loading = [false];
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Nenhum ServicoRobo selecionado!");
                $rootScope.loading = [false];
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo versão do ServicoRobo!");
            $rootScope.loading = [false];
        }
    }
    $scope.btnAtualizaListaServicoRobo = function () {
        try {
            $scope.ListaServicoRobo(0, 0, 0, '');
            $scope.ListaServicoServicoRobo(0, 0);
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel atualizar a lista de robôs!");
        }
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Versões do Robô');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/ServicoRoboVersao/ListaServicoRoboVersao.html';
    $scope.ListaServicoRoboVersaoStatus();
    $scope.ListaServicoRoboEstado();
    $scope.ListaServicoRobo(0, 0, 0, '');
});

