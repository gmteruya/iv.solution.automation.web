﻿angular.module('automationApp').controller('ArquivoController', function ($scope, $rootScope, $timeout, ArquivoService) {

    //TRATA PARAMETROS INICIAIS DA CONTROLLER (GERALMENTE SETADO PARA QUEM CONSOME O COMPONENTE)
    if ($rootScope.ControleSomenteLeitura === undefined || $rootScope.ControleSomenteLeitura === null) {
        $rootScope.ControleSomenteLeitura = false;
    }

    //FUNCOES CRUD
    $scope.InsereArquivo = function (oArquivo) {
        try {
            oArquivo.AutorCriacao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
            oArquivo.DataCriacao = new Date();
            ArquivoService.InsereArquivo(oArquivo).then(function successCallback(response) {
                $rootScope.Arquivos.unshift(response.data.Retorno);
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel inserir novo usuário!");
        }
    }
   
    // FUNCOES AUX
    $scope.ListaArquivoTipoGrupo = function () {
        $scope.GruposTipoArquivo = {};
        try {
            ArquivoService.ListaArquivoTipoGrupo().then(function successCallback(response) {
                $scope.GruposTipoArquivo = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de grupos de tipos de arquivos!");
        }
    }
    $scope.ListaArquivoTipo = function (nIdArquivoTipoGrupo) {
        $scope.TiposArquivos = {};
        try {
            ArquivoService.ListaArquivoTipo(nIdArquivoTipoGrupo).then(function successCallback(response) {
                $scope.TiposArquivos = response.data.Retorno;
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de tipo de arquivo!");
        }
    }
    $scope.SetFilesUpload = function (element) {
        //Trata Inicialização do array de files
        if ($rootScope.fileList === undefined || $rootScope.fileList === null) {
            $rootScope.fileList = [];
        }
        var files = element.files;
        for (var i = 0; i < files.length; i++) {
            $rootScope.fileList.push(files[i]);
        }
    };
    $scope.DownloadFile = function (oArquivo) {
        try {
            var filename = oArquivo.Nome;
            var contentType = oArquivo.ArquivoTipo.MimeType;
            var blob = $scope.ConvertBase64ToBlob(oArquivo.Arquivo, contentType)
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob, filename);
            } else {
                var url = window.URL.createObjectURL(blob);
                var clickEvent;
                if (document.createEvent) {
                    clickEvent = document.createEvent("MouseEvent");
                    clickEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                }
                else {
                    clickEvent = new MouseEvent("click", { "view": window, "bubbles": true, "cancelable": false });
                }
                var linkElement = document.createElement('a');
                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", filename);
                linkElement.dispatchEvent(clickEvent);
            }
        } catch (ex) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel disponibilizar o arquivo para download!");
        }
    };
    $scope.UploadFile = function (fileToUpload) {
        var reader = new FileReader();
        reader.onload = function () {

            //File in Binary
            var fileBinary;
            if (reader.result !== null && reader.result !== undefined) {
                fileBinary = reader.result;
            } else {
                fileBinary = reader.content;
            }
                       
            //Insere Arquivo
            oArquivo = { };
            oArquivo.Nome = fileToUpload.name;
            oArquivo.Tamanho = fileToUpload.size;
            oArquivo.Arquivo = btoa(fileBinary); //Base64
            $scope.InsereArquivo(oArquivo);

        };
        reader.onerror = function () {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel realizar upload do arquivo!");
        };
        //Implentação crossbrowser
        if (FileReader.prototype.readAsBinaryString === undefined) {
            FileReader.prototype.readAsBinaryString = function (fileToUpload) {
                var binary = '';
                var pt = this;
                var reader = new FileReader();
                reader.onload = function (e) {
                    var bytes = new Uint8Array(reader.result);
                    var length = bytes.byteLength;
                    for (var i = 0; i < length; i++) {
                        binary += String.fromCharCode(bytes[i]);
                    }
                    //pt.result  - readonly so assign content to another property
                    pt.content = binary;
                    pt.onload();
                }
                reader.readAsArrayBuffer(fileToUpload);
            }
        }
        reader.readAsBinaryString(fileToUpload);
    };
    $scope.ConvertBase64ToBlob = function (content, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(content);
        var byteArrays = [
        ];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };

    //EVENTOS
    $scope.btnUpload = function () {
        //Trata Inicialização do array de arquivos
        if ($rootScope.Arquivos === undefined || $rootScope.Arquivos === null) {
            $rootScope.Arquivos = [];
        }
        for (var i = 0; i < $rootScope.fileList.length; i++) {
            $scope.UploadFile($rootScope.fileList[i]);
        }
    };
    $scope.btnDownload = function () {
        try {
            var nIdArquivo = this.oArquivo.Id;
            ArquivoService.RetornaArquivo(nIdArquivo, true, true, true).then(function successCallback(response) {
                $scope.oArquivo = response.data.Retorno;
                $scope.DownloadFile($scope.oArquivo);
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar o arquivo para download!");
        }  
    };
    $scope.btnExclui = function () {
        try {
            var nIdArquivo = this.oArquivo.Id;
            ArquivoService.RetornaArquivo(nIdArquivo, false, false, false).then(function successCallback(response) {
                $scope.oArquivo = response.data.Retorno;
                $scope.oArquivo.Ativo = false;
                $scope.oArquivo.AutorAlteracao = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                $scope.oArquivo.DataAlteracao = new Date();
            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar o arquivo para exclusão!");
        }
    };
    $scope.btnExcluiSelecionados = function () {
        try {
            var lstArquivosSelecionados = $rootScope.Arquivos.filter(function (el) {
                return (el.isSelected === true);
            });
            if (lstArquivosSelecionados.length > 0) {
                var sAutor = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                try {
                    ArquivoService.ExcluiSelecionados(sAutor, lstArquivosSelecionados).then(function (response) {
                        lstArquivosSelecionados = response.data.Retorno;
                        $rootScope.Arquivos = $rootScope.Arquivos.filter(function (el) {
                            return (el.isSelected !== true);
                        });
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel excluir os arquivos selecionados!");
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel excluir os arquivos selecionados!");
                }
            } else {
                $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Nenhum arquivo selecionado!");
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel excluir os arquivos selecionados!");
        }  
    };
    $scope.btnDownloadSelecionados = function () {
        delete $scope.oArquivoZipado;
        try {
            var lstArquivosSelecionados = $rootScope.Arquivos.filter(function (el) {
                return (el.isSelected === true);
            });
            if (lstArquivosSelecionados.length > 0) {
                var sAutor = $rootScope.globals.Authentication.UsuarioSessao.Usuario.Login;
                try {
                    ArquivoService.DownloadSelecionados(sAutor, lstArquivosSelecionados).then(function (response) {
                        $scope.oArquivoZipado = response.data.Retorno;
                        $scope.DownloadFile($scope.oArquivoZipado);
                    }, function (error) {
                        $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel excluir os arquivos selecionados!");
                    });
                } catch (err) {
                    $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel excluir os arquivos selecionados!");
                }
            }
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel compactar arquivosselecionados!");
        }
    };

    // INICIALIZACAO
    $scope.ListaArquivoTipoGrupo();
    $scope.ListaArquivoTipo(0);
    
    //Dropzone
    $scope.bHabilitaDragAndDrop = true; /* variavel de validação do dragAndDrop, true = ativo, false = inativo */
    $scope.nTamanhoMaximoDeArquivo = 20; /* variavel de validação do dragAndDrop, true = ativo, false = inativo */
    $scope.lstArquivoTipo = '.pdf,.doc,.jpg,.rar,.zip,.dll,.exe'; /* lista de Extenções Permitidas vinda do Server */
    $scope.CarregaDragAndDrop = function () {
        var ValueToSubmit = ''; //parent.document.getElementById("hdValueToSubmit");
        var intIDProjeto = ''; //hdfDocumentoBase.Get("intIDProjeto");
        var intIDUsuario = ''; //hdfDocumentoBase.Get("intIDUsuario");
        var intIDRequisicao = ''; //hdfDocumentoBase.Get("intIDRequisicao");
        var intIDEmail = ''; //hdfDocumentoBase.Get("intIDEmail");
        var intIDPaginaSessaoUsuario = ''; //hdfDocumentoBase.Get("intIDPaginaSessaoUsuario");
        var intIDDocumento = ''; //hdfDocumentoBase.Get("intIDDocumento");
        if (BrowserIe7 == false && BrowserIe8 == false && BrowserIe9 == false) {
            Dropzone.autoDiscover = false;
            if ($scope.bHabilitaDragAndDrop == true && $scope.lstArquivoTipo != "") {
                $("#dZUpload").dropzone({
                    url: "UploadDragAndDrop.ashx?&id_projeto=" + intIDProjeto + "&id_usuario=" + intIDUsuario + "&id_requisicao=" + intIDRequisicao + "&id_email=" + intIDEmail + "&id_pagina_sessao_usuario=" + intIDPaginaSessaoUsuario + "&id_documento=" + intIDDocumento + "&id_files=" + false,
                    addRemoveLinks: true,
                    maxFilesize: $scope.nTamanhoMaximoDeArquivo,
                    acceptedFiles: $scope.lstArquivoTipo,
                    success: function (file, response) {
                        var imgName = response;
                        file.previewElement.classList.add("dz-success");
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                    }
                });
                $('.DragAndDropInitlayout').fadeOut(300, function () { $('.DragAndDroplayout_small').fadeIn(); });
            }
            else {
                $('.DragAndDropInitlayout, .DragAndDroplayout_small, .Amarra_dropzone_animacao').remove();
            }
        };
    };
    //$timeout(function () {
    //    $scope.CarregaDragAndDrop();
    //},1000);
    
});


