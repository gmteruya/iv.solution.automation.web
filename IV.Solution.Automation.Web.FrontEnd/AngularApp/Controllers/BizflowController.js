﻿angular.module('automationApp').controller('BizflowController', function ($scope, $rootScope, $filter, $uibModal, BizflowService) {

    //VARIAVEIS
    $scope.Data = new Date();
    $scope.DataMin = new Date();
    $scope.DataMinSelected = $scope.DataMin.setDate($scope.DataMin.getDate() - 14);
    $scope.DataHoje = new Date($scope.Data.getFullYear(), $scope.Data.getMonth(), $scope.Data.getDate());
    $scope.oParametro = { "Status": "0", "ChaveProcesso": "", "DataCriacaoInicial": $scope.DataHoje, "DataCriacaoFinal": $scope.DataHoje };

    //CAMPOS DE DATA
    $scope.clear = function () {
        $scope.oParametro.DataCriacaoInicial = null;
        $scope.oParametro.DataCriacaoFinal = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: $scope.DataMinSelected,
        showWeeks: true,
        language: 'pt-br'
    };

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: $scope.Data,
        minDate: $scope.DataMinSelected,
        startingDay: 1,
        language: 'pt-br'
    };

    function disabled(data) {
        var date = data.date,
          mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 8);
    };

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.oParametro.DataCriacaoInicial = new Date(year, month, day);
        $scope.oParametro.DataCriacaoFinal = new Date(year, month, day);
    };

    $scope.formats = ['dd/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['d!/M!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);

    $scope.events = [
      {
          date: tomorrow,
          status: 'full'
      },
      {
          date: afterTomorrow,
          status: 'partially'
      }
    ];

    function getDayClass(data) {
        var date = data.date,
          mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

    //FUNCOES
    $scope.ListaBizflow = function (nIdBizflowStatus, dDataCriacaoInicial, dDataCriacaoFinal, sChaveProcesso) {
        try {

            dDataCriacaoFinal = dDataCriacaoFinal.setDate(dDataCriacaoFinal.getDate() + 1);

            var DataCriacaoInicial = $filter('date')(dDataCriacaoInicial, "yyyy-MM-dd HH:mm:ss");
            var DataCriacaoFinal = $filter('date')(dDataCriacaoFinal, "yyyy-MM-dd HH:mm:ss");

            $rootScope.loading = [true, 'Carregando'];
            BizflowService.ListaBizflow(nIdBizflowStatus, sChaveProcesso, DataCriacaoInicial, DataCriacaoFinal).then(function successCallback(response) {
                $scope.ListaBizflow = response.data.Retorno;

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de Bizflow!");
        }

        $rootScope.loading = [false];
    }

    //EVENTOS
    $scope.btnEditar = function (oBizflow) {
        try {
            $scope.Bizflow = oBizflow;
            $scope.Bizflow.PropriedadesAdicionais = angular.fromJson($scope.Bizflow.PropriedadesAdicionais);

            var modalInstance = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'Views/Sistema/Bizflow/DetalheBizflow.html',
                controller: 'BizflowDetalheController',
                size: 'lg',
                backdrop: true,
                keyboard: true,
                resolve: {
                    oBizflow: function () {
                        return $scope.Bizflow;
                    }
                }
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar os dados do Bizflow!");
        }
    }
    $scope.btnPesquisar = function () {
        try {
                        
            var dDataCriacaoFinal = new Date($scope.oParametro.DataCriacaoFinal.getFullYear(), $scope.oParametro.DataCriacaoFinal.getMonth(), $scope.oParametro.DataCriacaoFinal.getDate());
            dDataCriacaoFinal = dDataCriacaoFinal.setDate(dDataCriacaoFinal.getDate() + 1);

            var DataCriacaoInicial = $filter('date')($scope.oParametro.DataCriacaoInicial, "yyyy-MM-dd HH:mm:ss");
            var DataCriacaoFinal = $filter('date')(dDataCriacaoFinal, "yyyy-MM-dd HH:mm:ss");

            $rootScope.loading = [true, 'Carregando'];
            BizflowService.ListaBizflow($scope.oParametro.Status, $scope.oParametro.ChaveProcesso, DataCriacaoInicial, DataCriacaoFinal).then(function successCallback(response) {
                $scope.ListaBizflow = response.data.Retorno;

            }, function errorCallback(response) {
                $rootScope.TrataMensagemResponseRPA(response, false);
            });
        } catch (err) {
            $rootScope.TrataMensagemResponseRPA(undefined, false, false, true, "Não foi possivel carregar a lista de Bizflow!");
        }
        $rootScope.loading = [false];
    }

    //TITULO DA PÁGINA
    $scope.$emit('atualizaTituloPagina', 'Bizflow');

    // INICIALIZACAO
    $scope.ViewMain = 'Views/Sistema/Bizflow/ListaBizflow.html';
    $scope.ListaBizflow($scope.oParametro.Status,
        new Date($scope.Data.getFullYear(), $scope.Data.getMonth(), $scope.Data.getDate()),
        new Date($scope.Data.getFullYear(), $scope.Data.getMonth(), $scope.Data.getDate()),
        $scope.oParametro.ChaveProcesso);

});

angular.module('automationApp').controller('BizflowDetalheController', function ($scope, $rootScope, $filter, $uibModalInstance, $uibModal, oBizflow) {

    $scope.oBizflow = oBizflow;

    $scope.btnFechar = function () {
        $uibModalInstance.dismiss('cancel');
    };

});