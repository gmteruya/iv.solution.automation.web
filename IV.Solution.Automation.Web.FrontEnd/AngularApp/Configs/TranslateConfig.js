﻿angular.module('automationApp').config(function ($translateProvider) {

    $translateProvider.translations('pt', {
        TITLE: 'Processos Automatizados (RPA)',
        SUBTITLE: 'Uma tecnologia inovadora que automatiza processos de negócios estruturados.',
        SUBTITLE_QUALITY: 'Rapidez, confiabilidade e qualidade.',
        USER: 'Usuário',
        PASSWORD: 'Senha',
        LOGIN: 'Login',
        CHANGE_PASSWORD: 'Trocar Senha',
        FORGOT_PASSWORD: 'Esqueci minha senha'
    });
    $translateProvider.translations('en', {
        TITLE: 'Procesos Automatizados (RPA)',
        SUBTITLE: 'Una tecnología innovadora que automatiza procesos de negocios estructurados.',
        SUBTITLE_QUALITY: 'Rapidez, confiabilidad y calidad.',
        USER: 'Usuario',
        PASSWORD: 'Contraseña',
        LOGIN: 'Login',
        CHANGE_PASSWORD: 'Cambiar Contraseña',
        FORGOT_PASSWORD: 'Olvide mi contraseña'
    });
    $translateProvider.translations('es', {
        TITLE: 'Procesos Automatizados (RPA)',
        SUBTITLE: 'Una tecnología innovadora que automatiza procesos de negocios estructurados.',
        SUBTITLE_QUALITY: 'Rapidez, confiabilidad y calidad.',
        USER: 'Usuario',
        PASSWORD: 'Contraseña',
        LOGIN: 'Login',
        CHANGE_PASSWORD: 'Cambiar Contraseña',
        FORGOT_PASSWORD: 'Olvide mi contraseña'
    });

    $translateProvider.registerAvailableLanguageKeys(['pt', 'en', 'es'], {
        'pt-BR': 'pt',
        'en_US': 'en',
        'en_UK': 'en',
        'es_MX': 'es',
        'es_ES': 'es'
    });

    $translateProvider.determinePreferredLanguage();

    $translateProvider.fallbackLanguage('pt');
    $translateProvider.preferredLanguage('pt');
});

angular.module('automationApp').factory('asyncLoader', function ($q, $timeout) {

});