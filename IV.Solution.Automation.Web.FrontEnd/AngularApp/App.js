﻿angular.module('automationApp', ['ngCookies', 'ngRoute', 'ngSanitize', 'ui.bootstrap', 'ui.bootstrap.tpls', 'smart-table', 'ivh.treeview', 'angular.filter', 'ui.mask', 'angularMoment', 'http-auth-interceptor', 'pascalprecht.translate']);

angular.module('automationApp').run(function ($rootScope, $location, $cookies, $http, $window, $injector, AuthenticationFactory, UtilService) {
    
    //TODO - Entender isso aqui
    $rootScope.MetaTag_ThemeColor = '#11a64c';

    //Carregando...
    $rootScope.$watch('loading', function (val) {
        if (typeof (val) == 'undefined') { $('.BoxPreload').fadeOut(); return; }
        if (val[0]){
            $('.BoxPreload').fadeIn();
            $('#txtPreload').html(val[1]);
        }            
        else {
            $('.BoxPreload').fadeOut();
            $('#txtPreload').html('');
        }
    });

    //Seta Globals
    $rootScope.globals = AuthenticationFactory.GetCredentials();

    //Controla de Autenticação
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        var restrictedPage = false;
        if ($location.absUrl().indexOf("IndexLogged") > 0) {
            restrictedPage = true;
        }       
        if (restrictedPage) {
            var loggedIn = false;
            if ($rootScope.globals != undefined && $rootScope.globals.Authentication != undefined) {
                loggedIn = true;
            }
            if (!loggedIn) {
                UtilService.DOM.PageRedirect('Index.html');
            }          
        }
    });

    //Controle https(SSL)
    $rootScope.forceSSL = function () {
        var arDominioSSL = UtilService.GetDominiosSSL();
        if (arDominioSSL.length > 0) {
            angular.forEach(arDominioSSL, function (oDominioSSL) {
                var oDominioRestritoSSL = false;
                if ($location.absUrl().indexOf(oDominioSSL) !== -1) {
                    oDominioRestritoSSL = true;
                }
                if ($location.protocol() !== 'https' && oDominioRestritoSSL === true) {
                    $window.location.href = $location.absUrl().replace('http', 'https');
                }
            });
        };
    }
    $rootScope.forceSSL();

});

angular.module('automationApp').config(['$locationProvider',
    function ($locationProvider) {
        $locationProvider.hashPrefix('');
    }
]);

angular.module('automationApp').config(['$httpProvider',
    function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.timeout = (1000 * 60);
        delete $httpProvider.defaults.headers.common['X-Requested-With'];        
    }
]);

//angular.module('automationApp').config(function (ivhTreeviewOptionsProvider) {
//    ivhTreeviewOptionsProvider.set({
//        idAttribute: 'Id',
//        labelAttribute: 'Descricao',
//        childrenAttribute: 'MenuFilho',
//        selectedAttribute: 'selected',
//        useCheckboxes: true,
//        expandToDepth: 0,
//        indeterminateAttribute: '__ivhTreeviewIndeterminate',
//        expandedAttribute: '__ivhTreeviewExpanded',
//        defaultSelectedState: true,
//        validate: true,
//        twistieExpandedTpl: '(-)',
//        twistieCollapsedTpl: '(+)',
//        twistieLeafTpl: 'o',
//        nodeTpl: [
//          '<div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">',
//            '<span ivh-treeview-toggle>',
//              '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>',
//            '</span>',
//            '<span class="ivh-treeview-checkbox-wrapper" ng-if="trvw.shouldUseCheckboxes()"',
//                'ivh-treeview-checkbox>',
//            '</span>',
//            '<span class="ivh-treeview-node-label" ivh-treeview-toggle>',
//              '{{trvw.label(node)}}',
//            '</span>',
//            '<div ivh-treeview-children></div>',
//          '</div>'
//            ].join('\n')
//    });
//});

//angular.module('automationApp').config(function (ivhTreeviewOptionsProvider) {
//    ivhTreeviewOptionsProvider.set({
//        idAttribute: 'id',
//        labelAttribute: 'label',
//        childrenAttribute: 'children',
//        selectedAttribute: 'selected',
//        useCheckboxes: true,
//        expandToDepth: 0,
//        indeterminateAttribute: '__ivhTreeviewIndeterminate',
//        expandedAttribute: '__ivhTreeviewExpanded',
//        defaultSelectedState: true,
//        validate: true,
//        twistieExpandedTpl: '(-)',
//        twistieCollapsedTpl: '(+)',
//        twistieLeafTpl: 'o',
//        nodeTpl: [
//          '<div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">',
//            '<span ivh-treeview-toggle>',
//              '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>',
//            '</span>',
//            '<span class="ivh-treeview-checkbox-wrapper" ng-if="trvw.shouldUseCheckboxes()"',
//                'ivh-treeview-checkbox>',
//            '</span>',
//            '<span class="ivh-treeview-node-label" ivh-treeview-toggle>',
//              '{{trvw.label(node)}}',
//            '</span>',
//            '<div ivh-treeview-children></div>',
//          '</div>'
//        ].join('\n')
//    });
//});
