﻿//Repeat last item
angular.module('automationApp').directive('onEndRepeatSt', function ($rootScope, $timeout) {
    return function (scope) {
        if (scope.$last) {
            $('#stSearchCustom').appendTo('.actionArea').attr('style', 'display:block;');
        };
    };
});

//Datepicker info adcional
angular.module('automationApp').directive('onScriptinfoadicionalRepeatEnd', function ($rootScope, $timeout) {
    return function (scope, element, attrs) {
        if (scope.$last) {
            $timeout(function () {
                $('.inputDate').datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Proximo',
                    prevText: 'Anterior'
                });
            }, 1000)
        }
    };
});

angular.module('automationApp').directive('autoFocus', function ($timeout) {
    return {
        link: function (scope, element, attrs) {
            attrs.$observe("autoFocus", function (newValue) {
                if (newValue === "true")
                    $timeout(function () { element.focus() });
            });
        }
    };
});
