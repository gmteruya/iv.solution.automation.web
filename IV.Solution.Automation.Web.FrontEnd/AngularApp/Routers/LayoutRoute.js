﻿angular.module('automationApp').config(function ($routeProvider, $httpProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'Views/Portal/Home.html?' + Math.random().toString().replace('.', ''),
        controller: 'HomeController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Home', {
        templateUrl: 'Views/Portal/Home.html?' + Math.random().toString().replace('.', ''),
        controller: 'HomeController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Home/:tipoacesso/:usuario/:chavemudancasenha/:datasolicitacao', {
        templateUrl: 'Views/Portal/Home.html?' + Math.random().toString().replace('.', ''),
        controller: 'HomeController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/SobreNos', {
        templateUrl: 'Views/Portal/SobreNos.html?' + Math.random().toString().replace('.', ''),
        controller: 'SobreNosController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Contato', {
        templateUrl: 'Views/Portal/Contato.html?' + Math.random().toString().replace('.', ''),
        controller: 'ContatoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Login', {
        templateUrl: 'Views/Portal/Login.html?' + Math.random().toString().replace('.', ''),
        controller: 'LoginController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Login/:tipoacesso/:usuario/:chavemudancasenha/:datasolicitacao', {
        templateUrl: 'Views/Portal/Login.html?' + Math.random().toString().replace('.', ''),
        controller: 'LoginController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ProcessoExterno/:stoken', {
        templateUrl: 'Views/Portal/ProcessoExterno.html?' + Math.random().toString().replace('.', ''),
        controller: 'ProcessoExternoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ZScope', {
        templateUrl: 'Views/Portal/ZScope.html?' + Math.random().toString().replace('.', ''),
        controller: 'ZScopeController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    });
});

angular.module('automationApp').run(function ($rootScope) {
    $rootScope.$on('$routeChangeStart', function (scope, next, current) {
        $('.Fade_Init').css({ display: 'block' });
    });

    $rootScope.$on('$routeChangeSuccess', function (scope, next, current) {
        $('.Fade_Init').fadeOut(1300);
    });
});
