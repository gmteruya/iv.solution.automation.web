﻿angular.module('automationApp').config(function ($routeProvider, $httpProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'CentralController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Cliente', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ClienteController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Usuario', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'UsuarioController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Grupo', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'GrupoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Script', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ScriptController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Processo', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ProcessoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ProdutoVersao', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ProdutoVersaoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Processo/:stoken', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ProcessoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Robo', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'RoboController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/RoboVersao', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'RoboVersaoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ServicoRobo', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ServicoRoboController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ServicoRoboVersao', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ServicoRoboVersaoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Dashboard', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'DashboardController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/AutomationConfiguracao', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'AutomationConfiguracaoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Menu', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'MenuController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Download', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'DownloadController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Logout', {
        templateUrl: 'Views/Sistema/Logout.html',
        controller: 'LogoutController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ClienteRegraAcesso', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ClienteRegraAcessoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/PerfilAcesso', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'PerfilAcessoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ScriptSistema', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ScriptSistemaController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ScriptSistemaAcesso', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ScriptSistemaAcessoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Dominio', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'DominioController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ScriptCodigoRetorno', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ScriptCodigoRetornoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ScriptInfoAdicional', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ScriptInfoAdicionalController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ScriptStep', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ScriptStepController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ScriptStepReprocessamento', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ScriptStepReprocessamentoController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ExpurgoDemanda', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ExpurgoDemandaController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ExpurgoDetalhe', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ExpurgoDetalheController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/ImportacaoDetalhe', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'ImportacaoDetalheController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Estrutura', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'EstruturaController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).when('/Bizflow', {
        templateUrl: 'Views/Sistema/ViewMain.html?' + Math.random().toString().replace('.', ''),
        controller: 'BizflowController',
        resolve: {
            init: function () {
                $(window).scrollTop(0);
            }
        }
    }).otherwise({ redirectTo: '/Home' });
});

angular.module('automationApp').run(function ($rootScope) {
    $rootScope.$on('$routeChangeStart', function (scope, next, current) {
        $('.Fade_Init').css({ display: 'block' });
    });

    $rootScope.$on('$routeChangeSuccess', function (scope, next, current) {
        $('.Fade_Init').fadeOut(1300);
    });
});
