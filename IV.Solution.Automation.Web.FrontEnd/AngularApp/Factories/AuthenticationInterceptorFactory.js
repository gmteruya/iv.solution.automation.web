﻿angular.module('automationApp').factory('AuthenticationInterceptorFactory', function ($scope, $rootScope, $q, $injector, UtilService, AuthenticationFactory) {
    var UsuarioService = null;
    function funRetryRequest(deferred, config) {
        $injector.get("$http")(config).then(
            function (response) {
                deferred.resolve(response);
            },
            function (response) {
                deferred.reject();
            }
        );
    };
    return {
        request: function (config) {
            config.headers = config.headers || {};

            //Pega as Credenciais 
            var oCredentials = AuthenticationFactory.GetCredentials();
            if (oCredentials) {
                     
                //Se nao for a rota de atualização de token
                if (config.url !== UtilService.AutomationServiceURI() + '/token') {

                    //Seta Token nas Chamadas da API
                    config.headers.Authorization = 'Bearer ' + oCredentials.Authentication.UsuarioAuthentication.access_token;
                    
                }
            }

            //Solução para evitar cache nos métodos get
            if (config.method == 'GET' && config.url.indexOf('.html') === -1) {
                var separator = config.url.indexOf('?') === -1 ? '?' : '&';
                config.url = config.url + separator + 'noCache=' + new Date().getTime();
            }

            //Timeout
            //config.timeout = 5 * 60 * 1000; 

            //Retorna Configuração 
            return config;
        },
        requestError: function (rejection) {
            return $q.reject(rejection);
        },
        response: function (response) {
            return response;
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                var deferred = $q.defer();
                if (rejection.config.url == UtilService.AutomationServiceURI() + '/token') {
                    //Limpa Credenciais
                    AuthenticationFactory.ClearCredentials();

                    //Rejeita o response
                    deferred.reject(rejection);

                    //Redirect Index/Login
                    UtilService.DOM.PageRedirect('Index.html');

                } else {

                    //Controla Singleton de Refresh
                    if (UsuarioService === null) {

                        //Pega as Credenciais 
                        var oCredentials = AuthenticationFactory.GetCredentials();
                        if (oCredentials) {

                            //Pega Token de Atualização
                            var rejection_original = rejection;
                            var refresh_token = oCredentials.Authentication.UsuarioAuthentication.refresh_token;
                            var client_id = oCredentials.Authentication.UsuarioAuthentication.id_retorno;

                            //Cria um novo array que armazena as chamadas.
                            $rootScope.arRoutesCall = [];

                            //Cria Instancia do Serviço
                            UsuarioService = $injector.get('UsuarioService');

                            //Atualiza o Token no Server API
                            return UsuarioService.AtualizaToken(refresh_token, client_id).then(function (r) {

                                //Retornou Novo Token?
                                if (r.data.access_token && r.data.refresh_token && r.data.expires_in) {

                                    //Put as Credenciais
                                    AuthenticationFactory.PutCredentials(r.data);

                                    //Limpa Instancia.
                                    UsuarioService = null;

                                    //Rechamada Rota Original que identificou que o token principal expirou
                                    $injector.get("$http")(rejection_original.config).then(function (resp_original) {

                                        //Resolve call original
                                        deferred.resolve(resp_original);

                                        //Rechamada das demais rotas que tbm retornaram mensagem de retorno de expiração do token (chamadas simultaneas)
                                        if ($rootScope.arRoutesCall !== null && $rootScope.arRoutesCall !== undefined) {
                                            if ($rootScope.arRoutesCall.length > 0) {
                                                for (var i = 0; i < $rootScope.arRoutesCall.length; i++) {
                                                    oRoute = $rootScope.arRoutesCall[i];
                                                    //funRetryRequest(deferred, oRoute.config);
                                                    $injector.get("$http")(oRoute.config).then(function (resp_secundario) {
                                                        deferred.resolve(resp_secundario);
                                                    }, function (resp_secundario) {
                                                        deferred.reject(resp_secundario);
                                                    }).catch(function (err_secundario) {
                                                        $scope.error = "Não foi possível completar a operação!";
                                                    });
                                                };
                                            };
                                        };

                                    }, function (resp_original) {
                                        deferred.reject(resp_original);
                                    }).catch(function (err_original) {
                                        $scope.error = "Não foi possível completar a operação!";
                                    });

                                } else {

                                    //Limpa Credenciais
                                    AuthenticationFactory.ClearCredentials();

                                    //Rejeita o response
                                    deferred.reject(rejection);

                                    //Redirect Index
                                    UtilService.DOM.PageRedirect('Index.html');

                                };
                            }, function (error) {

                                //Limpa Instancia.
                                UsuarioService = null;

                                //Limpa Credenciais
                                AuthenticationFactory.ClearCredentials();

                                //Rejeita o response
                                deferred.reject(rejection);

                                //Redirect Index
                                UtilService.DOM.PageRedirect('Index.html');

                            }).catch(function (err) {
                                $scope.error = "Não foi possível completar a operação!";
                            });
                        } else {

                            //Limpa Credenciais
                            AuthenticationFactory.ClearCredentials();

                            //Rejeita o response
                            deferred.reject(rejection);

                            //Redirect Index
                            UtilService.DOM.PageRedirect('Index.html');

                        };
                    } else {

                        //Adiciona Rota no Array que será rechamado assim que o token for refeito.
                        $rootScope.arRoutesCall.push(rejection);

                    };
                };

                //return promisse;
                return deferred.promise(rejection);
            };

            //Return
            return $q.reject(rejection);
        },
    };
});
