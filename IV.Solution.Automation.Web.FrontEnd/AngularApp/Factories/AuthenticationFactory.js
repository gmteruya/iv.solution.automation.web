﻿angular.module('automationApp').factory('AuthenticationFactory', function ($rootScope, $cookies, $window) {
    var factory = {};

    factory.SetCredentials = function SetCredentials(UsuarioAuthentication, UsuarioSessao) {

        //set in rootscope
        $rootScope.globals = {
            Authentication: {
                UsuarioAuthentication: UsuarioAuthentication,
                UsuarioSessao: UsuarioSessao
            }
        };

        // cookie
        //var cookieExp = new Date();
        //cookieExp.setMinutes(cookieExp.getMinutes() + 140);
        //$cookies.putObject('globals', $rootScope.globals, { expires: cookieExp });

        //local storage
        //localStorage.setItem('globals', JSON.stringify($rootScope.globals));

        //session storage
        sessionStorage.setItem('globals', JSON.stringify($rootScope.globals));
    }
   
    factory.GetCredentials = function GetCredentials() {
        // cookie
        //return $rootScope.globals;

        //local storage
        //return JSON.parse(localStorage.getItem('globals'));

        //session storage
        return JSON.parse(sessionStorage.getItem('globals'));
    }
    
    factory.PutCredentials = function PutCredentials(UsuarioAuthentication) {

        //put instancia
        var oAuthentication = $rootScope.globals;
        oAuthentication.Authentication.UsuarioAuthentication = UsuarioAuthentication;

        //put in rootscope
        $rootScope.globals = oAuthentication;

        //put session storage
        sessionStorage.removeItem('globals');
        sessionStorage.setItem('globals', JSON.stringify($rootScope.globals));
    }

    factory.ClearCredentials = function ClearCredentials() {
        delete $rootScope.globals;

        // cookie
        //$cookies.remove('globals');

        //local storage
        //localStorage.removeItem('globals');

        //session storage
        sessionStorage.removeItem('globals');
    }

    return factory;
});