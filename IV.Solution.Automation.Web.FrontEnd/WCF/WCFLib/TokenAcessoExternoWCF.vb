﻿Imports System.Linq
Imports IV.Automation.Integration.Model

Namespace WCFLib

    Public Class TokenAcessoExternoWCF
        Implements ITokenAcessoExternoWCF

        Public Function ConsultaToken(sTokenAcesso As String) As UsuarioWCF Implements ITokenAcessoExternoWCF.ConsultaToken
            Dim oUsuarioWCF As UsuarioWCF = Nothing
            Dim oConnectorAutomation As ConnectorAutomation = Nothing
            Dim oTokenAcessoExternoResponse As Models.ResponseGenerico(Of Models.TokenAcessoExterno) = Nothing
            Dim oTokenAcessoExterno As Models.TokenAcessoExterno = Nothing
            Try
                If String.IsNullOrEmpty(sTokenAcesso) Then Return Nothing

                'Conector com o Automation
                oConnectorAutomation = New ConnectorAutomation

                'Consulta Token
                oTokenAcessoExternoResponse = oConnectorAutomation.ConsultaToken(sTokenAcesso)
                oTokenAcessoExterno = oTokenAcessoExternoResponse.Retorno

                'Se Token Valido
                If oTokenAcessoExterno IsNot Nothing AndAlso
                    oTokenAcessoExterno.Usuario IsNot Nothing Then
                    'Preenche Usuario WCF
                    oUsuarioWCF = PreencheUsuario(oTokenAcessoExterno)
                End If
            Catch ex As Exception
                oUsuarioWCF = Nothing
            End Try
            Return oUsuarioWCF
        End Function

        Private Function PreencheUsuario(ByVal oTokenAcessoExterno As Models.TokenAcessoExterno) As UsuarioWCF
            Try
                Dim oUsuarioWCF As New UsuarioWCF
                oUsuarioWCF.Id = oTokenAcessoExterno.Usuario.Login
                oUsuarioWCF.Nome = oTokenAcessoExterno.Usuario.NomeApelido
                oUsuarioWCF.Perfis = PreenchePerfil(oTokenAcessoExterno)
                If oTokenAcessoExterno.IdPerfilAcesso = 0 Then
                    oTokenAcessoExterno.IdPerfilAcesso = 1
                End If
                oUsuarioWCF.PerfilAtual = oUsuarioWCF.Perfis.Where(Function(f) f.IdPerfil = oTokenAcessoExterno.IdPerfilAcesso).FirstOrDefault()
                oUsuarioWCF.IdOperacaoAtual = "RPA"
                oUsuarioWCF.AtributosAdicionais = New Dictionary(Of String, String)
                Return oUsuarioWCF
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function PreenchePerfil(ByVal oTokenAcessoExterno As Models.TokenAcessoExterno) As List(Of PerfilWCF)
            Try
                Dim lstPerfilWCF As New List(Of PerfilWCF)
                If Not IsNothing(oTokenAcessoExterno.PerfilAcesso) Then
                    Dim oPerfilWCF As New PerfilWCF
                    oPerfilWCF.IdPerfil = oTokenAcessoExterno.PerfilAcesso.Id
                    oPerfilWCF.NomePerfil = oTokenAcessoExterno.PerfilAcesso.Descricao
                    oPerfilWCF.PropriedadesAdicionais = New Dictionary(Of String, String)
                    lstPerfilWCF.Add(oPerfilWCF)
                Else
                    'Força Perfil Desenvolvedor
                    Dim oPerfilWCF As New PerfilWCF
                    oPerfilWCF.IdPerfil = 1
                    oPerfilWCF.NomePerfil = "Desenvolvedor TI"
                    oPerfilWCF.PropriedadesAdicionais = New Dictionary(Of String, String)
                    lstPerfilWCF.Add(oPerfilWCF)
                End If
                Return lstPerfilWCF
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
