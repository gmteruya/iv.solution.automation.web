﻿Imports System.ServiceModel

Namespace WCFLib
    <ServiceContract()>
    Public Interface ITokenAcessoExternoWCF

        <OperationContract()>
        Function ConsultaToken(ByVal sTokenAcesso As String) As UsuarioWCF

    End Interface
End Namespace


