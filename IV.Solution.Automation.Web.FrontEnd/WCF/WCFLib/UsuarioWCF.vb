﻿Imports System.Runtime.Serialization

Namespace WCFLib

    <DataContract()>
    Public Class UsuarioWCF

        <DataMember()>
        Public Property Id() As String

        <DataMember()>
        Public Property Nome() As String

        <DataMember()>
        Public Property PerfilAtual() As PerfilWCF

        <DataMember()>
        Public Property Perfis() As List(Of PerfilWCF)

        <DataMember()>
        Public Property IdOperacaoAtual() As String

        <DataMember()>
        Public Property AtributosAdicionais() As Dictionary(Of String, String)

    End Class
End Namespace

