﻿Imports System.Runtime.Serialization

Namespace WCFLib

    <DataContract()>
    Public Class PerfilWCF
        <DataMember()>
        Public Property IdPerfil() As String

        <DataMember()>
        Public Property NomePerfil() As String

        <DataMember()>
        Public Property PropriedadesAdicionais() As Dictionary(Of String, String)
    End Class

End Namespace
