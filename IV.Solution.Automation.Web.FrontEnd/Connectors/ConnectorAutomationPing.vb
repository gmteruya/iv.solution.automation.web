﻿Imports System.Configuration
Imports System.Net.Http
Imports System.Threading.Tasks
Imports IV.Automation.Integration.Model
Public Class ConnectorAutomationPing
    Inherits Connector

#Region "Constructor"

    ''' <summary>
    ''' Constructor
    ''' </summary>
    Sub New()
        MyBase.New(String.Format(ConfigurationManager.AppSettings("AUTOMATION_PING_URL_CONNECTOR"), Environment.MachineName))
    End Sub

#End Region

#Region "Overidable"

    ''' <summary>
    ''' Get Token if necessary
    ''' </summary>
    ''' <param name="sURLToken">URL where Token is generate</param>
    Protected Overrides Sub GetToken(Optional sURLToken As String = "")
        sURLToken = (String.Format(ConfigurationManager.AppSettings("AUTOMATION_PING_URL_TOKEN"), Environment.MachineName))
        MyBase.GetToken(sURLToken)
    End Sub

    Protected Overrides Function SetSecurityInfo(Optional sGrantType As String = "",
                                                Optional sUsername As String = "",
                                                Optional sPassword As String = "") As FormUrlEncodedContent
        Throw New NotImplementedException()
    End Function

#End Region

#Region "Ping"

    Public Function Ping() As Models.ResponseGenerico(Of Boolean)
        Dim oResponseGenerico As Models.ResponseGenerico(Of Boolean) = Nothing
        Try
            Dim oTask As Task(Of Models.ResponseGenerico(Of Boolean)) = Execute(Of Models.ResponseGenerico(Of Boolean))("Ping", isAnonimous:=True, oCallType:=ENUMHttpMethodType.GET_METHOD)
            oTask.Wait()
            oResponseGenerico = oTask.Result
        Catch ex As Exception
            oResponseGenerico = New Models.ResponseGenerico(Of Boolean) With {.Sucesso = False, .MsgRetorno = ex.Message}
        End Try
        Return oResponseGenerico
    End Function

#End Region

End Class
