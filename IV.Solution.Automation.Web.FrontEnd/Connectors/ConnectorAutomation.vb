﻿Imports System.Configuration
Imports System.Net.Http
Imports System.Threading.Tasks
Imports IV.Automation.Integration.Model

Public Class ConnectorAutomation
    Inherits Connector

#Region "Constructor"

    ''' <summary>
    ''' Constructor
    ''' </summary>
    Sub New()
        MyBase.New(String.Format(ConfigurationManager.AppSettings("AUTOMATION_URL_CONNECTOR"), Environment.MachineName))
    End Sub

#End Region

#Region "Overidable"

    ''' <summary>
    ''' Get Token if necessary
    ''' </summary>
    ''' <param name="sURLToken">URL where Token is generate</param>
    Protected Overrides Sub GetToken(Optional sURLToken As String = "")
        sURLToken = (String.Format(ConfigurationManager.AppSettings("AUTOMATION_URL_TOKEN"), Environment.MachineName))
        MyBase.GetToken(sURLToken)
    End Sub

    Protected Overrides Function SetSecurityInfo(Optional sGrantType As String = "",
                                                 Optional sUsername As String = "",
                                                 Optional sPassword As String = "") As FormUrlEncodedContent

        Return MyBase.SetSecurityInfo(ConfigurationManager.AppSettings("AUTOMATION_TOKEN_GRANT_TYPE"),
                                      ConfigurationManager.AppSettings("AUTOMATION_TOKEN_USERNAME"),
                                      ConfigurationManager.AppSettings("AUTOMATION_TOKEN_PASSWORD"))
    End Function

#End Region

#Region "TokenAcessoExterno"

    Public Function ConsultaToken(ByVal sToken As String) As Models.ResponseGenerico(Of Models.TokenAcessoExterno)
        Dim oResponseGenerico As Models.ResponseGenerico(Of Models.TokenAcessoExterno) = Nothing
        Try
            Dim oTask As Task(Of Models.ResponseGenerico(Of Models.TokenAcessoExterno)) = Execute(Of Models.ResponseGenerico(Of Models.TokenAcessoExterno))(String.Format("ConsultaToken/{0}", sToken), isAnonimous:=True, oCallType:=ENUMHttpMethodType.GET_METHOD)
            oTask.Wait()
            oResponseGenerico = oTask.Result
        Catch ex As Exception
            oResponseGenerico = New Models.ResponseGenerico(Of Models.TokenAcessoExterno) With {.Sucesso = False, .MsgRetorno = ex.Message}
        End Try
        Return oResponseGenerico
    End Function

#End Region

End Class
