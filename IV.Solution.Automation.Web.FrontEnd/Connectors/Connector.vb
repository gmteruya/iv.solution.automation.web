﻿Imports System.Net.Http
Imports System.Threading.Tasks
Imports IV.Solution.Automation.Web.FrontEnd.Security
Imports Newtonsoft.Json

''' <summary>
''' Bases functions and methods for a Connector
''' </summary>
Public MustInherit Class Connector

#Region "Properties"

    Private _ServiceURI As Uri
    Public Property ServiceURI As Uri
        Get
            Return _ServiceURI
        End Get
        Set(ByVal value As Uri)
            _ServiceURI = value
        End Set
    End Property

    Public Property [To] As String

    Private Shared _ParsedTokenBody As ParsedTokenBody
    Public ReadOnly Property ParsedTokenBody() As ParsedTokenBody
        Get
            If IsNothing(_ParsedTokenBody) Then
                GetToken()
            End If
            Return _ParsedTokenBody
        End Get
    End Property

    Private Shared _SecurityContent As FormUrlEncodedContent
    Public ReadOnly Property oSecurityContent As FormUrlEncodedContent
        Get
            Try
                Return _SecurityContent
            Catch
                Throw
            End Try
            Return Nothing
        End Get
    End Property

#End Region

#Region "Constructor"

    Sub New(ByVal sUrl As String)
        Try
            If String.IsNullOrEmpty(sUrl) Then Throw New Exception("Unable to connect, URL's configuration has been not found")
            _ServiceURI = New Uri(sUrl)
        Catch
            Throw
        End Try
    End Sub

#End Region

#Region "Public Methods"

    Protected Async Function Execute(Of T)(sRequest As String,
                                           Optional oCallType As ENUMHttpMethodType = ENUMHttpMethodType.GET_METHOD,
                                           Optional isAnonimous As Boolean = True,
                                           Optional oHttpContent As HttpContent = Nothing,
                                           Optional oObject As Object = Nothing) As Task(Of T)
        Try
            Dim sObject As String = Newtonsoft.Json.JsonConvert.SerializeObject(oObject)

            'Valida se é possivel construi o objeto
            If IsNothing(Me.ServiceURI) Then
                Throw New Exception("Unable to detect service URI")
            End If

            'Trata Http Content 
            If IsNothing(oHttpContent) Then
                oHttpContent = New StringContent(sRequest)
            End If

            'Variaveis 
            Dim oResponse As HttpResponseMessage = Nothing
            Dim sResponse As String = String.Empty

            'Chamada Http 
            Using oCliente As New HttpClient
                oCliente.BaseAddress = Me.ServiceURI
                oCliente.DefaultRequestHeaders.Accept.Clear()
                oCliente.DefaultRequestHeaders.Accept.Add(New System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"))
                oCliente.Timeout = TimeSpan.FromMinutes(20)
                If (Not isAnonimous) Then oCliente.DefaultRequestHeaders.Authorization = New Headers.AuthenticationHeaderValue(_ParsedTokenBody.token_type, _ParsedTokenBody.access_token)

                Try
                    If oCallType = ENUMHttpMethodType.GET_METHOD Then oResponse = oCliente.GetAsync(sRequest).Result
                    If oCallType = ENUMHttpMethodType.POST_METHOD Then oResponse = oCliente.PostAsync(sRequest, oHttpContent).Result
                    If oCallType = ENUMHttpMethodType.JSON_POST_METHOD Then oResponse = oCliente.PostAsJsonAsync(sRequest, oObject).Result
                    If oCallType = ENUMHttpMethodType.PUT_METHOD Then oResponse = oCliente.PutAsync(sRequest, oHttpContent).Result
                    If oCallType = ENUMHttpMethodType.JSON_PUT_METHOD Then oResponse = oCliente.PutAsJsonAsync(sRequest, oObject).Result

                    If IsNothing(oResponse) Then
                        Throw New Exception("Response has not been recieved")
                    End If

                Catch ex As Exception

                End Try

                If oResponse IsNot Nothing And oResponse.StatusCode <> Net.HttpStatusCode.NotFound Then
                    Return Await oResponse.Content.ReadAsAsync(Of T)()
                Else
                    Throw New Exception("Response has not been recieved")
                End If

            End Using
        Catch
            Throw
        End Try
    End Function

#End Region

#Region "Protected Method"

    Protected Overridable Sub GetToken(Optional ByVal sURLToken As String = "")
        Try
            'Try to Obtain a token 
            Using oClient = New Net.Http.HttpClient
                Using oTokenResponse As HttpResponseMessage = oClient.PostAsync(sURLToken, oSecurityContent).Result
                    Dim oTokenBody = oTokenResponse.Content.ReadAsStringAsync
                    Dim oParsedTokenBody As ParsedTokenBody = JsonConvert.DeserializeObject(Of ParsedTokenBody)(oTokenBody.Result.ToString)
                    _ParsedTokenBody = oParsedTokenBody
                End Using
            End Using

            'Verify token existence 
            If IsNothing(_ParsedTokenBody) Then
                Throw New Exception("Unable to obtain Token")
            End If

            'Verify any exception
            If Not String.IsNullOrEmpty(_ParsedTokenBody.error_message) OrElse
               Not String.IsNullOrEmpty(_ParsedTokenBody.error_description) Then
                Throw New Exception(_ParsedTokenBody.error_message, New Exception(_ParsedTokenBody.error_description))
            End If

        Catch
            Throw
        End Try
    End Sub

    Protected Overridable Function SetSecurityInfo(Optional ByVal sGrantType As String = "",
                                                   Optional ByVal sUsername As String = "",
                                                   Optional ByVal sPassword As String = "") As FormUrlEncodedContent
        Try
            Dim lstKeyValuePair As New List(Of KeyValuePair(Of String, String))
            lstKeyValuePair.Add(New KeyValuePair(Of String, String)("grant_type", sGrantType))
            lstKeyValuePair.Add(New KeyValuePair(Of String, String)("username", sUsername))
            lstKeyValuePair.Add(New KeyValuePair(Of String, String)("password", sPassword))
            Return New FormUrlEncodedContent(lstKeyValuePair)
        Catch
            Throw
        End Try
    End Function

#End Region

End Class
