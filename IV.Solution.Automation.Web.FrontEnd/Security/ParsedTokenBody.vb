﻿Imports Newtonsoft.Json

Namespace Security

    Public Class ParsedTokenBody

#Region "Propriedades"

        Public Property access_token As String

        Public Property token_type As String

        Public Property expires_in As String

        <JsonProperty(PropertyName:="error")>
        Public Property error_message As String

        Public Property error_description As String

#End Region

    End Class

End Namespace